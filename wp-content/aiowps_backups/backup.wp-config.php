<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'odontop_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'odontop_usite');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'IzWp[3-0^x5A');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r)07yk7_sOo/M]HKn8l_(;7gxDO*{U2BY1yfS/2k<|FE{/.)EX+yJoYRE`N%[xJ.');
define('SECURE_AUTH_KEY',  'p]FQ2|p{>QsZ27$zIc;xTWlqoJgYR&s dlv<Q)gsOd7k9b~6MgfQbO9)8sY`b!uu');
define('LOGGED_IN_KEY',    'J[f#LS&;rP8B6T|<6FblXB3@=@q{0xks p9E_dchY b!k3Of,4e86`/~TEQL5n8r');
define('NONCE_KEY',        '(`wZ/]>46m/T&2GKh~lFfW/@QDOvo,U+7#5dp2WP3 <fH[Uz4+7g$VDK=sA?xxY:');
define('AUTH_SALT',        'q|cFH_rVP.#{S7g,d(>:)ya!4r.d2b,I$]a#IN69d__u6.U-&RZ`ju^tN|GXp]JB');
define('SECURE_AUTH_SALT', '3%)eSukLy&@=?_e>?Jr5u4:0t5!WGrWho|Jy6gi;02kX ,J8q>rX&1TncE8w{wrZ');
define('LOGGED_IN_SALT',   'A$ZJUW:l]AQ?K:za-:m>RoI|A3s`<%cr2ayka0kjPi_=~(Q9u8<3h:{3aqr(A}l0');
define('NONCE_SALT',       'irNl!!&k&#hz9lpa;/=3jwY3kZ|9F8P*&TUDsm>_=|~j:VbH$2YAqI6Y2Kb5gwbK');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', true);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
