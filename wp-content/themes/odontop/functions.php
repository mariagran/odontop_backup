<?php

    include('vendor/general_functions.php');
    include('vendor/movie.php');
    include('vendor/admin.php');


add_theme_support( 'post-thumbnails' );

define('TEMPLATE', get_bloginfo('template_url') . '/');
define('ASSETS', get_bloginfo('template_url') . '/assets/');
define('HOME', get_bloginfo('url') . '/');


add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Blog Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets desta área será mostrado nas páginas do Blog.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}


function getSedesName(){
	$argsSedes = array(
		'post_type' 	 => 'sedes',
		'posts_per_page' => '-1'
	);

	$loopSedes = new WP_Query( $argsSedes );
	if($loopSedes->have_posts() ) :
		while ( $loopSedes->have_posts() ) : $loopSedes->the_post();
			$sedes[] = array('name' => get_the_title(),
							 'telefone' => get_field('telefone'),
							 'whatsapp' => get_field('whatsapp'),
							 'horarios'  => get_field('horarios'));
		endwhile;
		wp_reset_query();
	endif;

	return $sedes;

}

function getServicessName(){
	$argsServicos = array(
		'post_type' 	 => 'tratamentos',
		'posts_per_page' => '-1'
	);

	$loopServicos = new WP_Query( $argsServicos );
	if($loopServicos->have_posts() ) :
		global $post;
		while ( $loopServicos->have_posts() ) : $loopServicos->the_post();
			$servicos[] = array('name' => get_the_title(),
								'id'   => get_the_ID(),
								'slug' => $post->post_name,
								'permalink' => get_permalink());
		endwhile;
		wp_reset_query();
	endif;

	return $servicos;

}

function showDoctors(){
	$argsDoutores = array(
		'post_type' 	 => 'doutores',
		'posts_per_page' => '-1'
	);

	$loopDoutores = new WP_Query( $argsDoutores );
	if($loopDoutores->have_posts() ) :
		while ( $loopDoutores->have_posts() ) : $loopDoutores->the_post();
?>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="box-dr">
					<img src="<?php echo get_field('imagem'); ?>">
					<h4><?php the_title(); ?></h4>
					<span>CRM <?php echo get_field('crm'); ?></span>
					<p><?php echo get_field('descricao'); ?></p>
				</div>
			</div>
<?php
		endwhile;
		wp_reset_query();
	endif;
}


function showSedes($page = null){
	$argsSedes = array(
		'post_type' 	 => 'sedes',
		'posts_per_page' => '-1'
	);

	$loopSedes = new WP_Query( $argsSedes );
	$countSede = 1;
	if($loopSedes->have_posts() ) :
		while ( $loopSedes->have_posts() ) : $loopSedes->the_post();
?>
<?php if($page != 'contact-page'): ?>

<?php if($countSede == 1): ?>
	<div class="row novo">
<?php endif; ?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?php endif; ?>
	<div class="box-sede">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 tira-padding">
			<img src="<?php echo get_field('imagem'); ?>" alt="">
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
			<h4><?php the_title(); ?></h4>
			<p><?php echo get_field('endereco'); ?></p>
			<?php $horarios = get_field('horarios'); ?>
			<?php foreach ($horarios as $horario): ?>
				<p><?php echo $horario['dias'] . ' das ' . $horario['horario']; ?></p>
			<?php endforeach; ?>
			<ul>
				<li><img src="<?php echo ASSETS; ?>images/mini-fone.png" alt=""> <?php echo get_field('telefone'); ?></li>
				<li><img src="<?php echo ASSETS; ?>images/mini-whats.png" alt=""> <?php echo get_field('whatsapp'); ?></li>
				<li><a href="https://www.google.com.br/maps/place/<?php echo get_field('endereco'); ?>" target="_blank"><img src="<?php echo ASSETS; ?>images/mini-marker.png" alt=""> Como Chegar</a></li>
			</ul>
		</div>
	</div>
<?php if($page != 'contact-page'): ?>
</div>
<?php if($countSede % 2 == 0): ?>
	</div>
	<div class="row">
<?php endif; ?>
<?php endif; ?>
<?php
		$countSede++;
		endwhile;
		echo '</div>';
		wp_reset_query();
	endif;
}

function showDepoimentos(){
	$argsDepoimentos = array(
		'post_type' 	 => 'depoimentos',
		'posts_per_page' => '-1'
	);

	$loopDepoimentos = new WP_Query( $argsDepoimentos );
	if($loopDepoimentos->have_posts() ) :
		while ( $loopDepoimentos->have_posts() ) : $loopDepoimentos->the_post();
?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="box-depoimento">
					<p><?php echo get_field('descricao'); ?></p>
					<img src="<?php echo get_field('imagem'); ?>" alt="">
					<h4><?php the_title(); ?></h4>
					<span><?php echo get_field('cargo'); ?></span>
				</div>
			</div>
<?php
		endwhile;
		wp_reset_query();
	endif;
}