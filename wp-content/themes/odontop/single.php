<?php get_header(); ?>

    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>Blog</strong></h3>
        </div>
    </div>

    <section class="blog blog-single">
        <div class="container">
            <div class="row">
                
                <?php
                    if(have_posts()):
                        while ( have_posts() ) : the_post();

                ?>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 list-blog">
                                <div class="box-blog">
                                    <?php if( has_post_thumbnail()): ?>
                                        <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
                                        <img src="<?php echo $large_image_url[0]; ?>" alt="">
                                    <?php endif; ?>
                                    <h3><?php the_title(); ?></h3>
                                    <span><?php echo get_the_date('d M y'); ?> / <?php comments_number( '0 comentários', '1 comentário', '% comentários' ); ?></span>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                <?php

                        endwhile; // End of the loop.
                    endif;
                ?>

                <!-- SIDEBAR -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul id="sidebar">
                        <?php dynamic_sidebar('blog-sidebar'); ?>
                    </ul>
                </div>
                
            </div>
        </div>
    </section>

<?php get_footer(); ?>