<?php
	/* Template Name: A Clinica */
?>

<?php get_header(); ?>

<?php
		if(have_posts()): 
			while ( have_posts() ) : the_post();
?>
    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>A Clínica</strong></h3>
        </div>
    </div>

    <section class="escolher bg-branco">
		<div class="container">
			<div class="escolher">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h3>Por que escolher a Odontop?</h3>
						<?php echo get_field('por_que_escolher_a_odontop'); ?>
						<a href="<?php echo HOME; ?>tratamentos" class="link-tratamentos"><button>Tratamentos</button></a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="carousel-escolha">
							<?php $images = get_field('galeria'); ?>
							<?php foreach ($images as $image): ?>
							<img src="<?php echo $image['url']; ?>" alt="">
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="sobre bg-cinza">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h4>História</h4>
                    <?php echo get_field('historia'); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h4>Valores</h4>
                    <?php echo get_field('valores'); ?>
                </div>
            </div>
        </div>
    </section>

    <section class="sedes">
        <div class="container">
			<h2>Nossas Sedes</h2>
            <!-- <div class="row"> -->
                <?php showSedes(); ?>
            <!-- </div> -->
        </div>
    </section>

    <section class="dr bg-cinza">
		<div class="container">
            <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tira-padding">
					<div class="agendar">
						<?php echo get_template_part('partials/content', 'agendarconsulta1'); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<?php get_template_part('partials/content', 'responsavel_tecnico'); ?>
			</div>
		</div>
	</section>

    <section class="forma-pagamento">
        <div class="container">
            <h2>Formas de Pagamento</h2>
            <img src="<?php echo get_field('formas_de_pagamento'); ?>" alt=""> 
        </div>
    </section>

<?php
		endwhile;
	endif;
?>
<?php get_footer(); ?>