<?php
	/* Template Name: Blog */
?>

<?php get_header(); ?>

    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>Blog</strong></h3>
        </div>
    </div>

    <section class="blog">
        <div class="container">
            <div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 list-blog">
	            	<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args = array(
							'post_type' 	 => 'post',
							'posts_per_page' => '2',
							'paged'			 => $paged
						);

						$loop = new WP_Query( $args );
						if($loop->have_posts() ) :
							while ( $loop->have_posts() ) : $loop->the_post();
					?>
								<div class="box-blog blog-list">
			                        <?php if( has_post_thumbnail()): ?>
										<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
										<img src="<?php echo $large_image_url[0]; ?>" alt="">
								    <?php endif; ?>
			                        <h3><?php the_title(); ?></h3>
			                        <span><?php echo get_the_date('d M y'); ?> / <?php comments_number( '0 comentários', '1 comentário', '% comentários' ); ?></span>
			                        <p><?php echo get_field('breve_descricao'); ?></p>
			                        <a href="<?php echo get_permalink(); ?>">Leia Mais</a>
			                    </div>
					<?php
							endwhile;
						endif;
					?>
                
                    <div class="paginacao">
                    	<?php wp_pagenavi( array( 'query' => $loop ) ); ?>
                        <!-- <ul>
                            <li><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                        </ul> -->
                    </div>
                </div>

                <!-- SIDEBAR -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul id="sidebar">
                        <?php dynamic_sidebar('blog-sidebar'); ?>
                    </ul>
                </div>
            </div>



        </div>
    </section>

<?php get_footer(); ?>