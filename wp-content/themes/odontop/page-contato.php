<?php
	/* Template Name: Contato */
?>
<?php get_header(); ?>

    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>Contato</strong></h3>
        </div>
    </div>

    <section class="blog contato">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h3>Formulario de Contato</h3>
                </div>
            </div>
            <div class="row">
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 container_form_contato">
                   <form action="<?php echo get_template_directory_uri(); ?>/controllers/ContatoController.php" method="post" id="contact_form" accept-charset="utf-8" >
                       <h6>Nome</h6>
                       <input type="text" name="form[name]">
                       <h6>Sobrenome</h6>
                       <input type="text" name="form[lastname]">
                       <h6>E-mail</h6>
                       <span class="email-invalid" style="color: red; display: none;">Informe um e-mail válido.</span>
                       <input type="text" class="form-email" name="form[email]">
                       <h6>Telefone</h6>
                       <input type="text" name="form[phone]" onkeypress="Mascara(this,Telefone)" maxlength = "15">
                       <h6>Em qual sede você quer ser atendido?</h6>
                       <select name="form[sede]" id="">
                           <?php $sedesNames = getSedesName(); ?>
			              <?php foreach ($sedesNames as $sedeName): ?>
			                  <option value="<?php echo $sedeName['name']; ?>"><?php echo $sedeName['name']; ?></option>
			              <?php endforeach; ?>
                       </select>
                       <h6>Mensagem</h6>
                       <textarea name="form[message]"></textarea>
                       <input type="submit" class="btn-submit" value="Enviar">
                   </form>
					
					<div class="row" style="margin-top: 20px; ">
	                   	<div class="col-md-12">
							<div class="message_flash"></div>
						</div>
					</div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                   <?php showSedes('contact-page'); ?>
               </div> 
            </div>
        </div>
    </section>

<?php get_footer(); ?>