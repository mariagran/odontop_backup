<?php get_header(); ?>

    <div class="container">
        <div class="mapa-navegacao">
            <h3><a href="<?php echo HOME; ?>">Home</a> / <strong>Sedes</strong></h3>
        </div>
    </div>

    <section class="sedes">
        <div class="container">
			<h2>Nossas Sedes</h2>
            <div class="row">
                <?php showSedes(); ?>
            </div>
        </div>
    </section>


<?php get_footer(); ?>