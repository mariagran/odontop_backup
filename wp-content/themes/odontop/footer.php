	<footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-footer">
                    <h4>Nossos Serviços</h4>
                    <ul>
                        <?php $services = getServicessName(); ?>
                        <?php foreach ($services as $service): ?>
                        <li><a href="<?php echo $service['permalink']; ?>"><?php echo $service['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-footer">
                    <h4>Nossas Sedes</h4>

                    <ul>
                        <?php $sedesNames = getSedesName(); ?>
                        <?php foreach ($sedesNames as $sedeName): ?>
                            <li><a href="<?php echo get_permalink(4); ?>"><?php echo $sedeName['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-footer">
                    <h4>Telefones e Horários</h4>
                    <select id="sedes-footer">
                        <?php foreach ($sedesNames as $sedeName): ?>
                            <option value="<?php echo $sedeName['name']; ?>" rela="<?php echo $sedeName['telefone']; ?>" relb="<?php echo $sedeName['whatsapp']; ?>"><?php echo $sedeName['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <h2 class="sede-footer-telefone">
                      <img src="<?php echo ASSETS; ?>images/icon-fone-footer.png" alt="">
                      <div><?php echo $sedesNames[0]['telefone']; ?></div>
                    </h2>
                    
                    <h2 class="sede-footer-whatsapp">
                      <img src="<?php echo ASSETS; ?>images/icon-whats-footer.png" alt="">
                      <div><?php echo $sedesNames[0]['whatsapp']; ?></div>
                    </h2>
                    
                    <div class="horario">
                      <?php //debug($sedesNames); ?>
                      <?php $countHorarios = 0; ?>
                      <?php foreach ($sedesNames as $horarios): ?>
                        <div class="footer-horarios" rel="<?php echo $horarios['name']; ?>" <?php echo ($countHorarios == 0) ? '' : 'style="display:none;"' ?>>
                        <?php foreach ($horarios['horarios'] as $horario): ?>
                            <p><?php echo $horario['dias']; ?></p>
                            <span><?php echo $horario['horario']; ?></span>
                        <?php endforeach; ?>
                        </div>
                        <?php $countHorarios++; ?>
                      <?php endforeach; ?>
                    </div>
                    


                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 box-footer">
                    <h4>Redes Sociais</h4>
                    <ul>
                        <li><a href="<?php echo get_field('facebook', 4); ?>" target="_blank"><img src="<?php echo ASSETS; ?>images/icon-facebook-footer.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>   
    </footer>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tira-padding">
                <div class="agendar-footer">
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-2 col-sm-12">
                            <img src="<?php echo ASSETS; ?>images/icon-calendar-o.png" alt="">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
                            <h3><?php echo get_field('destaque2_titulo', 123); ?></h3>
                            <p><?php echo get_field('destaque2_descricao', 123); ?></p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <a class="btn-agendar-footer" href="<?php echo HOME; ?>contato"><button>Agendar</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="rodape">
        <div class="container">
            <div class="row">
                <div class="cophy">
                    <p>© Odontop - Todos os direitos reservados 2017</p>
                </div>
                <div class="dev">
                    <p>Desenvolvido por: <a href="http://www.handgran.com" target="_blank"><img src="<?php echo ASSETS; ?>images/icon-dev.png" alt=""></a></p>
                </div>
            </div>
        </div>
    </div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo ASSETS; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>bower_components/bxSlider/dist/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>js/lightbox.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>js/jquery.nice-select.min.js"></script>	
    <script type="text/javascript" src="<?php echo ASSETS; ?>js/mascara_ereg.js"></script>
    <script type="text/javascript" src="<?php echo ASSETS; ?>js/main.js"></script>
    
    <script>
      $(document).ready(function(){
        $('ul#sidebar').find('li ul li').prepend('<i class="fa fa-circle-o">&nbsp;</i>');
      })
    </script>

    <script>
        jQuery('#contact_form').submit(ajaxSubmit);

      function ajaxSubmit(){
          jQuery.ajax({
          type:"POST",
          url: $(this).attr('action'),
          dataType: 'json',
          data: jQuery(this).serialize(),
          beforeSend: function(){
            $('.btn-submit').val('Enviando...');
          },
          complete: function(){
            $('.btn-submit').val('Enviar');
          },
          error: function(){
            alert('error');
          },
          success:function(data){
            if(data.status == true) {
              $('.message_flash').html('');
              $('.message_flash').append('<i class="fa fa-check">&nbsp;</i>');
              $('.container_form_contato .message_flash').css('color', 'green');
              $('.message_flash').append('Mensagem enviada com sucesso.');
              $('.container_form_contato .message_flash').removeClass('bg-danger');
              $('.container_form_contato .message_flash').addClass('bg-success');
              $('.container_form_contato').show('fast');
              $('#contact_form').find('input[type="text"]').each(function(i, val){
                $(val).val('');
              });
              $('#contact_form').find('textarea').each(function(i, val){
                $(val).val('');
              });
              $('.btn-submit').val('Enviar');
            } else {
              $('.message_flash').html('');
              $('.message_flash').append('<i class="fa fa-ban">&nbsp;</i>');
              $('.container_form_contato .message_flash').css('color', 'red');
              $.each(data.validate, function(i, val) {
                $('.message_flash').append(val + '<br/>');
              });
                $('.container_form_contato .message_flash').removeClass('bg-success');
                $('.container_form_contato .message_flash').addClass('bg-danger');
                $('.container_form_contato').show('fast');
            }
          }
        });
      return false;
      };
    </script>

    <script>
      $('#sedes').on('change', function(){
        var value = $(this).val();
        var telefone = $('#sedes option[value="'+value+'"').attr('rela');
        var whatsapp = $('#sedes option[value="'+value+'"').attr('relb');
        $('h2.sede-telefone').find('span').html(telefone);
        $('h2.sede-whatsapp').find('span').html(whatsapp);
      })


      $('#sedes-footer').on('change', function(){
        var value = $(this).val();
        var telefone = $('#sedes-footer option[value="'+value+'"').attr('rela');
        var whatsapp = $('#sedes-footer option[value="'+value+'"').attr('relb');
        
        $('h2.sede-footer-telefone').find('div').html(telefone);
        $('h2.sede-footer-whatsapp').find('div').html(whatsapp);

        $('.footer-horarios').hide();
        $('.footer-horarios[rel="'+value+'"]').show();
      })
    </script>

    <script>
    	var validations ={
		    email: [/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/, 'Por favor informe um e-mail válido']
		};
		$(document).ready(function(){
		    // Check all the input fields of type email. This function will handle all the email addresses validations
		    $("input.form-email").change( function(){
		        // Set the regular expression to validate the email 
		        validation = new RegExp(validations['email'][0]);
		        // validate the email value against the regular expression
		        if (!validation.test(this.value)){
		        	$('.email-invalid').css('display', 'block');
		        	$('#contact_form').submit(function(){return false;});
		            // If the validation fails then we show the custom error message
		            this.setCustomValidity(validations['email'][1]);
		            return false;
		        } else {
		        	$('.email-invalid').css('display', 'none');
		            // This is really important. If the validation is successful you need to reset the custom error message
		            this.setCustomValidity('');
		        }
		    });
		})
    </script>
  </body>
</html>