<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Odontop</title>

     <!-- Imports css bower-->
    <link rel="stylesheet" href="<?php echo ASSETS; ?>bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>bower_components/bxSlider/dist/jquery.bxslider.css">
<!--     <link rel="stylesheet" href="bower_components/shadowbox/source/resources/shadowbox.css"> -->
    
    <!-- Imports css estilos -->
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/reset.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/header.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/index.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/clinica.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/blog.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/tratamentos.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/contato.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/footer.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/lightbox.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/pagenavi-css.css">
    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/nice-select.css">
    <link href='http://fonts.googleapis.com/css?family=Galindo' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo ASSETS; ?>css/custom.css">

  </head>
  <body>

  <header>
    <div class="container">
      <div class="row">
        <div class="top">
          
          <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12" style="text-align: center;">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="logo-header">
                <a href="<?php echo HOME; ?>"><img src="<?php echo ASSETS; ?>images/logo-header.png" alt=""></a>
              </div>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <select id="sedes">
              <?php $sedesNames = getSedesName(); ?>
              <?php foreach ($sedesNames as $sedeName): ?>
                  <option value="<?php echo $sedeName['name']; ?>" rela="<?php echo $sedeName['telefone']; ?>" relb="<?php echo $sedeName['whatsapp']; ?>"><?php echo $sedeName['name']; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 tira-padding" style="">
              <h2 class="sede-telefone">
                <img src="<?php echo ASSETS; ?>images/icon-fone.png" alt=""> 
                <span><?php echo $sedesNames[0]['telefone']; ?></span>
              </h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 tira-padding" style="">
              <h2 class="sede-whatsapp">
                <img src="<?php echo ASSETS; ?>images/icon-whats.png" alt=""> 
                <span><?php echo $sedesNames[0]['whatsapp']; ?></span>
              </h2>
            </div>
          </div>


        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="<?php echo HOME; ?>">Home</a></li>
                  <li><a href="<?php echo HOME; ?>a-clinica">A Clínica</a></li>
                  <li><a href="<?php echo HOME; ?>tratamentos">Tratamentos</a></li>
                  <li><a href="<?php echo HOME; ?>blog">Blog</a></li>
                  <li><a href="<?php echo HOME; ?>contato">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="<?php echo HOME; ?>contato">Agendar Consulta <img src="<?php echo ASSETS; ?>images/icon-calendar-mini.png" alt=""></a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div>
          </div><!-- /.container-fluid -->
        </nav>
      </div>
    </div>
  </header>

