<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Odontop</title>

     <!-- Imports css bower-->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="bower_components/bxSlider/dist/jquery.bxslider.css">
<!--     <link rel="stylesheet" href="bower_components/shadowbox/source/resources/shadowbox.css"> -->
    
    <!-- Imports css estilos -->
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/clinica.css">
    <link rel="stylesheet" href="css/blog.css">
    <link rel="stylesheet" href="css/tratamentos.css">
    <link rel="stylesheet" href="css/contato.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link href='http://fonts.googleapis.com/css?family=Galindo' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

  </head>
  <body>

  <header>
    <div class="container">
      <div class="row">
        <div class="top">
          <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="logo-header">
                <a href="#"><img src="images/logo-header.png" alt=""></a>
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-3 col-xs-8">
            <select>
              <option value="Colombo">Colombo</option>
            </select>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <h2><img src="images/icon-fone.png" alt=""> 41 3224-2989</h2>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <h2><img src="images/icon-whats.png" alt=""> 41 3224-2989</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="clinica.php">A Clínica</a></li>
                  <li><a href="sedes.php">Sedes</a></li>
                  <li><a href="tratamentos.php">Tratamentos</a></li>
                  <li><a href="blog.php">Blog</a></li>
                  <li><a href="contato.php">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="#">Agendar Consulta <img src="images/icon-calendar-mini.png" alt=""></a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div>
          </div><!-- /.container-fluid -->
        </nav>
      </div>
    </div>
  </header>

