$('.carousel-escolha').bxSlider({
    auto: true,
    controls: true,
    pager: false
});

$('.slider-tratamentos').bxSlider({
  minSlides: 3,
  maxSlides: 4,
  slideWidth: 170,
  slideMargin: 20,
  pager: false
});

$(document).ready(function() {
  $('select').niceSelect();
});