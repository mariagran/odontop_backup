<section class="dr">
	<div class="container">
		<div class="row box-responsavel">
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<img src="<?php echo get_field('imagem', 207); ?>">
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="info-responsavel">
						<h4><?php echo get_field('nome', 207); ?></h4>
						<p><?php echo get_field('cro', 207); ?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
				<p><?php echo get_field('descricao', 207); ?></p>
			</div>
		</div>
	</div>
</section>