<div class="row">
	<div class="col-lg-1 col-md-1 col-sm-2 col-sm-12">
		<img src="<?php echo ASSETS; ?>images/icon-calendar.png" alt="">
	</div>
	<div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
		<h3><?php echo get_field('destaque2_titulo', 123); ?></h3>
		<p><?php echo get_field('destaque2_descricao', 123); ?></p>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
		<a class="btn-agendar" href="<?php echo HOME; ?>contato"><button>Agendar</button></a>
	</div>
</div>