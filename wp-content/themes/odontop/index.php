<?php get_header(); ?>
    
	<section class="banner">
		<div class="container-fluid">
			<div class="row">
				<div class="banner-principal">
					<div class="item">
						<img src="<?php echo get_field('imagem', 118); ?>" alt="">
						<div class="container">
							<div class="caption-banner">
								<h1><?php echo get_field('titulo', 118); ?></h1>
								<p><?php echo get_field('subtitulo', 118); ?></p> 
								<a class="btn-agendar-banner" href="<?php echo HOME; ?>contato"><button>Agendar Consulta</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="servicos-home">
		<div class="container">
			<div class="row">
				<?php
					$argsServicos = array(
						'post_type' 	 => 'tratamentos',
						'meta_query' => array(
						    array(
						        'key' => 'icone',
						        'value'   => array(''),
						        'compare' => 'NOT IN'
						    )
						),
						'posts_per_page' => '-1'
					);

					$loopServicos = new WP_Query( $argsServicos );
					if($loopServicos->have_posts() ) :
						while ( $loopServicos->have_posts() ) : $loopServicos->the_post();
				?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tira-padding box-tratamentos">
								<div class="box-servico bg-servico1">
									<img src="<?php echo get_field('icone'); ?>" alt="">
									<h4><?php the_title(); ?></h4>
									<p><?php echo get_field('breve_descricao'); ?></p>
								</div>
							</div>
				<?php
						endwhile;
						wp_reset_query();
					endif;
				?>
			</div>
		</div>
	</section>

	<section class="escolher">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tira-padding">
					<div class="agendar">
						<?php echo get_template_part('partials/content', 'agendarconsulta1'); ?>
					</div>
				</div>
			</div>
			<div class="escolher">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h3>Porque escolher a Odontop?</h3>
						<?php echo get_field('por_que_escolher_a_odontop', 4); ?>
						<a href="<?php echo get_permalink(4); ?>"><button>Conheça</button></a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="carousel-escolha">
							<?php $images = get_field('galeria', 4); ?>
							<?php foreach ($images as $image): ?>
							<img src="<?php echo $image['url']; ?>" alt="">
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="spe">
		<div class="container">
			<div class="row">
				<?php $faixaLaranjaItens = get_field('itens', 127); ?>
				<?php foreach ($faixaLaranjaItens as $item): ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="box-spe">
						<img src="<?php echo $item['icone']; ?>" alt="">
						<h4><?php echo $item['titulo']; ?></h4>
						<p><?php echo $item['descricao']; ?></p>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>

	<?php get_template_part('partials/content', 'responsavel_tecnico'); ?>

	<section class="noticias">
		<div class="container">
			<div class="row">
				<?php
					$args = array(
						'post_type' 	 => 'post',
						'meta_key'		 => 'mostrar_na_pagina_inicial',
						'meta_value'	 => 1,
						'posts_per_page' => '3'
					);

					$loop = new WP_Query( $args );
					if($loop->have_posts() ) :
						while ( $loop->have_posts() ) : $loop->the_post();
				?>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="box-noticia">
									<?php if( has_post_thumbnail()): ?>
										<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); ?>
										<img src="<?php echo $large_image_url[0]; ?>" alt="">
								    <?php endif; ?>
									<h4><?php echo get_the_title(); ?></h4>
									<p><?php echo get_field('breve_descricao'); ?></p>
									<a class="btn-leiamais" href="<?php echo get_permalink(); ?>"><button>Leia mais</button></a>
								</div>
							</div>
				<?php
						endwhile;
						wp_reset_query();
					endif;
				?>

			</div>
		</div>
	</section>

	<section class="depoimentos">
		<div class="container">
			<div class="row">
				<?php showDepoimentos(); ?>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
    