<?php

/*
  Plugin Name: Módulo de Tratamentos
  Description: Módulo de Tratamentos
  Version: 1.0
  Author: Victor Magalhães
  Author URI: http://magalhaesvictor.com.br
 */

add_action("init", "moduleTratamentos");

function moduleTratamentos() {
    $init_class = new ModuleTratamentos();
    $init_class->init();
}

class ModuleTratamentos {

    function ModuleTratamentos() {
        
    }

	

    function init() {
	
		$labels = array(
			'name' => 'Serviçoes / Tratamentos',
			'singular_name' => 'Tratamento',
			'add_new' => 'Adicionar novo',
			'add_new_item' => 'Adicionar novo tratamento',
			'edit_item' => 'Editar Tratamento',
			'new_item' => 'Novo Tratamento',
			'all_items' => 'Todos os Tratamentos',
			'view_item' => 'Ver Tratamento',
			'search_items' => 'Pesquisar Tratamento',
			'not_found' =>  'Nenhum tratamento encontrado',
			'not_found_in_trash' => 'Nenhum tratamento encontrado no lixo',
			'parent_item_colon' => 'Tratamento pai',
			'menu_name' => 'Serviços / Tratamentos'
		);
		$args = array(
			'labels' => $labels,
			'description' => '',
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 25,
			'menu_icon' => WP_PLUGIN_URL.'/module-tratamentos/icon.png',
			'hierarchical' => false,
			'supports' => array(
				'title'
			),
			'has_archive' => true,
			'rewrite' => array(
				'slug' => 'tratamentos',
				'with_front' => false,
				'feeds' => true,
				'pages' => true,
			),
			'query_var' => 'tra]',
			'can_export' => true,
			'show_in_nav_menus' => false,
			'capability_type' => 'post',
		);
		
        register_post_type('tratamentos',$args);
		
		// Da um flush na geração de regras de navegação
        flush_rewrite_rules();

    }

}

?>