-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 25/06/2020 às 15:20
-- Versão do servidor: 5.7.30
-- Versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `odontop_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_events`
--

CREATE TABLE `wp_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_failed_logins`
--

CREATE TABLE `wp_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '1000-10-00 10:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_aiowps_failed_logins`
--

INSERT INTO `wp_aiowps_failed_logins` (`id`, `user_id`, `user_login`, `failed_login_date`, `login_attempt_ip`) VALUES
(1, 0, 'admin', '2020-02-05 17:28:27', '134.209.150.200'),
(2, 0, 'admin', '2020-02-05 17:30:03', '69.162.73.82'),
(3, 0, 'odontop', '2020-02-05 17:30:54', '54.176.188.51'),
(4, 0, 'odontop', '2020-04-14 14:07:04', '177.82.26.240'),
(5, 0, 'odontop', '2020-04-15 10:43:48', '177.82.26.240'),
(6, 0, 'gran@gran.ag', '2020-04-15 10:49:40', '191.245.85.139');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_global_meta`
--

CREATE TABLE `wp_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_login_activity`
--

CREATE TABLE `wp_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '1000-10-00 10:00:00',
  `logout_date` datetime NOT NULL DEFAULT '1000-10-00 10:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_aiowps_login_activity`
--

INSERT INTO `wp_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'clinicaodontop', '2020-02-05 17:26:15', '2020-02-05 17:26:32', '179.186.178.203', '', ''),
(2, 1, 'clinicaodontop', '2020-03-11 16:33:47', '1000-10-00 10:00:00', '191.179.227.206', '', ''),
(3, 1, 'clinicaodontop', '2020-04-23 09:44:44', '2020-04-23 10:01:39', '177.82.26.240', '', ''),
(4, 1, 'clinicaodontop', '2020-04-23 10:12:21', '2020-04-23 10:29:14', '177.82.26.240', '', ''),
(5, 1, 'clinicaodontop', '2020-05-25 14:23:18', '2020-05-25 14:38:17', '177.82.26.240', '', ''),
(6, 3, 'gran', '2020-05-25 14:38:25', '1000-10-00 10:00:00', '177.82.26.240', '', ''),
(7, 3, 'gran', '2020-06-25 15:11:58', '1000-10-00 10:00:00', '177.82.26.240', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_login_lockdown`
--

CREATE TABLE `wp_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `release_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_aiowps_login_lockdown`
--

INSERT INTO `wp_aiowps_login_lockdown` (`id`, `user_id`, `user_login`, `lockdown_date`, `release_date`, `failed_login_ip`, `lock_reason`, `unlock_key`) VALUES
(1, 0, 'admin', '2020-02-05 17:30:03', '2020-02-05 18:30:03', '69.162.73.82', 'login_fail', ''),
(2, 0, 'odontop', '2020-02-05 17:30:54', '2020-02-05 18:30:54', '54.176.188.51', 'login_fail', ''),
(3, 0, 'odontop', '2020-04-14 14:07:04', '2020-04-14 15:07:04', '177.82.26.240', 'login_fail', ''),
(4, 0, 'odontop', '2020-04-15 10:43:48', '2020-04-15 11:43:48', '177.82.26.240', 'login_fail', ''),
(5, 0, 'gran@gran.ag', '2020-04-15 10:49:40', '2020-04-15 11:49:40', '191.245.85.139', 'login_fail', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_aiowps_permanent_block`
--

CREATE TABLE `wp_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://clinicaodontop.com', 'yes'),
(2, 'home', 'https://clinicaodontop.com', 'yes'),
(3, 'blogname', 'Odontop', 'yes'),
(4, 'blogdescription', 'Clínica Odontop', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'willersonp@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:180:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:13:\"depoimento/?$\";s:31:\"index.php?post_type=depoimentos\";s:43:\"depoimento/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=depoimentos&feed=$matches[1]\";s:38:\"depoimento/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=depoimentos&feed=$matches[1]\";s:30:\"depoimento/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=depoimentos&paged=$matches[1]\";s:9:\"doutor/?$\";s:28:\"index.php?post_type=doutores\";s:39:\"doutor/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=doutores&feed=$matches[1]\";s:34:\"doutor/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=doutores&feed=$matches[1]\";s:26:\"doutor/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=doutores&paged=$matches[1]\";s:8:\"sedes/?$\";s:25:\"index.php?post_type=sedes\";s:38:\"sedes/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=sedes&feed=$matches[1]\";s:33:\"sedes/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=sedes&feed=$matches[1]\";s:25:\"sedes/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=sedes&paged=$matches[1]\";s:14:\"tratamentos/?$\";s:31:\"index.php?post_type=tratamentos\";s:44:\"tratamentos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=tratamentos&feed=$matches[1]\";s:39:\"tratamentos/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?post_type=tratamentos&feed=$matches[1]\";s:31:\"tratamentos/page/([0-9]{1,})/?$\";s:49:\"index.php?post_type=tratamentos&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:38:\"depoimento/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"depoimento/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"depoimento/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"depoimento/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"depoimento/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"depoimento/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"depoimento/([^/]+)/embed/?$\";s:44:\"index.php?depoimentos=$matches[1]&embed=true\";s:31:\"depoimento/([^/]+)/trackback/?$\";s:38:\"index.php?depoimentos=$matches[1]&tb=1\";s:51:\"depoimento/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?depoimentos=$matches[1]&feed=$matches[2]\";s:46:\"depoimento/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?depoimentos=$matches[1]&feed=$matches[2]\";s:39:\"depoimento/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?depoimentos=$matches[1]&paged=$matches[2]\";s:46:\"depoimento/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?depoimentos=$matches[1]&cpage=$matches[2]\";s:35:\"depoimento/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?depoimentos=$matches[1]&page=$matches[2]\";s:27:\"depoimento/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"depoimento/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"depoimento/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"depoimento/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"depoimento/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"depoimento/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"doutor/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"doutor/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"doutor/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"doutor/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"doutor/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"doutor/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"doutor/([^/]+)/embed/?$\";s:41:\"index.php?doutores=$matches[1]&embed=true\";s:27:\"doutor/([^/]+)/trackback/?$\";s:35:\"index.php?doutores=$matches[1]&tb=1\";s:47:\"doutor/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?doutores=$matches[1]&feed=$matches[2]\";s:42:\"doutor/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?doutores=$matches[1]&feed=$matches[2]\";s:35:\"doutor/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?doutores=$matches[1]&paged=$matches[2]\";s:42:\"doutor/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?doutores=$matches[1]&cpage=$matches[2]\";s:31:\"doutor/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?doutores=$matches[1]&page=$matches[2]\";s:23:\"doutor/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"doutor/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"doutor/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"doutor/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"doutor/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"doutor/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"sedes/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"sedes/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"sedes/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"sedes/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"sedes/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"sedes/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"sedes/([^/]+)/embed/?$\";s:38:\"index.php?sedes=$matches[1]&embed=true\";s:26:\"sedes/([^/]+)/trackback/?$\";s:32:\"index.php?sedes=$matches[1]&tb=1\";s:46:\"sedes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?sedes=$matches[1]&feed=$matches[2]\";s:41:\"sedes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?sedes=$matches[1]&feed=$matches[2]\";s:34:\"sedes/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?sedes=$matches[1]&paged=$matches[2]\";s:41:\"sedes/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?sedes=$matches[1]&cpage=$matches[2]\";s:30:\"sedes/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?sedes=$matches[1]&page=$matches[2]\";s:22:\"sedes/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"sedes/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"sedes/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"sedes/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"sedes/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"sedes/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"tratamentos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"tratamentos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"tratamentos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"tratamentos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"tratamentos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"tratamentos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"tratamentos/([^/]+)/embed/?$\";s:36:\"index.php?tra=$matches[1]&embed=true\";s:32:\"tratamentos/([^/]+)/trackback/?$\";s:30:\"index.php?tra=$matches[1]&tb=1\";s:52:\"tratamentos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tra=$matches[1]&feed=$matches[2]\";s:47:\"tratamentos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tra=$matches[1]&feed=$matches[2]\";s:40:\"tratamentos/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tra=$matches[1]&paged=$matches[2]\";s:47:\"tratamentos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:43:\"index.php?tra=$matches[1]&cpage=$matches[2]\";s:36:\"tratamentos/([^/]+)(?:/([0-9]+))?/?$\";s:42:\"index.php?tra=$matches[1]&page=$matches[2]\";s:28:\"tratamentos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"tratamentos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"tratamentos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"tratamentos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"tratamentos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"tratamentos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:14:{i:0;s:27:\"acf-gallery/acf-gallery.php\";i:1;s:29:\"acf-repeater/acf-repeater.php\";i:2;s:30:\"advanced-custom-fields/acf.php\";i:3;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:4;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:5;s:21:\"backwpup/backwpup.php\";i:6;s:33:\"duplicate-post/duplicate-post.php\";i:7;s:41:\"module-depoimentos/module_depoimentos.php\";i:8;s:35:\"module-doutores/module_doutores.php\";i:9;s:29:\"module-sedes/module_sedes.php\";i:10;s:41:\"module-tratamentos/module_tratamentos.php\";i:11;s:37:\"post-types-order/post-types-order.php\";i:12;s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";i:13;s:27:\"wp-pagenavi/wp-pagenavi.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:92:\"/home/odontop/public_html/wp-content/themes/odontop/partials/content-responsavel_tecnico.php\";i:2;s:62:\"/home/odontop/public_html/wp-content/themes/odontop/footer.php\";i:3;s:64:\"/home/odontop/public_html/wp-content/themes/odontop/category.php\";i:4;s:63:\"/home/odontop/public_html/wp-content/themes/odontop/archive.php\";i:5;s:68:\"/home/odontop/public_html/wp-content/themes/odontop/archive-blog.php\";}', 'no'),
(40, 'template', 'odontop', 'yes'),
(41, 'stylesheet', 'odontop', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:3:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}i:3;a:4:{s:5:\"title\";s:9:\"Categoria\";s:5:\"count\";i:1;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:546:\"<div class=\"agendar-blog\">\r\n   <div class=\"row\">\r\n      <div class=\"col-lg-4 col-md-4 col-sm-4 col-sm-12\">\r\n         <img src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/icon-calendar.png\" alt=\"\">\r\n      </div>\r\n      <div class=\"col-lg-8 col-md-8 col-sm-12 col-xs-12\">\r\n         <h3>Agende sua consulta sem compromisso.</h3>\r\n      </div>\r\n      <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n         <a class=\"btn-agendar\" href=\"http://clinicaodontop.com/contato\"><button>Agendar</button></a>\r\n      </div>\r\n   </div>\r\n</div>\";s:6:\"filter\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:2:{s:53:\"simple-custom-post-order/simple-custom-post-order.php\";s:18:\"scporder_uninstall\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:14:\"__return_false\";}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'wp_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:73:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:1;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:1;s:16:\"backwpup_restore\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:14:\"backwpup_admin\";a:2:{s:4:\"name\";s:14:\"BackWPup Admin\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:1;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:1;s:16:\"backwpup_restore\";b:1;}}s:14:\"backwpup_check\";a:2:{s:4:\"name\";s:21:\"BackWPup jobs checker\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:0;s:19:\"backwpup_jobs_start\";b:0;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:0;s:23:\"backwpup_backups_delete\";b:0;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:0;s:17:\"backwpup_settings\";b:0;s:16:\"backwpup_restore\";b:0;}}s:15:\"backwpup_helper\";a:2:{s:4:\"name\";s:23:\"BackWPup jobs functions\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:0;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:0;s:16:\"backwpup_restore\";b:0;}}}', 'yes'),
(93, 'WPLANG', 'pt_BR', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:3:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}i:3;a:3:{s:5:\"title\";s:8:\"Arquivos\";s:5:\"count\";i:1;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:4:{s:18:\"orphaned_widgets_1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:4:{i:0;s:6:\"text-2\";i:1;s:12:\"categories-3\";i:2;s:10:\"archives-3\";i:3;s:11:\"tag_cloud-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:2:{i:2;a:2:{s:5:\"title\";s:4:\"Tags\";s:8:\"taxonomy\";s:8:\"post_tag\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'cron', 'a:14:{i:1593109128;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593109159;a:1:{s:26:\"inpsyde_phone-home_checkin\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"twice_weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1209600;}}}i:1593109306;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1593109489;a:1:{s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1593116148;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593116323;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593116505;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593116506;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593116689;a:1:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1593134425;a:1:{s:22:\"backwpup_check_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1593152180;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1593175714;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1593648000;a:1:{s:13:\"backwpup_cron\";a:1:{s:32:\"2f35704147e4489fb9b8aeb31dbabaef\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{s:2:\"id\";i:1;}}}}s:7:\"version\";i:2;}', 'yes'),
(139, 'db_upgraded', '', 'yes'),
(143, 'theme_mods_twentysixteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1484073311;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(144, 'current_theme', 'Odontop', 'yes'),
(145, 'theme_mods_odontop', 'a:2:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(146, 'theme_switched', '', 'yes'),
(148, 'fresh_site', '0', 'yes'),
(149, 'recently_activated', 'a:0:{}', 'yes'),
(160, 'acf_version', '5.8.12', 'yes'),
(161, 'duplicate_post_copytitle', '1', 'yes'),
(162, 'duplicate_post_copydate', '', 'yes'),
(163, 'duplicate_post_copystatus', '1', 'yes'),
(164, 'duplicate_post_copyslug', '1', 'yes'),
(165, 'duplicate_post_copyexcerpt', '1', 'yes'),
(166, 'duplicate_post_copycontent', '1', 'yes'),
(167, 'duplicate_post_copythumbnail', '1', 'yes'),
(168, 'duplicate_post_copytemplate', '1', 'yes'),
(169, 'duplicate_post_copyformat', '1', 'yes'),
(170, 'duplicate_post_copyauthor', '', 'yes'),
(171, 'duplicate_post_copypassword', '', 'yes'),
(172, 'duplicate_post_copyattachments', '', 'yes'),
(173, 'duplicate_post_copychildren', '', 'yes'),
(174, 'duplicate_post_copycomments', '', 'yes'),
(175, 'duplicate_post_copymenuorder', '1', 'yes'),
(176, 'duplicate_post_taxonomies_blacklist', '', 'yes'),
(177, 'duplicate_post_blacklist', '', 'yes'),
(178, 'duplicate_post_types_enabled', 'a:6:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:11:\"depoimentos\";i:3;s:8:\"doutores\";i:4;s:5:\"sedes\";i:5;s:11:\"tratamentos\";}', 'yes'),
(179, 'duplicate_post_show_row', '1', 'yes'),
(180, 'duplicate_post_show_adminbar', '1', 'yes'),
(181, 'duplicate_post_show_submitbox', '1', 'yes'),
(182, 'duplicate_post_show_bulkactions', '1', 'yes'),
(187, 'duplicate_post_title_prefix', '', 'yes'),
(188, 'duplicate_post_title_suffix', '', 'yes'),
(189, 'duplicate_post_increase_menu_order_by', '', 'yes'),
(190, 'duplicate_post_roles', 'a:2:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";}', 'yes'),
(208, 'tto_options', 'a:3:{s:8:\"autosort\";s:1:\"1\";s:9:\"adminsort\";s:1:\"1\";s:10:\"capability\";s:15:\"install_plugins\";}', 'yes'),
(212, 'scporder_options', 'a:2:{s:7:\"objects\";a:5:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:8:\"doutores\";i:3;s:5:\"sedes\";i:4;s:8:\"servicos\";}s:4:\"tags\";s:0:\"\";}', 'yes'),
(215, 'cpto_options', 'a:6:{s:23:\"show_reorder_interfaces\";a:5:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"doutores\";s:4:\"show\";s:5:\"sedes\";s:4:\"show\";s:8:\"servicos\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:13:\"switch_themes\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(216, 'CPT_configured', 'TRUE', 'yes'),
(242, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:20:\"willersonp@gmail.com\";s:7:\"version\";s:5:\"5.4.2\";s:9:\"timestamp\";i:1591856204;}', 'no'),
(269, 'pagenavi_options', 'a:15:{s:10:\"pages_text\";s:0:\"\";s:12:\"current_text\";s:13:\"%PAGE_NUMBER%\";s:9:\"page_text\";s:13:\"%PAGE_NUMBER%\";s:10:\"first_text\";s:11:\"« Primeira\";s:9:\"last_text\";s:10:\"Última »\";s:9:\"prev_text\";s:4:\"&lt;\";s:9:\"next_text\";s:4:\"&gt;\";s:12:\"dotleft_text\";s:3:\"...\";s:13:\"dotright_text\";s:3:\"...\";s:9:\"num_pages\";i:5;s:23:\"num_larger_page_numbers\";i:3;s:28:\"larger_page_numbers_multiple\";i:10;s:11:\"always_show\";i:0;s:16:\"use_pagenavi_css\";i:1;s:5:\"style\";i:1;}', 'yes'),
(379, 'category_children', 'a:0:{}', 'yes'),
(12646, 'ai1wm_updater', 'a:0:{}', 'yes'),
(12660, 'duplicate_post_show_original_column', '0', 'yes'),
(12661, 'duplicate_post_show_original_in_post_states', '0', 'yes'),
(12662, 'duplicate_post_show_original_meta_box', '0', 'yes'),
(12663, 'duplicate_post_version', '3.2.4', 'yes'),
(12664, 'duplicate_post_show_notice', '0', 'no'),
(12671, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(12672, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(12673, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(12674, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(12675, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(12676, 'wp_page_for_privacy_policy', '0', 'yes'),
(12677, 'show_comments_cookies_opt_in', '1', 'yes'),
(12678, 'admin_email_lifespan', '1587218118', 'yes'),
(12682, 'recovery_keys', 'a:0:{}', 'yes'),
(12689, 'aiowpsec_db_version', '1.9', 'yes'),
(12690, 'aio_wp_security_configs', 'a:98:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:1:\"1\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:1:\"1\";s:28:\"aiowps_allow_unlock_requests\";s:1:\"1\";s:25:\"aiowps_max_login_attempts\";i:3;s:24:\"aiowps_retry_time_period\";i:5;s:26:\"aiowps_lockout_time_length\";i:60;s:28:\"aiowps_set_generic_login_msg\";s:1:\"1\";s:26:\"aiowps_enable_email_notify\";s:1:\"1\";s:20:\"aiowps_email_address\";s:11:\"dev@gran.ag\";s:27:\"aiowps_enable_forced_logout\";s:1:\"1\";s:25:\"aiowps_logout_time_period\";i:30;s:39:\"aiowps_enable_invalid_username_lockdown\";s:1:\"1\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"siahojzzdxsk2pne5lo9\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:1:\"1\";s:34:\"aiowps_enable_custom_login_captcha\";s:1:\"1\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:38:\"aiowps_enable_woo_lostpassword_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"qt0pmzw78oqg55rzfvzo\";s:42:\"aiowps_enable_manual_registration_approval\";s:1:\"1\";s:39:\"aiowps_enable_registration_page_captcha\";s:1:\"1\";s:35:\"aiowps_enable_registration_honeypot\";s:1:\"1\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:1:\"1\";s:26:\"aiowps_db_backup_frequency\";i:4;s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";i:2;s:32:\"aiowps_send_backup_email_address\";s:1:\"1\";s:27:\"aiowps_backup_email_address\";s:11:\"dev@gran.ag\";s:27:\"aiowps_disable_file_editing\";s:1:\"1\";s:37:\"aiowps_prevent_default_wp_file_access\";s:1:\"1\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:1:\"1\";s:27:\"aiowps_max_file_upload_size\";i:10;s:31:\"aiowps_enable_pingback_firewall\";s:1:\"1\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:1:\"1\";s:34:\"aiowps_block_debug_log_file_access\";s:1:\"1\";s:26:\"aiowps_disable_index_views\";s:1:\"1\";s:30:\"aiowps_disable_trace_and_track\";s:1:\"1\";s:28:\"aiowps_forbid_proxy_comments\";s:1:\"1\";s:29:\"aiowps_deny_bad_query_strings\";s:1:\"1\";s:34:\"aiowps_advanced_char_string_filter\";s:1:\"1\";s:25:\"aiowps_enable_5g_firewall\";s:1:\"1\";s:25:\"aiowps_enable_6g_firewall\";s:1:\"1\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:1:\"1\";s:28:\"aiowps_enable_404_IP_lockout\";s:1:\"1\";s:30:\"aiowps_404_lockout_time_length\";i:40;s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:1:\"1\";s:28:\"aiowps_enable_login_honeypot\";s:1:\"1\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:1:\"1\";s:29:\"aiowps_enable_comment_captcha\";s:1:\"1\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:1:\"1\";s:33:\"aiowps_spam_ip_min_comments_block\";i:3;s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:1:\"1\";s:25:\"aiowps_fcd_scan_frequency\";i:4;s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:1:\"1\";s:29:\"aiowps_fcd_scan_email_address\";s:11:\"dev@gran.ag\";s:27:\"aiowps_fcds_change_detected\";b:1;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:1:\"1\";s:32:\"aiowps_prevent_users_enumeration\";s:1:\"1\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";s:25:\"aiowps_recaptcha_site_key\";s:0:\"\";s:27:\"aiowps_recaptcha_secret_key\";s:0:\"\";s:24:\"aiowps_default_recaptcha\";s:0:\"\";s:28:\"aiowps_block_fake_googlebots\";s:1:\"1\";s:22:\"aiowps_login_page_slug\";s:6:\"painel\";s:23:\"aiowps_last_backup_time\";s:19:\"2020-06-24 19:41:09\";s:25:\"aiowps_last_fcd_scan_time\";s:19:\"2020-06-24 19:41:10\";s:19:\"aiowps_fcd_filename\";s:26:\"aiowps_fcd_data_enw4zgw5y5\";s:35:\"aiowps_enable_lost_password_captcha\";s:1:\"1\";}', 'yes'),
(13978, 'rlrsssl_options', 'a:16:{s:12:\"site_has_ssl\";b:1;s:4:\"hsts\";b:0;s:22:\"htaccess_warning_shown\";b:0;s:19:\"review_notice_shown\";b:1;s:25:\"ssl_success_message_shown\";b:1;s:26:\"autoreplace_insecure_links\";b:1;s:17:\"plugin_db_version\";s:5:\"3.3.4\";s:5:\"debug\";b:0;s:20:\"do_not_edit_htaccess\";b:0;s:17:\"htaccess_redirect\";b:0;s:11:\"ssl_enabled\";b:1;s:19:\"javascript_redirect\";b:0;s:11:\"wp_redirect\";b:1;s:31:\"switch_mixed_content_fixer_hook\";b:0;s:19:\"dismiss_all_notices\";b:0;s:21:\"dismiss_review_notice\";b:0;}', 'yes'),
(13986, 'rsssl_activation_timestamp', '1583955392', 'yes'),
(13987, 'rsssl_flush_rewrite_rules', '1583955392', 'yes'),
(15296, 'ai1wm_secret_key', 'VFEzXYpzl4LC', 'yes'),
(15297, 'ai1wm_status', 'a:2:{s:4:\"type\";s:8:\"download\";s:7:\"message\";s:328:\"<a href=\"https://clinicaodontop.com/wp-content/ai1wm-backups/clinicaodontop.com-20200525-174253-0kw2u6.wpress\" class=\"ai1wm-button-green ai1wm-emphasize ai1wm-button-download\" title=\"clinicaodontop.com\" download=\"clinicaodontop.com-20200525-174253-0kw2u6.wpress\"><span>Baixar clinicaodontop.com</span><em>Tamanho: 61 MB</em></a>\";}', 'yes'),
(15306, 'can_compress_scripts', '1', 'no'),
(15323, 'backwpup_cfg_hash', 'd8849b', 'no'),
(15324, 'backwpup_jobs', 'a:1:{i:1;a:38:{s:5:\"jobid\";i:1;s:10:\"backuptype\";s:7:\"archive\";s:4:\"type\";a:4:{i:0;s:6:\"DBDUMP\";i:1;s:4:\"FILE\";i:2;s:5:\"WPEXP\";i:3;s:8:\"WPPLUGIN\";}s:12:\"destinations\";a:1:{i:0;s:7:\"DROPBOX\";}s:4:\"name\";s:13:\"Job with ID 1\";s:14:\"mailaddresslog\";s:11:\"dev@gran.ag\";s:20:\"mailaddresssenderlog\";s:30:\"BackWPup Odontop <dev@gran.ag>\";s:13:\"mailerroronly\";b:1;s:13:\"archiveformat\";s:4:\".zip\";s:17:\"archiveencryption\";b:0;s:11:\"archivename\";s:24:\"%Y-%m-%d_%H-%i-%s_%hash%\";s:10:\"activetype\";s:6:\"wpcron\";s:10:\"cronselect\";s:5:\"basic\";s:4:\"cron\";s:10:\"0 21 * * 3\";s:11:\"fileexclude\";s:51:\".DS_Store,.git,.svn,.tmp,/node_modules/,desktop.ini\";s:10:\"dirinclude\";s:0:\"\";s:19:\"backupexcludethumbs\";b:0;s:18:\"backupspecialfiles\";b:1;s:10:\"backuproot\";b:1;s:17:\"backupabsfolderup\";b:0;s:13:\"backupcontent\";b:1;s:13:\"backupplugins\";b:1;s:12:\"backupthemes\";b:1;s:13:\"backupuploads\";b:1;s:21:\"backuprootexcludedirs\";a:0:{}s:24:\"backupcontentexcludedirs\";a:1:{i:0;s:7:\"upgrade\";}s:24:\"backuppluginsexcludedirs\";a:1:{i:0;s:8:\"backwpup\";}s:23:\"backupthemesexcludedirs\";a:0:{}s:24:\"backupuploadsexcludedirs\";a:0:{}s:12:\"dropboxtoken\";a:4:{s:12:\"access_token\";s:64:\"3RVm29K5KnAAAAAAAAAAMO-fFPjLPsNGdRsgUhwB2jNO8UTXRsOn-PA8Ryo25M4c\";s:10:\"token_type\";s:6:\"bearer\";s:3:\"uid\";s:10:\"2904327872\";s:10:\"account_id\";s:40:\"dbid:AABJMII7BYTV9nJ1TeNNhLBEvRNxcfR7Tkk\";}s:11:\"dropboxroot\";s:7:\"sandbox\";s:19:\"dropboxsyncnodelete\";b:0;s:17:\"dropboxmaxbackups\";i:2;s:10:\"dropboxdir\";s:9:\"/Odontop/\";s:7:\"lastrun\";i:1593033950;s:7:\"logfile\";s:110:\"/home/odontop/public_html/wp-content/uploads/backwpup-d8849b-logs/backwpup_log_dd08ca_2020-06-24_21-25-50.html\";s:21:\"lastbackupdownloadurl\";s:0:\"\";s:11:\"lastruntime\";i:253;}}', 'no'),
(15325, 'backwpup_version', '3.7.1', 'no'),
(15326, 'backwpup_cfg_showadminbar', '', 'no'),
(15327, 'backwpup_cfg_showfoldersize', '', 'no'),
(15328, 'backwpup_cfg_protectfolders', '1', 'no'),
(15329, 'backwpup_cfg_phone_home_client', '1', 'no'),
(15330, 'backwpup_cfg_jobmaxexecutiontime', '30', 'no'),
(15331, 'backwpup_cfg_jobstepretry', '3', 'no'),
(15332, 'backwpup_cfg_jobrunauthkey', '58464031', 'no'),
(15333, 'backwpup_cfg_loglevel', 'normal_translated', 'no'),
(15334, 'backwpup_cfg_jobwaittimems', '0', 'no'),
(15335, 'backwpup_cfg_jobdooutput', '0', 'no'),
(15336, 'backwpup_cfg_windows', '0', 'no'),
(15337, 'backwpup_cfg_maxlogs', '30', 'no'),
(15338, 'backwpup_cfg_gzlogs', '0', 'no'),
(15339, 'backwpup_cfg_logfolder', 'uploads/backwpup-d8849b-logs/', 'no'),
(15340, 'backwpup_cfg_httpauthuser', '', 'no'),
(15341, 'backwpup_cfg_httpauthpassword', '', 'no'),
(15343, 'inpsyde-phone-consent-given-BackWPup', 'a:4:{s:6:\"plugin\";s:8:\"BackWPup\";s:10:\"identifier\";s:32:\"b0f4ef90bf313cb26aeadab7250b0458\";s:11:\"php_version\";s:6:\"5.6.40\";s:10:\"wp_version\";s:3:\"5.4\";}', 'no'),
(15348, 'backwpup_messages', 'a:0:{}', 'no'),
(15388, '_site_transient_timeout_backwpup_1_dropbox', '1624581003', 'no'),
(15389, '_site_transient_backwpup_1_dropbox', 'a:1:{i:0;a:6:{s:6:\"folder\";s:8:\"/Odontop\";s:4:\"file\";s:43:\"/Odontop/2020-04-23_10-24-39_BLMIJG4P01.zip\";s:8:\"filename\";s:34:\"2020-04-23_10-24-39_BLMIJG4P01.zip\";s:11:\"downloadurl\";s:192:\"https://clinicaodontop.com/wp-admin/admin.php?page=backwpupbackups&action=downloaddropbox&file=/Odontop/2020-04-23_10-24-39_BLMIJG4P01.zip&local_file=2020-04-23_10-24-39_BLMIJG4P01.zip&jobid=1\";s:8:\"filesize\";i:30356864;s:4:\"time\";d:1587637508;}}', 'no'),
(15518, '_transient_health-check-site-status-result', '{\"good\":\"8\",\"recommended\":\"6\",\"critical\":\"3\"}', 'yes'),
(31287, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.4.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1593108981;s:15:\"version_checked\";s:5:\"5.4.2\";s:12:\"translations\";a:0:{}}', 'no'),
(32008, '_site_transient_timeout_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4', '1593176833', 'no'),
(32009, '_site_transient_php_check_a5907c2ea4d6fbd7e531b3aa7734f0e4', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:0;}', 'no'),
(33283, '_transient_timeout_users_online', '1593110634', 'no'),
(33284, '_transient_users_online', 'a:2:{i:0;a:3:{s:7:\"user_id\";i:3;s:13:\"last_activity\";i:1593097918;s:10:\"ip_address\";s:13:\"177.82.26.240\";}i:1;a:4:{s:7:\"user_id\";i:3;s:13:\"last_activity\";i:1593098034;s:10:\"ip_address\";s:13:\"177.82.26.240\";s:7:\"blog_id\";b:0;}}', 'no'),
(33286, '_site_transient_timeout_browser_44a97ba96b9033b5226c9e314accf4ba', '1593713519', 'no'),
(33287, '_site_transient_browser_44a97ba96b9033b5226c9e314accf4ba', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"83.0.4103.116\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(33294, '_transient_timeout_acf_plugin_updates', '1593281580', 'no'),
(33295, '_transient_acf_plugin_updates', 'a:3:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;}', 'no'),
(33297, '_transient_timeout_backwpup_notice_promoter', '1593152049', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(33298, '_transient_backwpup_notice_promoter', 'a:5:{s:2:\"en\";a:3:{s:7:\"content\";s:117:\"We celebrate 10 Mio BackWPup Downloads! Therefore, we offer 30% discount on BackWPup Pro. Only valid until June 30th!\";s:11:\"button-text\";s:12:\"Get 30% now!\";s:3:\"url\";s:122:\"https://backwpup.com/?utm_medium=banner&utm_source=adminnotice&utm_campaign=backwpup10m&utm_content=backwpup&utm_term=#buy\";}s:2:\"es\";a:3:{s:7:\"content\";s:151:\"¡Celebramos 10 millones de descargas de BackWPup! Para celebrarlo, ofrecemos un 30% de descuento en BackWPup Pro. ¡Solo válido hasta el 30 de junio!\";s:11:\"button-text\";s:35:\"¡Obtén un 30% de descuento ahora!\";s:3:\"url\";s:122:\"https://backwpup.com/?utm_medium=banner&utm_source=adminnotice&utm_campaign=backwpup10m&utm_content=backwpup&utm_term=#buy\";}s:2:\"fr\";a:3:{s:7:\"content\";s:117:\"We celebrate 10 Mio BackWPup Downloads! Therefore, we offer 30% discount on BackWPup Pro. Only valid until June 30th!\";s:11:\"button-text\";s:12:\"Get 30% now!\";s:3:\"url\";s:122:\"https://backwpup.com/?utm_medium=banner&utm_source=adminnotice&utm_campaign=backwpup10m&utm_content=backwpup&utm_term=#buy\";}s:2:\"de\";a:3:{s:7:\"content\";s:116:\"Wir feiern 10 Mio. BackWPup-Downloads! Deshalb bieten wir 30% Rabatt auf BackWPup Pro. Nur bis zum 30. Juni gültig!\";s:11:\"button-text\";s:25:\"Jetzt 30% Rabatt sichern!\";s:3:\"url\";s:122:\"https://backwpup.com/?utm_medium=banner&utm_source=adminnotice&utm_campaign=backwpup10m&utm_content=backwpup&utm_term=#buy\";}s:2:\"it\";a:3:{s:7:\"content\";s:144:\"Festeggiamo il traguardo di 10 milioni di downloads! Offriamo il 30% di sconto per l\'acquisto di BackWPup Pro. Offerta valida fino al 30 Giugno!\";s:11:\"button-text\";s:34:\"Acquista ora con il 30% di sconto!\";s:3:\"url\";s:122:\"https://backwpup.com/?utm_medium=banner&utm_source=adminnotice&utm_campaign=backwpup10m&utm_content=backwpup&utm_term=#buy\";}}', 'no'),
(33301, '_site_transient_timeout_theme_roots', '1593110617', 'no'),
(33302, '_site_transient_theme_roots', 'a:2:{s:7:\"odontop\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";}', 'no'),
(33308, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1593108981;s:7:\"checked\";a:14:{s:30:\"advanced-custom-fields/acf.php\";s:6:\"5.8.12\";s:27:\"acf-gallery/acf-gallery.php\";s:5:\"2.1.0\";s:29:\"acf-repeater/acf-repeater.php\";s:5:\"2.1.0\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"7.24\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.4.4\";s:21:\"backwpup/backwpup.php\";s:5:\"3.7.1\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.4\";s:41:\"module-depoimentos/module_depoimentos.php\";s:3:\"1.0\";s:35:\"module-doutores/module_doutores.php\";s:3:\"1.0\";s:29:\"module-sedes/module_sedes.php\";s:3:\"1.0\";s:41:\"module-tratamentos/module_tratamentos.php\";s:3:\"1.0\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.3\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:5:\"3.3.4\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:6:\"2.93.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:8:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"5.8.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.24\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.24.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2246309\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2246309\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2246309\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2246309\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.4.4\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"backwpup/backwpup.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/backwpup\";s:4:\"slug\";s:8:\"backwpup\";s:6:\"plugin\";s:21:\"backwpup/backwpup.php\";s:11:\"new_version\";s:5:\"3.7.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/backwpup/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/backwpup.3.7.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/backwpup/assets/icon-256x256.png?rev=1422084\";s:2:\"1x\";s:61:\"https://ps.w.org/backwpup/assets/icon-128x128.png?rev=1422084\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/backwpup/assets/banner-1544x500.png?rev=2324287\";s:2:\"1x\";s:63:\"https://ps.w.org/backwpup/assets/banner-772x250.png?rev=2324287\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.4.3\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.4.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/really-simple-ssl\";s:4:\"slug\";s:17:\"really-simple-ssl\";s:6:\"plugin\";s:47:\"really-simple-ssl/rlrsssl-really-simple-ssl.php\";s:11:\"new_version\";s:5:\"3.3.4\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/really-simple-ssl/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/really-simple-ssl.3.3.4.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/really-simple-ssl/assets/icon-128x128.png?rev=1782452\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/really-simple-ssl/assets/banner-1544x500.png?rev=2320223\";s:2:\"1x\";s:72:\"https://ps.w.org/really-simple-ssl/assets/banner-772x250.png?rev=2320228\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"wp-pagenavi/wp-pagenavi.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/wp-pagenavi\";s:4:\"slug\";s:11:\"wp-pagenavi\";s:6:\"plugin\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:11:\"new_version\";s:6:\"2.93.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/wp-pagenavi/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-pagenavi.2.93.3.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:55:\"https://ps.w.org/wp-pagenavi/assets/icon.svg?rev=977997\";s:3:\"svg\";s:55:\"https://ps.w.org/wp-pagenavi/assets/icon.svg?rev=977997\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-pagenavi/assets/banner-1544x500.jpg?rev=1206758\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-pagenavi/assets/banner-772x250.jpg?rev=1206758\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(33311, '_transient_timeout_rsssl_testpage', '1593109458', 'no'),
(33312, '_transient_rsssl_testpage', '<html>\n<head>\n    <meta charset=\"UTF-8\">\n    <META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">\n</head>\n<body>\n<h1>#SSL TEST PAGE#</h1>\n<p>This page is used purely to test for SSL availability.</p>\n#SERVER-HTTPS-ON# (on)<br>#SERVERPORT443#<br><br>#SUCCESSFULLY DETECTED SSL#\n</body>\n</html>\n', 'no'),
(33314, '_transient_timeout_rsssl_htaccess_test_success', '1593109477', 'no'),
(33315, '_transient_rsssl_htaccess_test_success', '', 'no'),
(33317, '_transient_timeout_rsssl_mixed_content_fixer_detected', '1593109460', 'no'),
(33318, '_transient_rsssl_mixed_content_fixer_detected', 'found', 'no'),
(33319, '_transient_timeout_rsssl_certinfo', '1593195277', 'no'),
(33320, '_transient_rsssl_certinfo', 'a:15:{s:4:\"name\";s:24:\"/CN=*.clinicaodontop.com\";s:7:\"subject\";a:1:{s:2:\"CN\";s:20:\"*.clinicaodontop.com\";}s:4:\"hash\";s:8:\"4feb31e3\";s:6:\"issuer\";a:3:{s:1:\"C\";s:2:\"US\";s:1:\"O\";s:13:\"Let\'s Encrypt\";s:2:\"CN\";s:26:\"Let\'s Encrypt Authority X3\";}s:7:\"version\";i:2;s:12:\"serialNumber\";s:42:\"327589718004167850363029876102176709336425\";s:9:\"validFrom\";s:13:\"200624015012Z\";s:7:\"validTo\";s:13:\"200922015012Z\";s:16:\"validFrom_time_t\";i:1592963412;s:14:\"validTo_time_t\";i:1600739412;s:15:\"signatureTypeSN\";s:10:\"RSA-SHA256\";s:15:\"signatureTypeLN\";s:23:\"sha256WithRSAEncryption\";s:16:\"signatureTypeNID\";i:668;s:8:\"purposes\";a:9:{i:1;a:3:{i:0;b:1;i:1;b:0;i:2;s:9:\"sslclient\";}i:2;a:3:{i:0;b:1;i:1;b:0;i:2;s:9:\"sslserver\";}i:3;a:3:{i:0;b:1;i:1;b:0;i:2;s:11:\"nssslserver\";}i:4;a:3:{i:0;b:0;i:1;b:0;i:2;s:9:\"smimesign\";}i:5;a:3:{i:0;b:0;i:1;b:0;i:2;s:12:\"smimeencrypt\";}i:6;a:3:{i:0;b:0;i:1;b:0;i:2;s:7:\"crlsign\";}i:7;a:3:{i:0;b:1;i:1;b:1;i:2;s:3:\"any\";}i:8;a:3:{i:0;b:1;i:1;b:0;i:2;s:10:\"ocsphelper\";}i:9;a:3:{i:0;b:0;i:1;b:0;i:2;s:13:\"timestampsign\";}}s:10:\"extensions\";a:9:{s:8:\"keyUsage\";s:35:\"Digital Signature, Key Encipherment\";s:16:\"extendedKeyUsage\";s:60:\"TLS Web Server Authentication, TLS Web Client Authentication\";s:16:\"basicConstraints\";s:8:\"CA:FALSE\";s:20:\"subjectKeyIdentifier\";s:59:\"38:F1:15:C7:EE:AF:B9:88:5D:76:72:BB:F4:5C:55:A9:5B:41:DE:DF\";s:22:\"authorityKeyIdentifier\";s:66:\"keyid:A8:4A:6A:63:04:7D:DD:BA:E6:D1:39:B7:A6:45:65:EF:F3:A8:EC:A1\n\";s:19:\"authorityInfoAccess\";s:99:\"OCSP - URI:http://ocsp.int-x3.letsencrypt.org\nCA Issuers - URI:http://cert.int-x3.letsencrypt.org/\n\";s:14:\"subjectAltName\";s:104:\"DNS:*.clinicaodontop.com, DNS:*.clinicaodontop.com.br, DNS:clinicaodontop.com, DNS:clinicaodontop.com.br\";s:19:\"certificatePolicies\";s:89:\"Policy: 2.23.140.1.2.1\nPolicy: 1.3.6.1.4.1.44947.1.1.1\n  CPS: http://cps.letsencrypt.org\n\";s:15:\"ct_precert_scts\";s:1161:\"Signed Certificate Timestamp:\n    Version   : v1(0)\n    Log ID    : B2:1E:05:CC:8B:A2:CD:8A:20:4E:87:66:F9:2B:B9:8A:\n                25:20:67:6B:DA:FA:70:E7:B2:49:53:2D:EF:8B:90:5E\n    Timestamp : Jun 24 02:50:12.402 2020 GMT\n    Extensions: none\n    Signature : ecdsa-with-SHA256\n                30:46:02:21:00:A2:D5:C0:18:A4:A6:62:A3:B0:D2:CA:\n                B4:F6:30:44:27:53:0B:12:21:8A:D5:BA:45:C0:BC:87:\n                D4:DD:5D:24:30:02:21:00:8A:9E:FA:98:73:D2:83:BB:\n                D7:65:FA:63:A7:4F:44:00:F4:ED:AA:B6:00:C8:AB:CE:\n                E4:1C:44:5A:7E:2B:93:B5\nSigned Certificate Timestamp:\n    Version   : v1(0)\n    Log ID    : 6F:53:76:AC:31:F0:31:19:D8:99:00:A4:51:15:FF:77:\n                15:1C:11:D9:02:C1:00:29:06:8D:B2:08:9A:37:D9:13\n    Timestamp : Jun 24 02:50:12.514 2020 GMT\n    Extensions: none\n    Signature : ecdsa-with-SHA256\n                30:46:02:21:00:ED:FE:71:60:EE:AA:77:80:46:B1:03:\n                CE:2E:EE:3E:AD:8A:15:07:40:12:63:66:DD:28:E7:36:\n                7B:D8:40:76:C6:02:21:00:ED:76:28:27:53:E1:58:EF:\n                1C:43:FF:AE:2E:18:E8:41:A2:EF:81:CA:63:AD:5C:A4:\n                1F:C1:97:D6:42:BA:AF:8C\";}}', 'no'),
(33323, 'rsssl_check_redirect_dismissed', '1', 'yes'),
(33327, '_transient_rsssl_plusone_count', '0', 'yes'),
(33349, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1593108981;s:7:\"checked\";a:2:{s:7:\"odontop\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.5\";}s:8:\"response\";a:1:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.6.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(33380, '_transient_doing_cron', '1593109159.8523359298706054687500', 'yes');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1487108540:1'),
(4, 6, '_edit_last', '1'),
(5, 6, '_edit_lock', '1487161450:1'),
(6, 4, '_wp_page_template', 'page-clinica.php'),
(7, 8, '_edit_last', '1'),
(8, 8, 'field_58754664571a6', 'a:11:{s:3:\"key\";s:19:\"field_58754664571a6\";s:5:\"label\";s:26:\"Por que escolher a odontop\";s:4:\"name\";s:26:\"por_que_escolher_a_odontop\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(9, 8, 'field_5875468d571a7', 'a:11:{s:3:\"key\";s:19:\"field_5875468d571a7\";s:5:\"label\";s:9:\"História\";s:4:\"name\";s:8:\"historia\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(10, 8, 'field_58754698571a8', 'a:11:{s:3:\"key\";s:19:\"field_58754698571a8\";s:5:\"label\";s:7:\"Valores\";s:4:\"name\";s:7:\"valores\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(11, 8, 'field_587546a3571a9', 'a:11:{s:3:\"key\";s:19:\"field_587546a3571a9\";s:5:\"label\";s:19:\"Formas de Pagamento\";s:4:\"name\";s:19:\"formas_de_pagamento\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(13, 8, 'position', 'normal'),
(14, 8, 'layout', 'no_box'),
(15, 8, 'hide_on_screen', 'a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}'),
(16, 8, '_edit_lock', '1484233663:1'),
(17, 8, 'field_58754713405f0', 'a:10:{s:3:\"key\";s:19:\"field_58754713405f0\";s:5:\"label\";s:7:\"Galeria\";s:4:\"name\";s:7:\"galeria\";s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(19, 9, '_edit_last', '1'),
(20, 9, 'field_58754bfb3a483', 'a:11:{s:3:\"key\";s:19:\"field_58754bfb3a483\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(21, 9, 'field_58754c083a484', 'a:13:{s:3:\"key\";s:19:\"field_58754c083a484\";s:5:\"label\";s:9:\"Endereço\";s:4:\"name\";s:8:\"endereco\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(22, 9, 'field_58754c2e3a485', 'a:13:{s:3:\"key\";s:19:\"field_58754c2e3a485\";s:5:\"label\";s:9:\"Horários\";s:4:\"name\";s:8:\"horarios\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:2:{i:0;a:15:{s:3:\"key\";s:19:\"field_58759cefddfc3\";s:5:\"label\";s:4:\"Dias\";s:4:\"name\";s:4:\"dias\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_58759cf6ddfc4\";s:5:\"label\";s:8:\"Horário\";s:4:\"name\";s:7:\"horario\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:18:\"Adicionar Registro\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(23, 9, 'field_58754c513a486', 'a:14:{s:3:\"key\";s:19:\"field_58754c513a486\";s:5:\"label\";s:8:\"Telefone\";s:4:\"name\";s:8:\"telefone\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(24, 9, 'field_58754c593a487', 'a:14:{s:3:\"key\";s:19:\"field_58754c593a487\";s:5:\"label\";s:8:\"Whatsapp\";s:4:\"name\";s:8:\"whatsapp\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(26, 9, 'position', 'normal'),
(27, 9, 'layout', 'no_box'),
(28, 9, 'hide_on_screen', 'a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}'),
(29, 9, '_edit_lock', '1484102784:1'),
(30, 10, '_wp_attached_file', '2017/01/img-escolha.png'),
(31, 10, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:570;s:6:\"height\";i:348;s:4:\"file\";s:23:\"2017/01/img-escolha.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(32, 11, '_wp_attached_file', '2017/01/img-escolha-1.png'),
(33, 11, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:570;s:6:\"height\";i:348;s:4:\"file\";s:25:\"2017/01/img-escolha-1.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(34, 12, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(35, 12, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(36, 12, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(37, 12, '_galeria', 'field_58754713405f0'),
(38, 12, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(39, 12, '_historia', 'field_5875468d571a7'),
(40, 12, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(41, 12, '_valores', 'field_58754698571a8'),
(42, 12, 'formas_de_pagamento', ''),
(43, 12, '_formas_de_pagamento', 'field_587546a3571a9'),
(44, 4, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(45, 4, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(46, 4, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(47, 4, '_galeria', 'field_58754713405f0'),
(48, 4, 'historia', 'Desde 2007 a Clínica Odontop está presente na região metropolitana de Curitiba-PR. Com com profissionais especializados para atender você cada vez melhor . Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.'),
(49, 4, '_historia', 'field_5875468d571a7'),
(50, 4, 'valores', '- Responsabilidade\r\n- Organização\r\n- Agilidade\r\n- Respeito'),
(51, 4, '_valores', 'field_58754698571a8'),
(52, 4, 'formas_de_pagamento', '23'),
(53, 4, '_formas_de_pagamento', 'field_587546a3571a9'),
(55, 13, '_edit_last', '1'),
(56, 13, '_edit_lock', '1487107640:1'),
(57, 14, '_wp_attached_file', '2017/01/img-sedes.png'),
(58, 14, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:21:\"2017/01/img-sedes.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(59, 13, 'imagem', '228'),
(60, 13, '_imagem', 'field_58754bfb3a483'),
(61, 13, 'endereco', 'Rua Emilio Jonhson, 145'),
(62, 13, '_endereco', 'field_58754c083a484'),
(63, 13, 'horario', 'Seg. à Sex. das 8h às 18h e Sáb. das 8h às 14h'),
(64, 13, '_horario', 'field_58754c2e3a485'),
(65, 13, 'telefone', '41 3699-0020'),
(66, 13, '_telefone', 'field_58754c513a486'),
(67, 13, 'whatsapp', 'Indisponível'),
(68, 13, '_whatsapp', 'field_58754c593a487'),
(121, 9, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"sedes\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(122, 13, 'horarios_0_dias', 'Segunda à Sexta'),
(123, 13, '_horarios_0_dias', 'field_58759cefddfc3'),
(124, 13, 'horarios_0_horario', '9h às 19h'),
(125, 13, '_horarios_0_horario', 'field_58759cf6ddfc4'),
(126, 13, 'horarios_1_dias', 'Sábado'),
(127, 13, '_horarios_1_dias', 'field_58759cefddfc3'),
(128, 13, 'horarios_1_horario', '10h às 17h'),
(129, 13, '_horarios_1_horario', 'field_58759cf6ddfc4'),
(130, 13, 'horarios', '2'),
(131, 13, '_horarios', 'field_58754c2e3a485'),
(144, 19, 'imagem', '226'),
(145, 19, '_imagem', 'field_58754bfb3a483'),
(146, 19, 'endereco', 'Rua Leonardo Francischelli, 1050 - Jardim Paulista'),
(147, 19, '_endereco', 'field_58754c083a484'),
(148, 19, 'horario', 'Seg. à Sex. das 8h às 18h e Sáb. das 8h às 14h'),
(149, 19, '_horario', 'field_58754c2e3a485'),
(150, 19, 'telefone', '41 3679-7908'),
(151, 19, '_telefone', 'field_58754c513a486'),
(152, 19, 'whatsapp', 'Indisponível'),
(153, 19, '_whatsapp', 'field_58754c593a487'),
(154, 19, 'horarios_0_dias', 'Segunda à Sexta'),
(155, 20, 'imagem', '227'),
(156, 19, '_horarios_0_dias', 'field_58759cefddfc3'),
(157, 20, '_imagem', 'field_58754bfb3a483'),
(158, 19, 'horarios_0_horario', '9h às 19h'),
(159, 20, 'endereco', 'Rua Domingos de Faria, 94'),
(160, 19, '_horarios_0_horario', 'field_58759cf6ddfc4'),
(161, 20, '_endereco', 'field_58754c083a484'),
(162, 19, 'horarios_1_dias', 'Sábado'),
(163, 20, 'horario', 'Seg. à Sex. das 8h às 18h e Sáb. das 8h às 14h'),
(164, 19, '_horarios_1_dias', 'field_58759cefddfc3'),
(165, 20, '_horario', 'field_58754c2e3a485'),
(166, 19, 'horarios_1_horario', '10h às 17h'),
(167, 20, 'telefone', '41 3652-1606'),
(168, 19, '_horarios_1_horario', 'field_58759cf6ddfc4'),
(169, 20, '_telefone', 'field_58754c513a486'),
(170, 19, 'horarios', '2'),
(171, 20, 'whatsapp', 'Indisponível'),
(172, 19, '_horarios', 'field_58754c2e3a485'),
(173, 20, '_whatsapp', 'field_58754c593a487'),
(174, 20, 'horarios_0_dias', 'Segunda à Sexta'),
(175, 21, 'imagem', '224'),
(176, 20, '_horarios_0_dias', 'field_58759cefddfc3'),
(177, 21, '_imagem', 'field_58754bfb3a483'),
(178, 19, '_dp_original', '13'),
(179, 20, 'horarios_0_horario', '9h às 19h'),
(180, 21, 'endereco', 'Rua Dom Pedro II, 554 - Centro'),
(181, 20, '_horarios_0_horario', 'field_58759cf6ddfc4'),
(182, 21, '_endereco', 'field_58754c083a484'),
(183, 20, 'horarios_1_dias', 'Sábado'),
(184, 21, 'horario', 'Seg. à Sex. das 8h às 18h e Sáb. das 8h às 14h'),
(185, 20, '_horarios_1_dias', 'field_58759cefddfc3'),
(186, 22, 'imagem', '225'),
(187, 21, '_horario', 'field_58754c2e3a485'),
(188, 20, 'horarios_1_horario', '10h às 17h'),
(189, 22, '_imagem', 'field_58754bfb3a483'),
(190, 20, '_horarios_1_horario', 'field_58759cf6ddfc4'),
(191, 21, 'telefone', '41 3672-5805'),
(192, 22, 'endereco', 'Rua Abel Scuissiato, 366, Sobreloja - Alto Maracanã'),
(193, 20, 'horarios', '2'),
(194, 21, '_telefone', 'field_58754c513a486'),
(195, 22, '_endereco', 'field_58754c083a484'),
(196, 20, '_horarios', 'field_58754c2e3a485'),
(197, 22, 'horario', 'Seg. à Sex. das 8h às 18h e Sáb. das 8h às 14h'),
(198, 21, 'whatsapp', 'Indisponível'),
(199, 20, '_dp_original', '13'),
(200, 22, '_horario', 'field_58754c2e3a485'),
(201, 21, '_whatsapp', 'field_58754c593a487'),
(202, 22, 'telefone', '41 3666-3161'),
(203, 21, 'horarios_0_dias', 'Segunda à Sexta'),
(204, 22, '_telefone', 'field_58754c513a486'),
(205, 21, '_horarios_0_dias', 'field_58759cefddfc3'),
(206, 22, 'whatsapp', 'Indisponível'),
(207, 21, 'horarios_0_horario', '9h às 19h'),
(208, 22, '_whatsapp', 'field_58754c593a487'),
(209, 21, '_horarios_0_horario', 'field_58759cf6ddfc4'),
(210, 22, 'horarios_0_dias', 'Segunda à Sexta'),
(211, 21, 'horarios_1_dias', 'Sábado'),
(212, 22, '_horarios_0_dias', 'field_58759cefddfc3'),
(213, 21, '_horarios_1_dias', 'field_58759cefddfc3'),
(214, 22, 'horarios_0_horario', '9h às 19h'),
(215, 21, 'horarios_1_horario', '10h às 17h'),
(216, 22, '_horarios_0_horario', 'field_58759cf6ddfc4'),
(217, 21, '_horarios_1_horario', 'field_58759cf6ddfc4'),
(218, 22, 'horarios_1_dias', 'Sábado'),
(219, 21, 'horarios', '2'),
(220, 22, '_horarios_1_dias', 'field_58759cefddfc3'),
(221, 21, '_horarios', 'field_58754c2e3a485'),
(222, 22, 'horarios_1_horario', '9h às 17h'),
(223, 22, '_horarios_1_horario', 'field_58759cf6ddfc4'),
(224, 22, 'horarios', '2'),
(225, 22, '_horarios', 'field_58754c2e3a485'),
(226, 22, '_dp_original', '13'),
(227, 21, '_dp_original', '13'),
(228, 20, '_edit_last', '1'),
(229, 20, '_edit_lock', '1487107771:1'),
(230, 19, '_edit_last', '1'),
(231, 19, '_edit_lock', '1487107758:1'),
(232, 21, '_edit_last', '1'),
(233, 21, '_edit_lock', '1487107605:1'),
(234, 22, '_edit_last', '1'),
(235, 22, '_edit_lock', '1487107736:1'),
(236, 23, '_wp_attached_file', '2017/01/img-cartoes.png'),
(237, 23, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:630;s:6:\"height\";i:50;s:4:\"file\";s:23:\"2017/01/img-cartoes.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(238, 24, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(239, 24, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(240, 24, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(241, 24, '_galeria', 'field_58754713405f0'),
(242, 24, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(243, 24, '_historia', 'field_5875468d571a7'),
(244, 24, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(245, 24, '_valores', 'field_58754698571a8'),
(246, 24, 'formas_de_pagamento', '23'),
(247, 24, '_formas_de_pagamento', 'field_587546a3571a9'),
(249, 25, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(250, 25, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(251, 25, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(252, 25, '_galeria', 'field_58754713405f0'),
(253, 25, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(254, 25, '_historia', 'field_5875468d571a7'),
(255, 25, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(256, 25, '_valores', 'field_58754698571a8'),
(257, 25, 'formas_de_pagamento', '23'),
(258, 25, '_formas_de_pagamento', 'field_587546a3571a9'),
(260, 26, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(261, 26, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(262, 26, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(263, 26, '_galeria', 'field_58754713405f0'),
(264, 26, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(265, 26, '_historia', 'field_5875468d571a7'),
(266, 26, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(267, 26, '_valores', 'field_58754698571a8'),
(268, 26, 'formas_de_pagamento2', '23'),
(269, 26, '_formas_de_pagamento2', 'field_587546a3571a9'),
(270, 4, 'formas_de_pagamento2', '14'),
(271, 4, '_formas_de_pagamento2', 'field_587546a3571a9'),
(272, 27, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(273, 27, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(274, 27, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(275, 27, '_galeria', 'field_58754713405f0'),
(276, 27, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(277, 27, '_historia', 'field_5875468d571a7'),
(278, 27, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(279, 27, '_valores', 'field_58754698571a8'),
(280, 27, 'formas_de_pagamento2', '14'),
(281, 27, '_formas_de_pagamento2', 'field_587546a3571a9'),
(283, 28, '_edit_last', '1'),
(284, 28, 'field_587624ed8eb9b', 'a:11:{s:3:\"key\";s:19:\"field_587624ed8eb9b\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(285, 28, 'field_587624f88eb9c', 'a:14:{s:3:\"key\";s:19:\"field_587624f88eb9c\";s:5:\"label\";s:3:\"CRM\";s:4:\"name\";s:3:\"crm\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(286, 28, 'field_587625038eb9d', 'a:13:{s:3:\"key\";s:19:\"field_587625038eb9d\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(287, 28, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"doutores\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(288, 28, 'position', 'normal'),
(289, 28, 'layout', 'no_box'),
(290, 28, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(291, 28, '_edit_lock', '1484137752:1'),
(292, 30, '_edit_last', '1'),
(293, 30, '_edit_lock', '1487959901:1'),
(294, 31, '_wp_attached_file', '2017/01/img-dr.png'),
(295, 31, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:158;s:6:\"height\";i:158;s:4:\"file\";s:18:\"2017/01/img-dr.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(296, 32, '_wp_attached_file', '2017/01/img-tratamento.png'),
(297, 32, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:750;s:6:\"height\";i:298;s:4:\"file\";s:26:\"2017/01/img-tratamento.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(298, 30, 'imagem', '32'),
(299, 30, '_imagem', 'field_587624ed8eb9b'),
(300, 30, 'crm', '1234'),
(301, 30, '_crm', 'field_587624f88eb9c'),
(302, 30, 'descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(303, 30, '_descricao', 'field_587625038eb9d'),
(304, 33, '_edit_last', '1'),
(305, 33, '_edit_lock', '1487104885:1'),
(306, 33, 'imagem', '211'),
(307, 33, '_imagem', 'field_587624ed8eb9b'),
(308, 33, 'crm', '4321'),
(309, 33, '_crm', 'field_587624f88eb9c'),
(310, 33, 'descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(311, 33, '_descricao', 'field_587625038eb9d'),
(312, 34, '_edit_last', '1'),
(313, 34, '_edit_lock', '1484137843:1'),
(314, 34, 'imagem', '32'),
(315, 34, '_imagem', 'field_587624ed8eb9b'),
(316, 34, 'crm', '1111'),
(317, 34, '_crm', 'field_587624f88eb9c'),
(318, 34, 'descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(319, 34, '_descricao', 'field_587625038eb9d'),
(331, 36, '_edit_last', '1'),
(332, 36, 'field_58762b248a4ec', 'a:11:{s:3:\"key\";s:19:\"field_58762b248a4ec\";s:5:\"label\";s:5:\"Icone\";s:4:\"name\";s:5:\"icone\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(333, 36, 'field_58762b4c8a4ed', 'a:13:{s:3:\"key\";s:19:\"field_58762b4c8a4ed\";s:5:\"label\";s:17:\"Breve Descrição\";s:4:\"name\";s:15:\"breve_descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(335, 36, 'position', 'normal'),
(336, 36, 'layout', 'no_box'),
(337, 36, 'hide_on_screen', 'a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}'),
(338, 36, '_edit_lock', '1484155359:1'),
(339, 37, '_edit_last', '1'),
(340, 37, '_edit_lock', '1484155098:1'),
(341, 38, '_wp_attached_file', '2017/01/icon-servico6.png'),
(342, 38, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:88;s:6:\"height\";i:76;s:4:\"file\";s:25:\"2017/01/icon-servico6.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 39, '_wp_attached_file', '2017/01/icon-servico1.png'),
(344, 39, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:56;s:6:\"height\";i:71;s:4:\"file\";s:25:\"2017/01/icon-servico1.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(345, 40, '_wp_attached_file', '2017/01/icon-servico2.png'),
(346, 40, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:56;s:6:\"height\";i:71;s:4:\"file\";s:25:\"2017/01/icon-servico2.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(347, 41, '_wp_attached_file', '2017/01/icon-servico3.png'),
(348, 41, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:56;s:6:\"height\";i:71;s:4:\"file\";s:25:\"2017/01/icon-servico3.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(349, 42, '_wp_attached_file', '2017/01/icon-servico4.png'),
(350, 42, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:70;s:6:\"height\";i:71;s:4:\"file\";s:25:\"2017/01/icon-servico4.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(351, 43, '_wp_attached_file', '2017/01/icon-servico5.png'),
(352, 43, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:56;s:6:\"height\";i:71;s:4:\"file\";s:25:\"2017/01/icon-servico5.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(353, 37, 'icone', '39'),
(354, 37, '_icone', 'field_58762b248a4ec'),
(355, 37, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(356, 37, '_descricao', 'field_58763ee1b0460'),
(357, 44, '_edit_last', '1'),
(358, 44, '_edit_lock', '1484154627:1'),
(359, 44, 'icone', '40'),
(360, 44, '_icone', 'field_58762b248a4ec'),
(361, 44, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(362, 44, '_descricao', 'field_58763ee1b0460'),
(363, 45, 'icone', '41'),
(364, 45, '_icone', 'field_58762b248a4ec'),
(365, 45, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(366, 45, '_descricao', 'field_58763ee1b0460'),
(367, 45, '_dp_original', '44'),
(368, 46, 'icone', '42'),
(369, 46, '_icone', 'field_58762b248a4ec'),
(370, 46, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(371, 46, '_descricao', 'field_58763ee1b0460'),
(372, 46, '_dp_original', '44'),
(373, 47, 'icone', '38'),
(374, 47, '_icone', 'field_58762b248a4ec'),
(375, 47, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(376, 47, '_descricao', 'field_58763ee1b0460'),
(377, 48, 'icone', '43'),
(378, 47, '_dp_original', '44'),
(379, 48, '_icone', 'field_58762b248a4ec'),
(380, 48, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(381, 48, '_descricao', 'field_58763ee1b0460'),
(382, 48, '_dp_original', '44'),
(383, 49, 'icone', ''),
(384, 49, '_icone', 'field_58762b248a4ec'),
(385, 49, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(386, 49, '_descricao', 'field_58763ee1b0460'),
(387, 49, '_dp_original', '44'),
(388, 45, '_edit_lock', '1484154661:1'),
(389, 45, '_edit_last', '1'),
(390, 46, '_edit_lock', '1484154660:1'),
(391, 46, '_edit_last', '1'),
(392, 48, '_edit_lock', '1484154660:1'),
(393, 48, '_edit_last', '1'),
(394, 47, '_edit_lock', '1484154659:1'),
(395, 47, '_edit_last', '1'),
(396, 49, '_edit_lock', '1484154658:1'),
(397, 49, '_edit_last', '1'),
(398, 50, '_edit_last', '1'),
(399, 50, '_edit_lock', '1484154654:1'),
(400, 50, 'icone', ''),
(401, 50, '_icone', 'field_58762b248a4ec'),
(402, 50, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(403, 50, '_descricao', 'field_58763ee1b0460'),
(404, 51, '_edit_last', '1'),
(405, 51, '_edit_lock', '1484154655:1'),
(406, 51, 'icone', ''),
(407, 51, '_icone', 'field_58762b248a4ec'),
(408, 51, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(409, 51, '_descricao', 'field_58763ee1b0460'),
(410, 52, '_edit_last', '1'),
(411, 52, '_edit_lock', '1484154656:1'),
(412, 52, 'icone', ''),
(413, 52, '_icone', 'field_58762b248a4ec'),
(414, 52, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(415, 52, '_descricao', 'field_58763ee1b0460'),
(416, 53, '_edit_last', '1'),
(417, 53, '_edit_lock', '1484154657:1'),
(418, 53, 'icone', ''),
(419, 53, '_icone', 'field_58762b248a4ec'),
(420, 53, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(421, 53, '_descricao', 'field_58763ee1b0460'),
(422, 54, '_edit_last', '1'),
(423, 54, 'field_5876373dd5321', 'a:11:{s:3:\"key\";s:19:\"field_5876373dd5321\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(424, 54, 'field_58763749d5322', 'a:14:{s:3:\"key\";s:19:\"field_58763749d5322\";s:5:\"label\";s:5:\"Cargo\";s:4:\"name\";s:5:\"cargo\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(425, 54, 'field_58763758d5323', 'a:13:{s:3:\"key\";s:19:\"field_58763758d5323\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(426, 54, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"depoimentos\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(427, 54, 'position', 'normal'),
(428, 54, 'layout', 'no_box'),
(429, 54, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(430, 54, '_edit_lock', '1484142306:1'),
(431, 55, '_edit_last', '1'),
(432, 55, '_edit_lock', '1484845649:1'),
(433, 56, '_wp_attached_file', '2017/01/img-depoimento.png'),
(434, 56, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:69;s:6:\"height\";i:69;s:4:\"file\";s:26:\"2017/01/img-depoimento.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 55, 'imagem', '142'),
(436, 55, '_imagem', 'field_5876373dd5321'),
(437, 55, 'cargo', 'Cabelereira'),
(438, 55, '_cargo', 'field_58763749d5322'),
(439, 55, 'descricao', 'Melhor clínica da minha região, com preço justo e dentistas atenciosos.'),
(440, 55, '_descricao', 'field_58763758d5323'),
(441, 57, '_edit_last', '1'),
(442, 57, '_edit_lock', '1484845537:1'),
(443, 57, 'imagem', '155'),
(444, 57, '_imagem', 'field_5876373dd5321'),
(445, 57, 'cargo', 'Autonomo'),
(446, 57, '_cargo', 'field_58763749d5322'),
(447, 57, 'descricao', 'Tratamento excelente, muito bom, recomendo a Odontop'),
(448, 57, '_descricao', 'field_58763758d5323'),
(449, 6, '_wp_page_template', 'page-contato.php'),
(451, 36, 'field_58763ed1b045f', 'a:11:{s:3:\"key\";s:19:\"field_58763ed1b045f\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(452, 36, 'field_58763ee1b0460', 'a:11:{s:3:\"key\";s:19:\"field_58763ee1b0460\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";s:3:\"yes\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(453, 36, 'field_58763eefb0461', 'a:10:{s:3:\"key\";s:19:\"field_58763eefb0461\";s:5:\"label\";s:7:\"Galeria\";s:4:\"name\";s:7:\"galeria\";s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(455, 37, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(456, 37, '_breve_descricao', 'field_58762b4c8a4ed'),
(457, 37, 'imagem', '32'),
(458, 37, '_imagem', 'field_58763ed1b045f'),
(459, 37, 'galeria', 'a:1:{i:0;s:2:\"58\";}'),
(460, 37, '_galeria', 'field_58763eefb0461'),
(461, 58, '_wp_attached_file', '2017/01/img-mini-tratamento.png'),
(462, 58, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:31:\"2017/01/img-mini-tratamento.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(463, 59, '_edit_last', '1'),
(464, 59, '_edit_lock', '1487105641:1'),
(465, 59, '_wp_page_template', 'default'),
(466, 44, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(467, 44, '_breve_descricao', 'field_58762b4c8a4ed'),
(468, 44, 'imagem', '32'),
(469, 44, '_imagem', 'field_58763ed1b045f'),
(470, 44, 'galeria', ''),
(471, 44, '_galeria', 'field_58763eefb0461'),
(472, 50, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(473, 50, '_breve_descricao', 'field_58762b4c8a4ed'),
(474, 50, 'imagem', '32'),
(475, 50, '_imagem', 'field_58763ed1b045f'),
(476, 50, 'galeria', ''),
(477, 50, '_galeria', 'field_58763eefb0461'),
(478, 51, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(479, 51, '_breve_descricao', 'field_58762b4c8a4ed'),
(480, 51, 'imagem', '32'),
(481, 51, '_imagem', 'field_58763ed1b045f'),
(482, 51, 'galeria', ''),
(483, 51, '_galeria', 'field_58763eefb0461'),
(484, 52, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(485, 52, '_breve_descricao', 'field_58762b4c8a4ed'),
(486, 52, 'imagem', '32'),
(487, 52, '_imagem', 'field_58763ed1b045f'),
(488, 52, 'galeria', ''),
(489, 52, '_galeria', 'field_58763eefb0461'),
(490, 53, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(491, 53, '_breve_descricao', 'field_58762b4c8a4ed'),
(492, 53, 'imagem', '32'),
(493, 53, '_imagem', 'field_58763ed1b045f'),
(494, 53, 'galeria', ''),
(495, 53, '_galeria', 'field_58763eefb0461'),
(496, 49, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(497, 49, '_breve_descricao', 'field_58762b4c8a4ed'),
(498, 49, 'imagem', '32'),
(499, 49, '_imagem', 'field_58763ed1b045f'),
(500, 49, 'galeria', ''),
(501, 49, '_galeria', 'field_58763eefb0461'),
(502, 47, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(503, 47, '_breve_descricao', 'field_58762b4c8a4ed'),
(504, 47, 'imagem', '32'),
(505, 47, '_imagem', 'field_58763ed1b045f'),
(506, 47, 'galeria', ''),
(507, 47, '_galeria', 'field_58763eefb0461'),
(508, 48, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(509, 48, '_breve_descricao', 'field_58762b4c8a4ed'),
(510, 48, 'imagem', '32'),
(511, 48, '_imagem', 'field_58763ed1b045f'),
(512, 48, 'galeria', ''),
(513, 48, '_galeria', 'field_58763eefb0461'),
(514, 46, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(515, 46, '_breve_descricao', 'field_58762b4c8a4ed'),
(516, 46, 'imagem', '32'),
(517, 46, '_imagem', 'field_58763ed1b045f'),
(518, 46, 'galeria', ''),
(519, 46, '_galeria', 'field_58763eefb0461'),
(520, 45, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(521, 45, '_breve_descricao', 'field_58762b4c8a4ed'),
(522, 45, 'imagem', '32'),
(523, 45, '_imagem', 'field_58763ed1b045f'),
(524, 45, 'galeria', ''),
(525, 45, '_galeria', 'field_58763eefb0461'),
(526, 36, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"tratamentos\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(527, 64, '_edit_last', '1'),
(528, 64, '_edit_lock', '1484854300:1'),
(529, 65, '_wp_attached_file', '2017/01/img-mini-tratamento-Copia-Copia-Copia.png'),
(530, 65, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:49:\"2017/01/img-mini-tratamento-Copia-Copia-Copia.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(531, 66, '_wp_attached_file', '2017/01/img-mini-tratamento-Copia-Copia.png'),
(532, 66, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:43:\"2017/01/img-mini-tratamento-Copia-Copia.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(533, 67, '_wp_attached_file', '2017/01/img-mini-tratamento-Copia-2-Copia.png'),
(534, 67, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:45:\"2017/01/img-mini-tratamento-Copia-2-Copia.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(535, 68, '_wp_attached_file', '2017/01/img-mini-tratamento-Copia-2.png'),
(536, 68, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:39:\"2017/01/img-mini-tratamento-Copia-2.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(537, 69, '_wp_attached_file', '2017/01/img-mini-tratamento-Copia.png'),
(538, 69, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:130;s:6:\"height\";i:130;s:4:\"file\";s:37:\"2017/01/img-mini-tratamento-Copia.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(539, 64, 'icone', '38'),
(540, 64, '_icone', 'field_58762b248a4ec'),
(541, 64, 'breve_descricao', 'Avaliado por um profissional, a extração é um procedimento cirúrgico que melhora o posicionamento dos dentes.'),
(542, 64, '_breve_descricao', 'field_58762b4c8a4ed'),
(543, 64, 'imagem', '180'),
(544, 64, '_imagem', 'field_58763ed1b045f'),
(545, 64, 'descricao', '<h4>Qual a função da extração do siso?</h4>\r\nConhecido popularmente como “dente do juízo”, o siso na maioria dos casos é um incômodo que surge a partir dos 20 anos de idade. São os últimos dentes a nascer e geralmente podem causar problemas, por não ter espaço suficiente na boca, ou por nascer torto e em alguns casos completamente virado.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nQuando ele começa a nascer, pode empurrar os demais dentes e se não extraído mudará a posição dos demais, causando muita dor e necessidade de tratamento ortodôntico para correção.\r\n\r\nEle também tem facilidade de gerar infecções pelo crescimento lento, a gengiva fica exposta por muito tempo até que o dente nasça completamente e, em alguns casos ele nem termina de crescer.\r\n\r\nAgende uma avaliação com nossos dentistas e previna-se de incômodos posteriores.\r\n<h5>Equipe Odontop</h5>'),
(546, 64, '_descricao', 'field_58763ee1b0460'),
(547, 64, 'galeria', 'a:4:{i:0;s:3:\"181\";i:1;s:3:\"182\";i:2;s:3:\"183\";i:3;s:3:\"184\";}'),
(548, 64, '_galeria', 'field_58763eefb0461'),
(549, 70, 'icone', ''),
(550, 70, '_icone', 'field_58762b248a4ec'),
(551, 70, 'breve_descricao', ''),
(552, 70, '_breve_descricao', 'field_58762b4c8a4ed'),
(553, 70, 'imagem', '32'),
(554, 70, '_imagem', 'field_58763ed1b045f'),
(555, 70, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(556, 70, '_descricao', 'field_58763ee1b0460'),
(557, 70, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(558, 70, '_galeria', 'field_58763eefb0461'),
(559, 70, '_dp_original', '64'),
(560, 71, 'icone', ''),
(561, 71, '_icone', 'field_58762b248a4ec'),
(562, 71, 'breve_descricao', ''),
(563, 71, '_breve_descricao', 'field_58762b4c8a4ed'),
(564, 71, 'imagem', '32'),
(565, 71, '_imagem', 'field_58763ed1b045f'),
(566, 71, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(567, 71, '_descricao', 'field_58763ee1b0460'),
(568, 71, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(569, 71, '_galeria', 'field_58763eefb0461'),
(570, 71, '_dp_original', '64'),
(571, 72, 'icone', ''),
(572, 72, '_icone', 'field_58762b248a4ec'),
(573, 72, 'breve_descricao', ''),
(574, 72, '_breve_descricao', 'field_58762b4c8a4ed'),
(575, 72, 'imagem', '32'),
(576, 72, '_imagem', 'field_58763ed1b045f'),
(577, 72, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(578, 72, '_descricao', 'field_58763ee1b0460'),
(579, 72, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(580, 73, 'icone', ''),
(581, 72, '_galeria', 'field_58763eefb0461'),
(582, 73, '_icone', 'field_58762b248a4ec'),
(583, 72, '_dp_original', '64'),
(584, 73, 'breve_descricao', ''),
(585, 73, '_breve_descricao', 'field_58762b4c8a4ed'),
(586, 73, 'imagem', '32'),
(587, 73, '_imagem', 'field_58763ed1b045f'),
(588, 73, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(589, 73, '_descricao', 'field_58763ee1b0460'),
(590, 73, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(591, 74, 'icone', ''),
(592, 73, '_galeria', 'field_58763eefb0461'),
(593, 74, '_icone', 'field_58762b248a4ec'),
(594, 73, '_dp_original', '64'),
(595, 74, 'breve_descricao', ''),
(596, 74, '_breve_descricao', 'field_58762b4c8a4ed'),
(597, 74, 'imagem', '32'),
(598, 74, '_imagem', 'field_58763ed1b045f'),
(599, 74, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(600, 74, '_descricao', 'field_58763ee1b0460'),
(601, 74, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(602, 74, '_galeria', 'field_58763eefb0461'),
(603, 74, '_dp_original', '64'),
(604, 71, '_edit_last', '1'),
(605, 71, '_wp_old_slug', 'extracao-de-siso-3'),
(606, 71, '_edit_lock', '1484854508:1'),
(607, 70, '_edit_last', '1'),
(608, 70, '_wp_old_slug', 'extracao-de-siso-2'),
(609, 70, '_edit_lock', '1484854501:1'),
(610, 72, '_edit_last', '1'),
(611, 72, '_wp_old_slug', 'extracao-de-siso-4'),
(612, 72, '_edit_lock', '1484854495:1'),
(613, 74, '_edit_last', '1'),
(614, 74, '_wp_old_slug', 'extracao-de-siso-6'),
(615, 74, '_edit_lock', '1484844453:1'),
(616, 73, '_edit_last', '1'),
(617, 73, '_wp_old_slug', 'extracao-de-siso-5'),
(618, 73, '_edit_lock', '1484854487:1'),
(619, 75, 'icone', '41'),
(620, 75, '_icone', 'field_58762b248a4ec'),
(621, 75, 'breve_descricao', 'As próteses proporcionam um sorriso natural de forma rápida e indolor. '),
(622, 75, '_breve_descricao', 'field_58762b4c8a4ed'),
(623, 75, 'imagem', '164'),
(624, 75, '_imagem', 'field_58763ed1b045f'),
(625, 75, 'descricao', '<h4>O que é prótese?</h4>\r\nÉ um produto moldado de acordo com a estrutura facial do paciente, que substitui os dentes perdidos ou extraídos. Ela pode ser móvel ou fixa, depende da escolha do paciente junto ao seu dentista.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nConhecidas por sua praticidade e custo reduzido, as próteses móveis alcançam uma grande massa de clientes. Sendo fixas ou removíveis, elas garantem a segurança durante a mastigação e na proteção da gengiva contra atritos.\r\nComo o processo de implante tem um custo bem elevado, a grande massa de pacientes que precisa de passar por este processo acaba optando pelas próteses removíveis. Com a grande procura o mercado se especializou cada vez mais, criando produtos mais resistentes e adaptáveis.\r\n\r\nHoje, com a gama de produtos para o uso diário garante ao paciente um sorriso seguro e harmonioso com mais facilidade.\r\n<h5>Equipe Odontop</h5>'),
(626, 75, '_descricao', 'field_58763ee1b0460'),
(627, 75, 'galeria', 'a:4:{i:0;s:3:\"165\";i:1;s:3:\"166\";i:2;s:3:\"167\";i:3;s:3:\"168\";}'),
(628, 75, '_galeria', 'field_58763eefb0461'),
(630, 75, '_wp_old_slug', 'extracao-de-siso-5'),
(631, 75, '_dp_original', '73'),
(632, 75, '_edit_last', '1'),
(633, 75, '_wp_old_slug', 'periodontia-2'),
(634, 75, '_edit_lock', '1484848134:1'),
(635, 76, 'icone', '42'),
(636, 76, '_icone', 'field_58762b248a4ec'),
(637, 76, 'breve_descricao', 'O clareamento dental recupera o branco natural de forma segura e confiável.'),
(638, 76, '_breve_descricao', 'field_58762b4c8a4ed'),
(639, 76, 'imagem', '169'),
(640, 76, '_imagem', 'field_58763ed1b045f'),
(641, 76, 'descricao', '<h4>Para que serve o clareamento dental?</h4>\r\nO clareamento pode aumentar a autoestima, garantindo um sorriso de aparência mais saudável. Seja o procedimento feito por gel (caseiro) ou laser (consultório), clareia os dentes de acordo com o número de sessões feitas.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nExistem dezenas de alimentos, bebidas e produtos com excesso de pigmentação, seja natural ou industrial que podem eventualmente manchar os dentes. Isso se dá pelo enfraquecimento do esmalte dental, que o torna mais poroso e absorve com mais facilidade a pigmentação que entra em contato com eles. Pelo contato constante com ácidos e bactérias, os dentes são diariamente atacados, a medida de prevenção mais comum para evitar o escurecimento é não deixar os dentes em contato direto com alimentos e bebidas de alta pigmentação.\r\nConsulte um dentista para definir quantas sessões serão necessárias para atingir o resultado esperado sem agredir seu sorriso.\r\n<h5>Equipe Odontop</h5>'),
(642, 76, '_descricao', 'field_58763ee1b0460'),
(643, 76, 'galeria', 'a:4:{i:0;s:3:\"170\";i:1;s:3:\"171\";i:2;s:3:\"172\";i:3;s:3:\"173\";}'),
(644, 76, '_galeria', 'field_58763eefb0461'),
(645, 76, '_wp_old_slug', 'extracao-de-siso-5'),
(646, 76, '_wp_old_slug', 'periodontia-2'),
(648, 76, '_dp_original', '75'),
(649, 77, 'icone', ''),
(650, 77, '_icone', 'field_58762b248a4ec'),
(651, 77, 'breve_descricao', ''),
(652, 77, '_breve_descricao', 'field_58762b4c8a4ed'),
(653, 77, 'imagem', '32'),
(654, 77, '_imagem', 'field_58763ed1b045f'),
(655, 77, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(656, 77, '_descricao', 'field_58763ee1b0460'),
(657, 77, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(658, 77, '_galeria', 'field_58763eefb0461'),
(659, 77, '_wp_old_slug', 'extracao-de-siso-5'),
(660, 77, '_wp_old_slug', 'periodontia-2'),
(662, 77, '_dp_original', '75'),
(663, 78, 'icone', '39'),
(664, 78, '_icone', 'field_58762b248a4ec'),
(665, 78, 'breve_descricao', 'Usado em funções estéticas, ele pode recriar um ou mais dentes e proporcionar um sorriso natural e harmonioso.'),
(666, 78, '_breve_descricao', 'field_58762b4c8a4ed'),
(667, 78, 'imagem', '143'),
(668, 78, '_imagem', 'field_58763ed1b045f'),
(669, 78, 'descricao', '<h4>O que é implantodontia?</h4>\r\nUm famoso recurso para substituir poucos dentes perdidos ou até a reconstrução completa do sorriso, como o implante de protocolo. Através da fixação de uma base de metal junto ao osso da arcada dentária, é possível recriar a função da raiz do dente e acoplar um dente substituto, feito sob medida de acordo com os demais, mantendo a harmonia estética e a segurança em seu sorriso.\r\n<h4>Por que realizar este procedimento?</h4>\r\nO implante tem conquistado cada vez mais os pacientes, alguns que usavam prótese por anos haviam se cansado da manutenção diárias, as possíveis quedas e preferiram migrar para o implante. Outros optam por esse procedimento por algum acidente, queda de poucos dentes ou por motivos estéticos.\r\n\r\nEste processo não tem limitação de pacientes, o importante é que ele com o auxílio de um profissional conheça qual a melhor solução para retomar o sorriso, então, agende uma consulta conosco.\r\n\r\nFicaremos felizes em te ajudar.\r\n<h5>Equipe Odontop</h5>'),
(670, 78, '_descricao', 'field_58763ee1b0460'),
(671, 78, 'galeria', 'a:4:{i:0;s:3:\"159\";i:1;s:3:\"160\";i:2;s:3:\"161\";i:3;s:3:\"162\";}'),
(672, 78, '_galeria', 'field_58763eefb0461'),
(673, 78, '_wp_old_slug', 'extracao-de-siso-5'),
(674, 78, '_wp_old_slug', 'periodontia-2'),
(676, 78, '_dp_original', '75'),
(677, 79, 'icone', '40'),
(678, 79, '_icone', 'field_58762b248a4ec'),
(679, 79, 'breve_descricao', 'A Ortodontia reposiciona os dentes, corrigindo imperfeições e solucionando problemas que podem ocasionar danos posteriores.'),
(680, 79, '_breve_descricao', 'field_58762b4c8a4ed'),
(681, 79, 'imagem', '32'),
(682, 79, '_imagem', 'field_58763ed1b045f'),
(683, 79, 'descricao', '<h4>O que é ortodontia?</h4>\r\nO procedimento ortodôntico é realizado por um profissional especializado, ele permite que durante o tempo de uso a arcada dentária se transforme, seja para corrigir a mordida, posicionamento dos dentes ou para corrigir pequenas imperfeições. Tudo dependerá do tempo estipulado pelo Ortodontista.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nExistem pessoas que ficam por anos com aparelhos, por isso o mercado se preocupou em criar modelos que se adaptassem cada vez mais ao dia a dia dos pacientes, já não existe somente o aparelho auto ligado, temos também o aparelho estético, que é feito com um material transparente e muito resistente. Tirando o ar de “sorriso metálico” este aparelho tem melhorado a autoestima das pessoas que ainda passam pelo tratamento.\r\nAinda contamos com o aparelho móvel e outras maneiras de corrigir o sorriso da maneira mais adaptável possível.\r\n<h5>Equipe Odontop</h5>\r\n&nbsp;'),
(684, 79, '_descricao', 'field_58763ee1b0460'),
(685, 79, 'galeria', 'a:4:{i:0;s:3:\"145\";i:1;s:3:\"146\";i:2;s:3:\"147\";i:3;s:3:\"149\";}'),
(686, 79, '_galeria', 'field_58763eefb0461'),
(687, 79, '_wp_old_slug', 'extracao-de-siso-5'),
(688, 79, '_wp_old_slug', 'periodontia-2'),
(690, 79, '_dp_original', '75'),
(691, 76, '_edit_last', '1'),
(692, 76, '_wp_old_slug', 'protese-2'),
(693, 76, '_edit_lock', '1484850567:1'),
(694, 77, '_edit_last', '1'),
(695, 77, '_wp_old_slug', 'protese-3'),
(696, 77, '_edit_lock', '1484854481:1'),
(697, 78, '_edit_last', '1'),
(698, 78, '_wp_old_slug', 'protese-4'),
(699, 78, '_edit_lock', '1484846581:1'),
(700, 79, '_edit_last', '1'),
(701, 79, '_wp_old_slug', 'protese-5'),
(702, 79, '_edit_lock', '1484843814:1'),
(703, 80, 'icone', '43'),
(704, 80, '_icone', 'field_58762b248a4ec'),
(705, 80, 'breve_descricao', 'Cuide do seu sorriso e o da sua família, mantenha as consultas periódicas em dia e garanta sua saúde bucal. '),
(706, 80, '_breve_descricao', 'field_58762b4c8a4ed'),
(707, 80, 'imagem', '174'),
(708, 80, '_imagem', 'field_58763ed1b045f'),
(709, 80, 'descricao', '<h4>Qual a função da clínica geral?</h4>\r\nO profissional de clínica geral odontológica garante por meio de consultas e avaliações periódicas a integridade da saúde bucal do seu paciente. Recomendável que 2 vezes ao ano, no mínimo, seja feita uma avaliação.\r\nEle é responsável pelas primeiras avaliações, em descobrir qual o real problema do paciente e encaminhar ao especialista necessário.\r\n<h4>Por que consultar com um clínico ?</h4>\r\nA importância em manter suas consultas regulares no dentista é imprescindível para que haja um histórico, de doenças, pré-disposições que auxiliem no tratamento se necessário.\r\nPor isso, não perca mais tempo. Aqui na Odontop temos profissionais especializados esperando por você. Marque sua consulta.\r\n<h5>Equipe Odontop</h5>'),
(710, 80, '_descricao', 'field_58763ee1b0460'),
(711, 80, 'galeria', 'a:4:{i:0;s:3:\"175\";i:1;s:3:\"176\";i:2;s:3:\"177\";i:3;s:3:\"179\";}'),
(712, 80, '_galeria', 'field_58763eefb0461'),
(713, 80, '_wp_old_slug', 'extracao-de-siso-5'),
(714, 80, '_wp_old_slug', 'periodontia-2'),
(715, 80, '_wp_old_slug', 'protese-4'),
(717, 80, '_dp_original', '78'),
(718, 80, '_edit_lock', '1484852379:1'),
(719, 80, '_edit_last', '1'),
(720, 80, '_wp_old_slug', 'implantodontia-2'),
(733, 95, '_edit_last', '1'),
(734, 95, '_edit_lock', '1487105550:1'),
(735, 95, '_wp_page_template', 'archive-blog.php'),
(740, 99, '_edit_last', '1'),
(741, 99, 'field_58776c0610352', 'a:13:{s:3:\"key\";s:19:\"field_58776c0610352\";s:5:\"label\";s:17:\"Breve Descrição\";s:4:\"name\";s:15:\"breve_descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:3:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:0:\"\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(743, 99, 'position', 'acf_after_title'),
(744, 99, 'layout', 'no_box'),
(745, 99, 'hide_on_screen', ''),
(746, 99, '_edit_lock', '1484235919:1'),
(747, 100, '_wp_attached_file', '2017/01/img-blog1.png'),
(748, 100, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:750;s:6:\"height\";i:328;s:4:\"file\";s:21:\"2017/01/img-blog1.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(764, 103, '_thumbnail_id', '193'),
(765, 103, 'breve_descricao', 'O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. '),
(766, 103, '_breve_descricao', 'field_58776c0610352'),
(767, 103, '_dp_original', '97'),
(776, 105, '_thumbnail_id', '190'),
(777, 105, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(780, 105, '_breve_descricao', 'field_58776c0610352'),
(782, 106, '_thumbnail_id', '197'),
(784, 106, 'breve_descricao', 'Conhecido popularmente como \"dente do juízo\", o siso tem uma notoriedade bem grande no meio dos jovens adultos. Responsável por inúmeras dores de cabeça, desconforto na mastigação e muitas vezes até entortando os demais dentes.'),
(786, 106, '_breve_descricao', 'field_58776c0610352'),
(788, 105, '_dp_original', '97'),
(794, 106, '_dp_original', '97'),
(795, 109, '_thumbnail_id', '186'),
(797, 109, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa...'),
(798, 109, '_breve_descricao', 'field_58776c0610352'),
(800, 109, '_dp_original', '97'),
(808, 103, '_edit_lock', '1487106733:1'),
(809, 103, '_edit_last', '1'),
(812, 115, '_wp_attached_file', '2017/01/icon-calendar.png'),
(813, 115, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:67;s:6:\"height\";i:69;s:4:\"file\";s:25:\"2017/01/icon-calendar.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(814, 115, '_edit_lock', '1484224857:1'),
(815, 8, 'field_58779c31a2e47', 'a:14:{s:3:\"key\";s:19:\"field_58779c31a2e47\";s:5:\"label\";s:8:\"Facebook\";s:4:\"name\";s:8:\"facebook\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(816, 8, 'rule', 'a:5:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:16:\"page-clinica.php\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(817, 116, 'por_que_escolher_a_odontop', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo, dolorum accusantium nam dolores, delectus harum aspernatur, vitae, similique earum suscipit aliquam dicta sit labore repudiandae. Placeat non, optio consequuntur!\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi nemo.'),
(818, 116, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(819, 116, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(820, 116, '_galeria', 'field_58754713405f0'),
(821, 116, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(822, 116, '_historia', 'field_5875468d571a7'),
(823, 116, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(824, 116, '_valores', 'field_58754698571a8'),
(825, 116, 'formas_de_pagamento', '23'),
(826, 116, '_formas_de_pagamento', 'field_587546a3571a9'),
(827, 116, 'facebook', 'https://www.facebook.com/'),
(828, 116, '_facebook', 'field_58779c31a2e47'),
(829, 4, 'facebook', 'www.facebook.com/odontopodonto'),
(830, 4, '_facebook', 'field_58779c31a2e47'),
(834, 118, '_edit_last', '1'),
(835, 118, '_edit_lock', '1487107462:1'),
(836, 118, '_wp_page_template', 'default'),
(837, 120, '_edit_last', '1'),
(838, 120, 'field_58779ceb62bf4', 'a:11:{s:3:\"key\";s:19:\"field_58779ceb62bf4\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(839, 120, 'field_58779cfc62bf5', 'a:13:{s:3:\"key\";s:19:\"field_58779cfc62bf5\";s:5:\"label\";s:7:\"Título\";s:4:\"name\";s:6:\"titulo\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(840, 120, 'field_58779d0462bf6', 'a:13:{s:3:\"key\";s:19:\"field_58779d0462bf6\";s:5:\"label\";s:10:\"Subtítulo\";s:4:\"name\";s:9:\"subtitulo\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(841, 120, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"118\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(842, 120, 'position', 'normal'),
(843, 120, 'layout', 'no_box'),
(844, 120, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(845, 120, '_edit_lock', '1484233866:1'),
(846, 121, '_wp_attached_file', '2017/01/banner1.png'),
(847, 121, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:1905;s:6:\"height\";i:635;s:4:\"file\";s:19:\"2017/01/banner1.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(848, 122, 'imagem', '121'),
(849, 122, '_imagem', 'field_58779ceb62bf4'),
(850, 122, 'titulo', 'ENTRE EM CONTATO E AGENDE SUA CONSULTA.'),
(851, 122, '_titulo', 'field_58779cfc62bf5'),
(852, 122, 'subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.'),
(853, 122, '_subtitulo', 'field_58779d0462bf6'),
(854, 118, 'imagem', '238'),
(855, 118, '_imagem', 'field_58779ceb62bf4'),
(856, 118, 'titulo', 'Tenha um sorriso de celebridade'),
(857, 118, '_titulo', 'field_58779cfc62bf5'),
(858, 118, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(859, 118, '_subtitulo', 'field_58779d0462bf6'),
(860, 123, '_edit_last', '1'),
(861, 123, '_edit_lock', '1487105347:1'),
(862, 123, '_wp_page_template', 'default'),
(863, 125, '_edit_last', '1'),
(864, 125, 'field_58779defed7b5', 'a:8:{s:3:\"key\";s:19:\"field_58779defed7b5\";s:5:\"label\";s:10:\"Destaque 1\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(865, 125, 'field_58779e05ed7b6', 'a:13:{s:3:\"key\";s:19:\"field_58779e05ed7b6\";s:5:\"label\";s:7:\"Título\";s:4:\"name\";s:16:\"destaque1_titulo\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(866, 125, 'field_58779e18ed7b7', 'a:13:{s:3:\"key\";s:19:\"field_58779e18ed7b7\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:19:\"destaque1_descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(867, 125, 'field_58779e25ed7b8', 'a:8:{s:3:\"key\";s:19:\"field_58779e25ed7b8\";s:5:\"label\";s:10:\"Destaque 2\";s:4:\"name\";s:0:\"\";s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(868, 125, 'field_58779e2eed7b9', 'a:13:{s:3:\"key\";s:19:\"field_58779e2eed7b9\";s:5:\"label\";s:7:\"Título\";s:4:\"name\";s:16:\"destaque2_titulo\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:4;}'),
(869, 125, 'field_58779e37ed7ba', 'a:13:{s:3:\"key\";s:19:\"field_58779e37ed7ba\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:19:\"destaque2_descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:5;}'),
(870, 125, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"123\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(871, 125, 'position', 'normal'),
(872, 125, 'layout', 'no_box'),
(873, 125, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(874, 125, '_edit_lock', '1484234189:1'),
(875, 126, 'destaque1_titulo', 'Agende sua consulta sem compromisso.'),
(876, 126, '_destaque1_titulo', 'field_58779e05ed7b6'),
(877, 126, 'destaque1_descricao', 'Lorem ipsum dolor sit amet consectetur adipiscing elit.'),
(878, 126, '_destaque1_descricao', 'field_58779e18ed7b7'),
(879, 126, 'destaque2_titulo', 'Agende sua consulta sem compromisso.'),
(880, 126, '_destaque2_titulo', 'field_58779e2eed7b9'),
(881, 126, 'destaque2_descricao', 'Lorem ipsum dolor sit amet consectetur adipiscing elit.'),
(882, 126, '_destaque2_descricao', 'field_58779e37ed7ba'),
(883, 123, 'destaque1_titulo', 'Agende já sua avaliação.'),
(884, 123, '_destaque1_titulo', 'field_58779e05ed7b6'),
(885, 123, 'destaque1_descricao', 'Agende uma avaliação com nossos dentistas e previna-se de incômodos posteriores.\r\n'),
(886, 123, '_destaque1_descricao', 'field_58779e18ed7b7'),
(887, 123, 'destaque2_titulo', 'Agende sua avaliação agora.'),
(888, 123, '_destaque2_titulo', 'field_58779e2eed7b9'),
(889, 123, 'destaque2_descricao', 'O sorriso que você e toda sua família merece. '),
(890, 123, '_destaque2_descricao', 'field_58779e37ed7ba'),
(897, 127, '_edit_last', '1'),
(898, 127, '_edit_lock', '1487105341:1'),
(899, 127, '_wp_page_template', 'default'),
(900, 129, '_edit_last', '1'),
(901, 129, 'field_5877a2ece5c26', 'a:13:{s:3:\"key\";s:19:\"field_5877a2ece5c26\";s:5:\"label\";s:5:\"Itens\";s:4:\"name\";s:5:\"itens\";s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:10:\"sub_fields\";a:3:{i:0;a:12:{s:3:\"key\";s:19:\"field_5877a311e5c28\";s:5:\"label\";s:5:\"Icone\";s:4:\"name\";s:5:\"icone\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}i:1;a:15:{s:3:\"key\";s:19:\"field_5877a31be5c29\";s:5:\"label\";s:7:\"Título\";s:4:\"name\";s:6:\"titulo\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}i:2;a:14:{s:3:\"key\";s:19:\"field_5877a322e5c2a\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:12:\"column_width\";s:0:\"\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:1:\"3\";s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:18:\"Adicionar Registro\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(902, 129, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"127\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(903, 129, 'position', 'normal'),
(904, 129, 'layout', 'no_box'),
(905, 129, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(906, 129, '_edit_lock', '1484235445:1'),
(907, 130, '_wp_attached_file', '2017/01/img-spe2.png'),
(908, 130, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:72;s:6:\"height\";i:72;s:4:\"file\";s:20:\"2017/01/img-spe2.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(909, 131, '_wp_attached_file', '2017/01/img-spe3.png'),
(910, 131, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:69;s:6:\"height\";i:69;s:4:\"file\";s:20:\"2017/01/img-spe3.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(911, 132, '_wp_attached_file', '2017/01/img-spe1.png');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(912, 132, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:69;s:6:\"height\";i:72;s:4:\"file\";s:20:\"2017/01/img-spe1.png\";s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(913, 133, 'itens_0_icone', '132'),
(914, 133, '_itens_0_icone', 'field_5877a311e5c28'),
(915, 133, 'itens_0_titulo', 'Saúde'),
(916, 133, '_itens_0_titulo', 'field_5877a31be5c29'),
(917, 133, 'itens_0_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(918, 133, '_itens_0_descricao', 'field_5877a322e5c2a'),
(919, 133, 'itens_1_icone', '130'),
(920, 133, '_itens_1_icone', 'field_5877a311e5c28'),
(921, 133, 'itens_1_titulo', 'Prevenção'),
(922, 133, '_itens_1_titulo', 'field_5877a31be5c29'),
(923, 133, 'itens_1_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(924, 133, '_itens_1_descricao', 'field_5877a322e5c2a'),
(925, 133, 'itens_2_icone', '131'),
(926, 133, '_itens_2_icone', 'field_5877a311e5c28'),
(927, 133, 'itens_2_titulo', 'Estética'),
(928, 133, '_itens_2_titulo', 'field_5877a31be5c29'),
(929, 133, 'itens_2_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'),
(930, 133, '_itens_2_descricao', 'field_5877a322e5c2a'),
(931, 133, 'itens', '3'),
(932, 133, '_itens', 'field_5877a2ece5c26'),
(933, 127, 'itens_0_icone', '132'),
(934, 127, '_itens_0_icone', 'field_5877a311e5c28'),
(935, 127, 'itens_0_titulo', 'Saúde'),
(936, 127, '_itens_0_titulo', 'field_5877a31be5c29'),
(937, 127, 'itens_0_descricao', 'Mantenha sua boca longe das doenças bucais com a nossa ajuda.'),
(938, 127, '_itens_0_descricao', 'field_5877a322e5c2a'),
(939, 127, 'itens_1_icone', '130'),
(940, 127, '_itens_1_icone', 'field_5877a311e5c28'),
(941, 127, 'itens_1_titulo', 'Prevenção'),
(942, 127, '_itens_1_titulo', 'field_5877a31be5c29'),
(943, 127, 'itens_1_descricao', 'Consulte o dentista regularmente em uma de nossas clínicas.'),
(944, 127, '_itens_1_descricao', 'field_5877a322e5c2a'),
(945, 127, 'itens_2_icone', '131'),
(946, 127, '_itens_2_icone', 'field_5877a311e5c28'),
(947, 127, 'itens_2_titulo', 'Estética'),
(948, 127, '_itens_2_titulo', 'field_5877a31be5c29'),
(949, 127, 'itens_2_descricao', 'Com a gente é sempre possível melhorar a aparência do seu sorriso.'),
(950, 127, '_itens_2_descricao', 'field_5877a322e5c2a'),
(951, 127, 'itens', '3'),
(952, 127, '_itens', 'field_5877a2ece5c26'),
(953, 99, 'field_5877a4ac873fb', 'a:10:{s:3:\"key\";s:19:\"field_5877a4ac873fb\";s:5:\"label\";s:26:\"Mostrar na Página Inicial\";s:4:\"name\";s:25:\"mostrar_na_pagina_inicial\";s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:7:\"message\";s:0:\"\";s:13:\"default_value\";s:1:\"0\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(955, 109, '_edit_lock', '1487618797:1'),
(956, 109, '_edit_last', '1'),
(959, 134, 'mostrar_na_pagina_inicial', ''),
(960, 134, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(961, 134, 'breve_descricao', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação'),
(962, 134, '_breve_descricao', 'field_58776c0610352'),
(963, 109, '_wp_old_slug', 'manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-6'),
(964, 109, 'mostrar_na_pagina_inicial', '1'),
(965, 109, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(966, 99, 'rule', 'a:5:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(969, 135, 'mostrar_na_pagina_inicial', '1'),
(970, 135, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(971, 135, 'breve_descricao', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação'),
(972, 135, '_breve_descricao', 'field_58776c0610352'),
(973, 106, '_edit_lock', '1484940012:1'),
(975, 106, '_edit_last', '1'),
(978, 136, 'mostrar_na_pagina_inicial', '1'),
(979, 136, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(980, 136, 'breve_descricao', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação'),
(981, 136, '_breve_descricao', 'field_58776c0610352'),
(982, 106, '_wp_old_slug', 'manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-4'),
(983, 106, 'mostrar_na_pagina_inicial', '1'),
(984, 106, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(999, 21, '_wp_old_slug', 'coritiba'),
(1000, 19, '_wp_old_slug', 'araucaria'),
(1001, 20, '_wp_old_slug', 'rio-branco'),
(1002, 140, '_wp_attached_file', '2017/01/Foto-em-07-11-16-às-11.44.jpg'),
(1003, 140, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:720;s:4:\"file\";s:39:\"2017/01/Foto-em-07-11-16-às-11.44.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"Foto-em-07-11-16-às-11.44-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:39:\"Foto-em-07-11-16-às-11.44-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:39:\"Foto-em-07-11-16-às-11.44-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:40:\"Foto-em-07-11-16-às-11.44-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:1:{i:0;s:11:\"Photo Booth\";}}}'),
(1004, 142, '_wp_attached_file', '2017/01/MARINALVA.jpg'),
(1005, 142, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:21:\"2017/01/MARINALVA.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1006, 143, '_wp_attached_file', '2017/01/odontop_implante.jpg'),
(1007, 143, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:28:\"2017/01/odontop_implante.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_implante-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_implante-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1018, 145, '_wp_attached_file', '2017/01/odontop_aparelho_estetico.jpg'),
(1019, 145, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:37:\"2017/01/odontop_aparelho_estetico.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"odontop_aparelho_estetico-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"odontop_aparelho_estetico-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1020, 146, '_wp_attached_file', '2017/01/odontop_aparelho_metalico.jpg'),
(1021, 146, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:37:\"2017/01/odontop_aparelho_metalico.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"odontop_aparelho_metalico-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"odontop_aparelho_metalico-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1022, 147, '_wp_attached_file', '2017/01/odontop_aparelho_móvel.jpg'),
(1023, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:36:\"2017/01/odontop_aparelho_móvel.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"odontop_aparelho_móvel-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"odontop_aparelho_móvel-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1034, 149, '_wp_attached_file', '2017/01/odontop_aparelho_autoligavel.jpg'),
(1035, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:40:\"2017/01/odontop_aparelho_autoligavel.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"odontop_aparelho_autoligavel-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"odontop_aparelho_autoligavel-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1036, 105, '_edit_lock', '1487619760:1'),
(1039, 150, 'mostrar_na_pagina_inicial', '1'),
(1040, 150, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1041, 150, 'breve_descricao', 'O flúor é um elemento que auxilia a dentição a permanecer protegida contra cáries, sua fórmula fortalece o esmalte dental.'),
(1042, 150, '_breve_descricao', 'field_58776c0610352'),
(1049, 151, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Desde (ano de início da clínica), buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1050, 151, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1051, 151, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1052, 151, '_galeria', 'field_58754713405f0'),
(1053, 151, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1054, 151, '_historia', 'field_5875468d571a7'),
(1055, 151, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1056, 151, '_valores', 'field_58754698571a8'),
(1057, 151, 'formas_de_pagamento', '23'),
(1058, 151, '_formas_de_pagamento', 'field_587546a3571a9'),
(1059, 151, 'facebook', 'https://www.facebook.com/'),
(1060, 151, '_facebook', 'field_58779c31a2e47'),
(1061, 153, 'itens_0_icone', '132'),
(1062, 153, '_itens_0_icone', 'field_5877a311e5c28'),
(1063, 153, 'itens_0_titulo', 'Saúde'),
(1064, 153, '_itens_0_titulo', 'field_5877a31be5c29'),
(1065, 153, 'itens_0_descricao', 'Mantenha sua boca longe das doenças bucais com a nossa ajuda.'),
(1066, 153, '_itens_0_descricao', 'field_5877a322e5c2a'),
(1067, 153, 'itens_1_icone', '130'),
(1068, 153, '_itens_1_icone', 'field_5877a311e5c28'),
(1069, 153, 'itens_1_titulo', 'Prevenção'),
(1070, 153, '_itens_1_titulo', 'field_5877a31be5c29'),
(1071, 153, 'itens_1_descricao', 'Mantenha sua visita regular ao dentista e um sorriso saudável.'),
(1072, 153, '_itens_1_descricao', 'field_5877a322e5c2a'),
(1073, 153, 'itens_2_icone', '131'),
(1074, 153, '_itens_2_icone', 'field_5877a311e5c28'),
(1075, 153, 'itens_2_titulo', 'Estética'),
(1076, 153, '_itens_2_titulo', 'field_5877a31be5c29'),
(1077, 153, 'itens_2_descricao', 'Com a gente é sempre possível melhorar a aparência do seu sorriso.'),
(1078, 153, '_itens_2_descricao', 'field_5877a322e5c2a'),
(1079, 153, 'itens', '3'),
(1080, 153, '_itens', 'field_5877a2ece5c26'),
(1081, 154, 'destaque1_titulo', 'Agende já sua avaliação.'),
(1082, 154, '_destaque1_titulo', 'field_58779e05ed7b6'),
(1083, 154, 'destaque1_descricao', 'Agende uma avaliação com nossos dentistas e previna-se de incômodos posteriores.\r\n'),
(1084, 154, '_destaque1_descricao', 'field_58779e18ed7b7'),
(1085, 154, 'destaque2_titulo', 'Agende sua avaliação agora.'),
(1086, 154, '_destaque2_titulo', 'field_58779e2eed7b9'),
(1087, 154, 'destaque2_descricao', 'O sorriso que você e toda sua família merece. '),
(1088, 154, '_destaque2_descricao', 'field_58779e37ed7ba'),
(1089, 155, '_wp_attached_file', '2017/01/cezar.jpg'),
(1090, 155, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:70;s:6:\"height\";i:70;s:4:\"file\";s:17:\"2017/01/cezar.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1093, 156, 'mostrar_na_pagina_inicial', '1'),
(1094, 156, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1095, 156, 'breve_descricao', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes'),
(1096, 156, '_breve_descricao', 'field_58776c0610352'),
(1105, 158, 'mostrar_na_pagina_inicial', '1'),
(1106, 158, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1107, 158, 'breve_descricao', 'O flúor é um elemento que auxilia a dentição a permanecer protegida contra cáries, sua fórmula fortalece o esmalte dental.'),
(1108, 158, '_breve_descricao', 'field_58776c0610352'),
(1109, 159, '_wp_attached_file', '2017/01/odontop_implante_protocolo.jpg'),
(1110, 159, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:38:\"2017/01/odontop_implante_protocolo.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"odontop_implante_protocolo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"odontop_implante_protocolo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1111, 160, '_wp_attached_file', '2017/01/odontop_implante2.jpg'),
(1112, 160, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:29:\"2017/01/odontop_implante2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_implante2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_implante2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1113, 161, '_wp_attached_file', '2017/01/odontop_implante3.jpg'),
(1114, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:29:\"2017/01/odontop_implante3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_implante3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_implante3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1115, 162, '_wp_attached_file', '2017/01/odontop_implante-1.jpg'),
(1116, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:30:\"2017/01/odontop_implante-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"odontop_implante-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"odontop_implante-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1117, 163, '_wp_attached_file', '2017/01/odontop_protese.jpg'),
(1118, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:27:\"2017/01/odontop_protese.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"odontop_protese-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"odontop_protese-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1119, 164, '_wp_attached_file', '2017/01/odontop_protese-1.jpg'),
(1120, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:29:\"2017/01/odontop_protese-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_protese-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_protese-1-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1121, 165, '_wp_attached_file', '2017/01/odontop_prótese4.jpg'),
(1122, 165, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:29:\"2017/01/odontop_prótese4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_prótese4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_prótese4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1123, 166, '_wp_attached_file', '2017/01/odontop_prótese.jpg'),
(1124, 166, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:28:\"2017/01/odontop_prótese.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_prótese-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_prótese-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1125, 167, '_wp_attached_file', '2017/01/odontop_prótese2.jpg'),
(1126, 167, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:29:\"2017/01/odontop_prótese2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_prótese2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_prótese2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1127, 168, '_wp_attached_file', '2017/01/odontop_prótese3.jpg'),
(1128, 168, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:29:\"2017/01/odontop_prótese3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"odontop_prótese3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"odontop_prótese3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1129, 169, '_wp_attached_file', '2017/01/odontop_clareamento.jpg'),
(1130, 169, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:31:\"2017/01/odontop_clareamento.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"odontop_clareamento-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"odontop_clareamento-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1131, 170, '_wp_attached_file', '2017/01/odontop_clareamento4.jpg'),
(1132, 170, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:32:\"2017/01/odontop_clareamento4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1133, 171, '_wp_attached_file', '2017/01/odontop_clareamento2.jpg'),
(1134, 171, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:32:\"2017/01/odontop_clareamento2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1135, 172, '_wp_attached_file', '2017/01/odontop_clareamento1.jpg'),
(1136, 172, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:32:\"2017/01/odontop_clareamento1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1137, 173, '_wp_attached_file', '2017/01/odontop_clareamento3.jpg'),
(1138, 173, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:32:\"2017/01/odontop_clareamento3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"odontop_clareamento3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1139, 174, '_wp_attached_file', '2017/01/odontop_clinica.jpg'),
(1140, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:27:\"2017/01/odontop_clinica.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"odontop_clinica-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"odontop_clinica-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1141, 175, '_wp_attached_file', '2017/01/odontop_clinica1.jpg'),
(1142, 175, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:28:\"2017/01/odontop_clinica1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_clinica1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_clinica1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1143, 176, '_wp_attached_file', '2017/01/odontop_clinica4.jpg'),
(1144, 176, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:28:\"2017/01/odontop_clinica4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_clinica4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_clinica4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1145, 177, '_wp_attached_file', '2017/01/odontop_clinica3.jpg'),
(1146, 177, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:28:\"2017/01/odontop_clinica3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_clinica3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_clinica3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1147, 178, 'icone', '43'),
(1148, 178, '_icone', 'field_58762b248a4ec'),
(1149, 178, 'breve_descricao', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi consequatur excepturi vero consequuntur.'),
(1150, 178, '_breve_descricao', 'field_58762b4c8a4ed'),
(1151, 178, 'imagem', '32'),
(1152, 178, '_imagem', 'field_58763ed1b045f'),
(1153, 178, 'descricao', '<h4>O que é ortodontia?</h4>\r\nOrtodontia é uma especialidade odontológica que corrige a posição dos dentes e dos ossos maxilares posicionados de forma inadequada. Dentes tortos ou dentes que não se encaixam corretamente são difíceis de serem mantidos limpos.Também pode levar a dores de cabeça, dores na região do pescoço, dos ombros e das costas. Os dentes tortos ou mal posicionados também prejudicam a sua aparência.\r\n<h4>Porque realizar esse tratamento?</h4>\r\nA função principal do tratamento ortodôntico é restabelecer a oclusão dentária (perfeito engrenamento dos dentes superiores com os inferiores), que é fundamental para a correta mastigação e, consequentemente, a adequada nutrição e saúde bucal.Com o restabelecimento da oclusão, evitam-se problemas relativos à respiração, deglutição, fala e articulação temporomandibular (ATM).Não existe idade máxima para a realização de tratamento ortodôntico, embora no paciente adulto alguns cuidados especiais devam ser tomados, principalmente em relação aos tecidos de suporte dos dentes, que podem chegar a contraindicar o tratamento (problemas periodontais).\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.'),
(1154, 178, '_descricao', 'field_58763ee1b0460'),
(1155, 178, 'galeria', 'a:6:{i:0;s:2:\"58\";i:1;s:2:\"65\";i:2;s:2:\"66\";i:3;s:2:\"67\";i:4;s:2:\"68\";i:5;s:2:\"69\";}'),
(1156, 178, '_galeria', 'field_58763eefb0461'),
(1157, 179, '_wp_attached_file', '2017/01/odontop_clinica2.jpg'),
(1158, 179, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:28:\"2017/01/odontop_clinica2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"odontop_clinica2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"odontop_clinica2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1159, 180, '_wp_attached_file', '2017/01/odontop_siso.jpg'),
(1160, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:300;s:4:\"file\";s:24:\"2017/01/odontop_siso.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"odontop_siso-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"odontop_siso-300x120.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1161, 181, '_wp_attached_file', '2017/01/odontop_siso1.jpg'),
(1162, 181, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:25:\"2017/01/odontop_siso1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"odontop_siso1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"odontop_siso1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1163, 182, '_wp_attached_file', '2017/01/odontop_siso3.jpg'),
(1164, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:25:\"2017/01/odontop_siso3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"odontop_siso3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"odontop_siso3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1165, 183, '_wp_attached_file', '2017/01/odontop_siso4.jpg'),
(1166, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:25:\"2017/01/odontop_siso4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"odontop_siso4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"odontop_siso4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1167, 184, '_wp_attached_file', '2017/01/odontop_siso2.jpg'),
(1168, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:25:\"2017/01/odontop_siso2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"odontop_siso2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"odontop_siso2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1169, 186, '_wp_attached_file', '2017/01/A-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso.jpg'),
(1170, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:328;s:4:\"file\";s:68:\"2017/01/A-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:68:\"A-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:68:\"A-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso-300x131.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1173, 187, 'mostrar_na_pagina_inicial', '1'),
(1174, 187, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1175, 187, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1176, 187, '_breve_descricao', 'field_58776c0610352'),
(1177, 189, '_wp_attached_file', '2017/01/Ortodontia-mordida-300x253.png'),
(1178, 189, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:253;s:4:\"file\";s:38:\"2017/01/Ortodontia-mordida-300x253.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"Ortodontia-mordida-300x253-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"Ortodontia-mordida-300x253-300x253.png\";s:5:\"width\";i:300;s:6:\"height\";i:253;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1179, 190, '_wp_attached_file', '2017/01/Ortodontia-transforme-o-seu-sorriso.jpg'),
(1180, 190, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:328;s:4:\"file\";s:47:\"2017/01/Ortodontia-transforme-o-seu-sorriso.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"Ortodontia-transforme-o-seu-sorriso-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"Ortodontia-transforme-o-seu-sorriso-300x131.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1181, 105, '_edit_last', '1'),
(1184, 191, 'mostrar_na_pagina_inicial', '0'),
(1185, 191, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1186, 191, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1187, 191, '_breve_descricao', 'field_58776c0610352'),
(1188, 105, '_wp_old_slug', 'manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-3'),
(1189, 105, 'mostrar_na_pagina_inicial', '1'),
(1190, 105, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1191, 193, '_wp_attached_file', '2017/01/clareamento-dental.jpg'),
(1192, 193, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:328;s:4:\"file\";s:30:\"2017/01/clareamento-dental.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"clareamento-dental-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"clareamento-dental-300x131.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1195, 194, 'mostrar_na_pagina_inicial', '0'),
(1196, 194, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1197, 194, 'breve_descricao', 'O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. '),
(1198, 194, '_breve_descricao', 'field_58776c0610352'),
(1199, 103, '_wp_old_slug', 'manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-2'),
(1200, 103, 'mostrar_na_pagina_inicial', '1'),
(1201, 103, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1204, 195, 'mostrar_na_pagina_inicial', '0'),
(1205, 195, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1206, 195, 'breve_descricao', 'O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. '),
(1207, 195, '_breve_descricao', 'field_58776c0610352'),
(1209, 197, '_wp_attached_file', '2017/01/extração-do-siso.jpg'),
(1210, 197, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:328;s:4:\"file\";s:30:\"2017/01/extração-do-siso.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"extração-do-siso-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"extração-do-siso-300x131.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1213, 198, 'mostrar_na_pagina_inicial', '1'),
(1214, 198, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1215, 198, 'breve_descricao', 'Conhecido popularmente como \"dente do juízo\", o siso tem uma notoriedade bem grande no meio dos jovens adultos. Responsável por inúmeras dores de cabeça, desconforto na mastigação e muitas vezes até entortando os demais dentes.'),
(1216, 198, '_breve_descricao', 'field_58776c0610352'),
(1217, 106, '_wp_old_slug', 'prevencao'),
(1231, 200, 'itens_0_icone', '132'),
(1232, 200, '_itens_0_icone', 'field_5877a311e5c28'),
(1233, 200, 'itens_0_titulo', 'Saúde'),
(1234, 200, '_itens_0_titulo', 'field_5877a31be5c29'),
(1235, 200, 'itens_0_descricao', 'Mantenha sua boca longe das doenças bucais com a nossa ajuda.'),
(1236, 200, '_itens_0_descricao', 'field_5877a322e5c2a'),
(1237, 200, 'itens_1_icone', '130'),
(1238, 200, '_itens_1_icone', 'field_5877a311e5c28'),
(1239, 200, 'itens_1_titulo', 'Prevenção'),
(1240, 200, '_itens_1_titulo', 'field_5877a31be5c29'),
(1241, 200, 'itens_1_descricao', 'Faça a visita regular ao dentista regularmente.'),
(1242, 200, '_itens_1_descricao', 'field_5877a322e5c2a'),
(1243, 200, 'itens_2_icone', '131'),
(1244, 200, '_itens_2_icone', 'field_5877a311e5c28'),
(1245, 200, 'itens_2_titulo', 'Estética'),
(1246, 200, '_itens_2_titulo', 'field_5877a31be5c29'),
(1247, 200, 'itens_2_descricao', 'Com a gente é sempre possível melhorar a aparência do seu sorriso.'),
(1248, 200, '_itens_2_descricao', 'field_5877a322e5c2a'),
(1249, 200, 'itens', '3'),
(1250, 200, '_itens', 'field_5877a2ece5c26'),
(1251, 201, 'itens_0_icone', '132'),
(1252, 201, '_itens_0_icone', 'field_5877a311e5c28'),
(1253, 201, 'itens_0_titulo', 'Saúde'),
(1254, 201, '_itens_0_titulo', 'field_5877a31be5c29'),
(1255, 201, 'itens_0_descricao', 'Mantenha sua boca longe das doenças bucais com a nossa ajuda.'),
(1256, 201, '_itens_0_descricao', 'field_5877a322e5c2a'),
(1257, 201, 'itens_1_icone', '130'),
(1258, 201, '_itens_1_icone', 'field_5877a311e5c28'),
(1259, 201, 'itens_1_titulo', 'Prevenção'),
(1260, 201, '_itens_1_titulo', 'field_5877a31be5c29'),
(1261, 201, 'itens_1_descricao', 'Consulte o dentista regularmente em uma de nossas clínicas.'),
(1262, 201, '_itens_1_descricao', 'field_5877a322e5c2a'),
(1263, 201, 'itens_2_icone', '131'),
(1264, 201, '_itens_2_icone', 'field_5877a311e5c28'),
(1265, 201, 'itens_2_titulo', 'Estética'),
(1266, 201, '_itens_2_titulo', 'field_5877a31be5c29'),
(1267, 201, 'itens_2_descricao', 'Com a gente é sempre possível melhorar a aparência do seu sorriso.'),
(1268, 201, '_itens_2_descricao', 'field_5877a322e5c2a'),
(1269, 201, 'itens', '3'),
(1270, 201, '_itens', 'field_5877a2ece5c26'),
(1273, 202, 'mostrar_na_pagina_inicial', '1'),
(1274, 202, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1275, 202, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1276, 202, '_breve_descricao', 'field_58776c0610352'),
(1279, 203, 'mostrar_na_pagina_inicial', '1'),
(1280, 203, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1281, 203, 'breve_descricao', 'O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. '),
(1282, 203, '_breve_descricao', 'field_58776c0610352'),
(1285, 204, 'mostrar_na_pagina_inicial', '1'),
(1286, 204, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1287, 204, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1288, 204, '_breve_descricao', 'field_58776c0610352'),
(1291, 205, 'mostrar_na_pagina_inicial', '1'),
(1292, 205, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1293, 205, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1294, 205, '_breve_descricao', 'field_58776c0610352'),
(1297, 206, 'mostrar_na_pagina_inicial', '1'),
(1298, 206, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1299, 206, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1300, 206, '_breve_descricao', 'field_58776c0610352'),
(1301, 207, '_edit_last', '1'),
(1302, 207, '_edit_lock', '1487959973:1'),
(1303, 207, '_wp_page_template', 'default'),
(1304, 209, '_edit_last', '1'),
(1305, 209, 'field_588b33aec0440', 'a:14:{s:3:\"key\";s:19:\"field_588b33aec0440\";s:5:\"label\";s:4:\"Nome\";s:4:\"name\";s:4:\"nome\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:0;}'),
(1306, 209, 'field_588b3388c043f', 'a:11:{s:3:\"key\";s:19:\"field_588b3388c043f\";s:5:\"label\";s:6:\"Imagem\";s:4:\"name\";s:6:\"imagem\";s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:11:\"save_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:1;}'),
(1307, 209, 'field_588b33d9c0441', 'a:14:{s:3:\"key\";s:19:\"field_588b33d9c0441\";s:5:\"label\";s:3:\"CRO\";s:4:\"name\";s:3:\"cro\";s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:2;}'),
(1308, 209, 'field_588b33edc0442', 'a:13:{s:3:\"key\";s:19:\"field_588b33edc0442\";s:5:\"label\";s:11:\"Descrição\";s:4:\"name\";s:9:\"descricao\";s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";s:1:\"0\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:10:\"formatting\";s:2:\"br\";s:17:\"conditional_logic\";a:3:{s:6:\"status\";s:1:\"0\";s:5:\"rules\";a:1:{i:0;a:2:{s:5:\"field\";s:4:\"null\";s:8:\"operator\";s:2:\"==\";}}s:8:\"allorany\";s:3:\"all\";}s:8:\"order_no\";i:3;}'),
(1309, 209, 'rule', 'a:5:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"207\";s:8:\"order_no\";i:0;s:8:\"group_no\";i:0;}'),
(1310, 209, 'position', 'normal'),
(1311, 209, 'layout', 'no_box'),
(1312, 209, 'hide_on_screen', 'a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}'),
(1313, 209, '_edit_lock', '1485517682:1'),
(1314, 210, 'nome', 'Dr. Maurício Azevedos da Fonseca'),
(1315, 210, '_nome', 'field_588b33aec0440'),
(1316, 210, 'imagem', '31'),
(1317, 210, '_imagem', 'field_588b3388c043f'),
(1318, 210, 'cro', 'CRO - 8989'),
(1319, 210, '_cro', 'field_588b33d9c0441'),
(1320, 210, 'descricao', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(1321, 210, '_descricao', 'field_588b33edc0442'),
(1322, 207, 'nome', 'Dr. Carla Dreyer'),
(1323, 207, '_nome', 'field_588b33aec0440'),
(1324, 207, 'imagem', '254'),
(1325, 207, '_imagem', 'field_588b3388c043f'),
(1326, 207, 'cro', 'CRO-PR 19181'),
(1327, 207, '_cro', 'field_588b33d9c0441'),
(1328, 207, 'descricao', 'Dr. Carla Dreyer fundadora Odontop.\r\n\r\nSomos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop.'),
(1329, 207, '_descricao', 'field_588b33edc0442'),
(1330, 211, '_wp_attached_file', '2017/01/carla_icone.jpg'),
(1331, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:162;s:6:\"height\";i:162;s:4:\"file\";s:23:\"2017/01/carla_icone.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"carla_icone-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1332, 212, 'nome', 'Dr. Carla Dreyer'),
(1333, 212, '_nome', 'field_588b33aec0440'),
(1334, 212, 'imagem', '211'),
(1335, 212, '_imagem', 'field_588b3388c043f'),
(1336, 212, 'cro', 'CRO - 8989'),
(1337, 212, '_cro', 'field_588b33d9c0441'),
(1338, 212, 'descricao', 'Dr. Carla Dreyer fundadora Odontop, especialista em Implantodontia e ortodontia'),
(1339, 212, '_descricao', 'field_588b33edc0442'),
(1340, 213, 'nome', 'Dr. Carla Dreyer'),
(1341, 213, '_nome', 'field_588b33aec0440'),
(1342, 213, 'imagem', '211'),
(1343, 213, '_imagem', 'field_588b3388c043f'),
(1344, 213, 'cro', 'CRO - 8989'),
(1345, 213, '_cro', 'field_588b33d9c0441'),
(1346, 213, 'descricao', 'Dr. Carla Dreyer fundadora Odontop, especialista em Implantodontia e ortodontia'),
(1347, 213, '_descricao', 'field_588b33edc0442'),
(1348, 214, '_wp_attached_file', '2017/01/CARLA_foto.png'),
(1349, 214, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:162;s:6:\"height\";i:162;s:4:\"file\";s:22:\"2017/01/CARLA_foto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"CARLA_foto-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1350, 215, 'nome', 'Dr. Carla Dreyer'),
(1351, 215, '_nome', 'field_588b33aec0440'),
(1352, 215, 'imagem', '214'),
(1353, 215, '_imagem', 'field_588b3388c043f'),
(1354, 215, 'cro', 'CRO - 8989'),
(1355, 215, '_cro', 'field_588b33d9c0441'),
(1356, 215, 'descricao', 'Dr. Carla Dreyer fundadora Odontop, especialista em Implantodontia e ortodontia'),
(1357, 215, '_descricao', 'field_588b33edc0442'),
(1358, 216, 'nome', 'Dr. Carla Dreyer'),
(1359, 216, '_nome', 'field_588b33aec0440'),
(1360, 216, 'imagem', '214'),
(1361, 216, '_imagem', 'field_588b3388c043f'),
(1362, 216, 'cro', 'CRO - 8989'),
(1363, 216, '_cro', 'field_588b33d9c0441'),
(1364, 216, 'descricao', 'Dr. Carla Dreyer fundadora Odontop, especialista em Implantodontia e ortodontia'),
(1365, 216, '_descricao', 'field_588b33edc0442'),
(1366, 217, 'nome', 'Dr. Carla Dreyer'),
(1367, 217, '_nome', 'field_588b33aec0440'),
(1368, 217, 'imagem', '214'),
(1369, 217, '_imagem', 'field_588b3388c043f'),
(1370, 217, 'cro', 'CRO - 8989'),
(1371, 217, '_cro', 'field_588b33d9c0441'),
(1372, 217, 'descricao', 'Dr. Carla Dreyer fundadora Odontop.\r\n\r\nSomos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop.'),
(1373, 217, '_descricao', 'field_588b33edc0442'),
(1374, 218, 'nome', 'Dr. Carla Dreyer'),
(1375, 218, '_nome', 'field_588b33aec0440'),
(1376, 218, 'imagem', '214'),
(1377, 218, '_imagem', 'field_588b3388c043f'),
(1378, 218, 'cro', 'CRO-PR 19181'),
(1379, 218, '_cro', 'field_588b33d9c0441'),
(1380, 218, 'descricao', 'Dr. Carla Dreyer fundadora Odontop.\r\n\r\nSomos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop.'),
(1381, 218, '_descricao', 'field_588b33edc0442'),
(1382, 219, 'imagem', '121'),
(1383, 219, '_imagem', 'field_58779ceb62bf4'),
(1384, 219, 'titulo', 'Tenha um sorriso de celebridade, Invista em um tratamento de Lentes de Contato'),
(1385, 219, '_titulo', 'field_58779cfc62bf5'),
(1386, 219, 'subtitulo', 'Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma'),
(1387, 219, '_subtitulo', 'field_58779d0462bf6'),
(1394, 221, 'imagem', '121'),
(1395, 221, '_imagem', 'field_58779ceb62bf4'),
(1396, 221, 'titulo', 'Tenha um sorriso de celebridade'),
(1397, 221, '_titulo', 'field_58779cfc62bf5'),
(1398, 221, 'subtitulo', 'Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma'),
(1399, 221, '_subtitulo', 'field_58779d0462bf6'),
(1400, 222, 'imagem', '121'),
(1401, 222, '_imagem', 'field_58779ceb62bf4'),
(1402, 222, 'titulo', 'Tenha um sorriso de celebridade'),
(1403, 222, '_titulo', 'field_58779cfc62bf5'),
(1404, 222, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1405, 222, '_subtitulo', 'field_58779d0462bf6'),
(1406, 223, '_wp_attached_file', '2017/01/clinca_odontop_colombo.jpg'),
(1407, 223, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:34:\"2017/01/clinca_odontop_colombo.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"clinca_odontop_colombo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1408, 224, '_wp_attached_file', '2017/01/clinca_odontop_quatrobarras.jpg'),
(1409, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:39:\"2017/01/clinca_odontop_quatrobarras.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"clinca_odontop_quatrobarras-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1410, 225, '_wp_attached_file', '2017/01/clinca_odontop_colombs.jpg'),
(1411, 225, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:34:\"2017/01/clinca_odontop_colombs.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"clinca_odontop_colombs-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1412, 226, '_wp_attached_file', '2017/01/clinica_odontop_campina.jpg'),
(1413, 226, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:35:\"2017/01/clinica_odontop_campina.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"clinica_odontop_campina-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1414, 227, '_wp_attached_file', '2017/01/clinca_odontop_riobranco.jpg'),
(1415, 227, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:36:\"2017/01/clinca_odontop_riobranco.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"clinca_odontop_riobranco-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1416, 228, '_wp_attached_file', '2017/01/clinca_odontop_Almirante.jpg'),
(1417, 228, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:177;s:6:\"height\";i:221;s:4:\"file\";s:36:\"2017/01/clinca_odontop_Almirante.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"clinca_odontop_Almirante-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1420, 229, 'mostrar_na_pagina_inicial', '1'),
(1421, 229, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1422, 229, 'breve_descricao', 'O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. '),
(1423, 229, '_breve_descricao', 'field_58776c0610352'),
(1426, 230, 'mostrar_na_pagina_inicial', '1'),
(1427, 230, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1428, 230, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1429, 230, '_breve_descricao', 'field_58776c0610352'),
(1432, 231, 'mostrar_na_pagina_inicial', '1'),
(1433, 231, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1434, 231, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. '),
(1435, 231, '_breve_descricao', 'field_58776c0610352'),
(1438, 232, 'mostrar_na_pagina_inicial', '1'),
(1439, 232, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1440, 232, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1441, 232, '_breve_descricao', 'field_58776c0610352'),
(1444, 233, 'mostrar_na_pagina_inicial', '1'),
(1445, 233, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1446, 233, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa...'),
(1447, 233, '_breve_descricao', 'field_58776c0610352'),
(1450, 235, 'imagem', '234'),
(1451, 235, '_imagem', 'field_58779ceb62bf4'),
(1452, 235, 'titulo', 'Tenha um sorriso de celebridade'),
(1453, 235, '_titulo', 'field_58779cfc62bf5'),
(1454, 235, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1455, 235, '_subtitulo', 'field_58779d0462bf6'),
(1456, 236, 'imagem', '234'),
(1457, 236, '_imagem', 'field_58779ceb62bf4'),
(1458, 236, 'titulo', 'Tenha um sorriso de celebridade'),
(1459, 236, '_titulo', 'field_58779cfc62bf5'),
(1460, 236, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1461, 236, '_subtitulo', 'field_58779d0462bf6'),
(1462, 237, 'imagem', '121'),
(1463, 237, '_imagem', 'field_58779ceb62bf4'),
(1464, 237, 'titulo', 'Tenha um sorriso de celebridade'),
(1465, 237, '_titulo', 'field_58779cfc62bf5'),
(1466, 237, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1467, 237, '_subtitulo', 'field_58779d0462bf6'),
(1468, 238, '_wp_attached_file', '2017/01/clinca_odontop_header.jpg'),
(1469, 238, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1905;s:6:\"height\";i:635;s:4:\"file\";s:33:\"2017/01/clinca_odontop_header.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"clinca_odontop_header-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"clinca_odontop_header-300x100.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"clinca_odontop_header-768x256.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:256;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"clinca_odontop_header-1024x341.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:341;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1470, 239, 'imagem', '238'),
(1471, 239, '_imagem', 'field_58779ceb62bf4'),
(1472, 239, 'titulo', 'Tenha um sorriso de celebridade'),
(1473, 239, '_titulo', 'field_58779cfc62bf5'),
(1474, 239, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1475, 239, '_subtitulo', 'field_58779d0462bf6'),
(1476, 240, 'imagem', '238'),
(1477, 240, '_imagem', 'field_58779ceb62bf4'),
(1478, 240, 'titulo', 'Tenha um sorriso de celebridade'),
(1479, 240, '_titulo', 'field_58779cfc62bf5'),
(1480, 240, 'subtitulo', 'Lentes de Contato ou facetas. Indicada para dentes que possuem uma boa coloração, mas que exista algum defeito estrutural ou de forma.'),
(1481, 240, '_subtitulo', 'field_58779d0462bf6'),
(1494, 243, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Desde (ano de início da clínica), buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1495, 243, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1496, 243, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1497, 243, '_galeria', 'field_58754713405f0'),
(1498, 243, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1499, 243, '_historia', 'field_5875468d571a7'),
(1500, 243, 'valores', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1501, 243, '_valores', 'field_58754698571a8'),
(1502, 243, 'formas_de_pagamento', '23'),
(1503, 243, '_formas_de_pagamento', 'field_587546a3571a9'),
(1504, 243, 'facebook', 'www.facebook.com/odontopodonto'),
(1505, 243, '_facebook', 'field_58779c31a2e47'),
(1506, 244, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Desde (ano de início da clínica), buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1507, 244, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1508, 244, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1509, 244, '_galeria', 'field_58754713405f0'),
(1510, 244, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1511, 244, '_historia', 'field_5875468d571a7'),
(1512, 244, 'valores', '<ul>\r\n 	<li>Responsabilidade</li>\r\n 	<li>Organização</li>\r\n 	<li>Agilidade</li>\r\n 	<li>Respeito</li>\r\n</ul>'),
(1513, 244, '_valores', 'field_58754698571a8'),
(1514, 244, 'formas_de_pagamento', '23'),
(1515, 244, '_formas_de_pagamento', 'field_587546a3571a9'),
(1516, 244, 'facebook', 'www.facebook.com/odontopodonto'),
(1517, 244, '_facebook', 'field_58779c31a2e47'),
(1518, 245, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Desde (ano de início da clínica), buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1519, 245, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1520, 245, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1521, 245, '_galeria', 'field_58754713405f0'),
(1522, 245, 'historia', 'Nunc lacinia pharetra orci, id pulvinar sapien faucibus et. Nunc etis sum pellentesque dignissim lorem, vel pharetra dolor rutrum in. Suspendisse metus libero, malesuada facilisis tempor ac, laoreet quis lectus adispicing Vestibulum tincidunt dictum sem pretium consectetur.\r\n\r\nDuis vel arcu in magna blandit accumsan. Integer vitae mauris id nunc consectetur vehicula quis et turpis.'),
(1523, 245, '_historia', 'field_5875468d571a7'),
(1524, 245, 'valores', '- Responsabilidade\r\n- Organização\r\n- Agilidade\r\n- Respeito'),
(1525, 245, '_valores', 'field_58754698571a8'),
(1526, 245, 'formas_de_pagamento', '23'),
(1527, 245, '_formas_de_pagamento', 'field_587546a3571a9'),
(1528, 245, 'facebook', 'www.facebook.com/odontopodonto'),
(1529, 245, '_facebook', 'field_58779c31a2e47'),
(1530, 246, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1531, 246, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1532, 246, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1533, 246, '_galeria', 'field_58754713405f0'),
(1534, 246, 'historia', 'Desde 2007 a Clínica Odontop está presente na região metropolitana de Curitiba-PR. Com com profissionais especializados para atender você cada vez melhor . Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.'),
(1535, 246, '_historia', 'field_5875468d571a7'),
(1536, 246, 'valores', '- Responsabilidade\r\n- Organização\r\n- Agilidade\r\n- Respeito'),
(1537, 246, '_valores', 'field_58754698571a8'),
(1538, 246, 'formas_de_pagamento', '23'),
(1539, 246, '_formas_de_pagamento', 'field_587546a3571a9'),
(1540, 246, 'facebook', 'www.facebook.com/odontopodonto'),
(1541, 246, '_facebook', 'field_58779c31a2e47'),
(1542, 248, 'por_que_escolher_a_odontop', 'Somos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop. Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.\r\n\r\nMarque uma avaliação conosco, ficaremos gratos em poder contribuir para que seu sorriso permaneça sempre saudável.'),
(1543, 248, '_por_que_escolher_a_odontop', 'field_58754664571a6'),
(1544, 248, 'galeria', 'a:2:{i:0;s:2:\"10\";i:1;s:2:\"11\";}'),
(1545, 248, '_galeria', 'field_58754713405f0'),
(1546, 248, 'historia', 'Desde 2007 a Clínica Odontop está presente na região metropolitana de Curitiba-PR. Com com profissionais especializados para atender você cada vez melhor . Buscamos a excelência em nosso atendimento, proporcionando a segurança e confiabilidade para o melhor tratamento possível.'),
(1547, 248, '_historia', 'field_5875468d571a7'),
(1548, 248, 'valores', '- Responsabilidade\r\n- Organização\r\n- Agilidade\r\n- Respeito'),
(1549, 248, '_valores', 'field_58754698571a8'),
(1550, 248, 'formas_de_pagamento', '23'),
(1551, 248, '_formas_de_pagamento', 'field_587546a3571a9'),
(1552, 248, 'facebook', 'www.facebook.com/odontopodonto'),
(1553, 248, '_facebook', 'field_58779c31a2e47'),
(1554, 185, 'mostrar_na_pagina_inicial', '1'),
(1555, 185, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1556, 185, 'breve_descricao', 'A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa...'),
(1557, 185, '_breve_descricao', 'field_58776c0610352'),
(1558, 188, 'mostrar_na_pagina_inicial', '1'),
(1559, 188, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1560, 188, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1561, 188, '_breve_descricao', 'field_58776c0610352'),
(1564, 249, 'mostrar_na_pagina_inicial', '1'),
(1565, 249, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1566, 249, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1567, 249, '_breve_descricao', 'field_58776c0610352'),
(1570, 250, 'mostrar_na_pagina_inicial', '1'),
(1571, 250, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1572, 250, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1573, 250, '_breve_descricao', 'field_58776c0610352'),
(1576, 251, 'mostrar_na_pagina_inicial', '1'),
(1577, 251, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1578, 251, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1579, 251, '_breve_descricao', 'field_58776c0610352'),
(1582, 252, 'mostrar_na_pagina_inicial', '1'),
(1583, 252, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1584, 252, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1585, 252, '_breve_descricao', 'field_58776c0610352'),
(1588, 253, 'mostrar_na_pagina_inicial', '1'),
(1589, 253, '_mostrar_na_pagina_inicial', 'field_5877a4ac873fb'),
(1590, 253, 'breve_descricao', 'A ortodontia é a área responsável por ajustar o alinhamento dos dentes. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. '),
(1591, 253, '_breve_descricao', 'field_58776c0610352'),
(1592, 254, '_wp_attached_file', '2017/01/icon_carla.png'),
(1593, 254, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:162;s:6:\"height\";i:162;s:4:\"file\";s:22:\"2017/01/icon_carla.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"icon_carla-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1594, 255, 'nome', 'Dr. Carla Dreyer'),
(1595, 255, '_nome', 'field_588b33aec0440'),
(1596, 255, 'imagem', '254'),
(1597, 255, '_imagem', 'field_588b3388c043f'),
(1598, 255, 'cro', 'CRO-PR 19181'),
(1599, 255, '_cro', 'field_588b33d9c0441'),
(1600, 255, 'descricao', 'Dr. Carla Dreyer fundadora Odontop.\r\n\r\nSomos uma clínica odontológica presente na região metropolitana de Curitiba-PR. Contamos com profissionais especializados para atender cada vez melhor os pacientes da Odontop.'),
(1601, 255, '_descricao', 'field_588b33edc0442');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(4, 1, '2017-01-10 18:16:00', '2017-01-10 20:16:00', '', 'A Clínica', '', 'publish', 'closed', 'closed', '', 'a-clinica', '', '', '2017-02-14 19:44:20', '2017-02-14 21:44:20', '', 0, 'http://clinicaodontop.com/?page_id=4', 1, 'page', '', 0),
(5, 1, '2017-01-10 18:16:00', '2017-01-10 20:16:00', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-10 18:16:00', '2017-01-10 20:16:00', '', 4, 'http://clinicaodontop.com/2017/01/10/4-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2017-01-10 18:17:17', '2017-01-10 20:17:17', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2017-01-11 12:06:15', '2017-01-11 14:06:15', '', 0, 'http://clinicaodontop.com/?page_id=6', 2, 'page', '', 0),
(7, 1, '2017-01-10 18:17:17', '2017-01-10 20:17:17', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2017-01-10 18:17:17', '2017-01-10 20:17:17', '', 6, 'http://clinicaodontop.com/2017/01/10/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2017-01-10 18:40:35', '2017-01-10 20:40:35', '', 'A Clinica', '', 'publish', 'closed', 'closed', '', 'acf_a-clinica', '', '', '2017-01-12 13:09:45', '2017-01-12 15:09:45', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=8', 0, 'acf', '', 0),
(9, 1, '2017-01-10 19:13:31', '2017-01-10 21:13:31', '', 'Sedes', '', 'publish', 'closed', 'closed', '', 'acf_sedes', '', '', '2017-01-11 00:48:44', '2017-01-11 02:48:44', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=9', 0, 'acf', '', 0),
(10, 1, '2017-01-11 00:27:18', '2017-01-11 02:27:18', '', 'img-escolha', '', 'inherit', 'open', 'closed', '', 'img-escolha', '', '', '2017-01-11 00:27:18', '2017-01-11 02:27:18', '', 4, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-escolha.png', 0, 'attachment', 'image/png', 0),
(11, 1, '2017-01-11 00:27:29', '2017-01-11 02:27:29', '', 'img-escolha', '', 'inherit', 'open', 'closed', '', 'img-escolha-2', '', '', '2017-01-11 00:27:29', '2017-01-11 02:27:29', '', 4, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-escolha-1.png', 0, 'attachment', 'image/png', 0),
(12, 1, '2017-01-11 00:27:40', '2017-01-11 02:27:40', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-11 00:27:40', '2017-01-11 02:27:40', '', 4, 'http://clinicaodontop.com/2017/01/11/4-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2017-01-11 00:44:51', '2017-01-11 02:44:51', '', 'Alm. Tamandaré', '', 'publish', 'closed', 'closed', '', 'alm-tamandare', '', '', '2017-02-14 19:29:40', '2017-02-14 21:29:40', '', 0, 'http://clinicaodontop.com/?post_type=sedes&#038;p=13', 5, 'sedes', '', 0),
(14, 1, '2017-01-11 00:43:56', '2017-01-11 02:43:56', '', 'img-sedes', '', 'inherit', 'open', 'closed', '', 'img-sedes', '', '', '2017-01-11 00:43:56', '2017-01-11 02:43:56', '', 13, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-sedes.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2017-01-11 00:50:28', '2017-01-11 02:50:28', '', 'Campina Grande do Sul', '', 'publish', 'closed', 'closed', '', 'campinagrandedosul', '', '', '2017-02-14 19:29:18', '2017-02-14 21:29:18', '', 0, 'http://clinicaodontop.com/?post_type=sedes&#038;p=19', 3, 'sedes', '', 0),
(20, 1, '2017-01-11 00:50:28', '2017-01-11 02:50:28', '', 'Rio Branco do Sul', '', 'publish', 'closed', 'closed', '', 'riobrancodosul', '', '', '2017-02-14 19:29:30', '2017-02-14 21:29:30', '', 0, 'http://clinicaodontop.com/?post_type=sedes&#038;p=20', 4, 'sedes', '', 0),
(21, 1, '2017-01-11 00:50:29', '2017-01-11 02:50:29', '', 'Quatro Barras', '', 'publish', 'closed', 'closed', '', 'quatrobarras', '', '', '2017-02-14 19:29:06', '2017-02-14 21:29:06', '', 0, 'http://clinicaodontop.com/?post_type=sedes&#038;p=21', 2, 'sedes', '', 0),
(22, 1, '2017-01-11 00:50:30', '2017-01-11 02:50:30', '', 'Colombo', '', 'publish', 'closed', 'closed', '', 'colombo', '', '', '2017-02-14 19:28:55', '2017-02-14 21:28:55', '', 0, 'http://clinicaodontop.com/?post_type=sedes&#038;p=22', 1, 'sedes', '', 0),
(23, 1, '2017-01-11 00:57:47', '2017-01-11 02:57:47', '', 'img-cartoes', '', 'inherit', 'open', 'closed', '', 'img-cartoes', '', '', '2017-01-11 00:57:47', '2017-01-11 02:57:47', '', 4, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-cartoes.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2017-01-11 00:57:53', '2017-01-11 02:57:53', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-11 00:57:53', '2017-01-11 02:57:53', '', 4, 'http://clinicaodontop.com/2017/01/11/4-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2017-01-11 01:00:58', '2017-01-11 03:00:58', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-11 01:00:58', '2017-01-11 03:00:58', '', 4, 'http://clinicaodontop.com/2017/01/11/4-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2017-01-11 01:01:50', '2017-01-11 03:01:50', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-11 01:01:50', '2017-01-11 03:01:50', '', 4, 'http://clinicaodontop.com/2017/01/11/4-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2017-01-11 01:02:08', '2017-01-11 03:02:08', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-11 01:02:08', '2017-01-11 03:02:08', '', 4, 'http://clinicaodontop.com/2017/01/11/4-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2017-01-11 10:29:24', '2017-01-11 12:29:24', '', 'Doutores', '', 'publish', 'closed', 'closed', '', 'acf_doutores', '', '', '2017-01-11 10:29:24', '2017-01-11 12:29:24', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=28', 0, 'acf', '', 0),
(30, 1, '2017-01-11 10:32:27', '2017-01-11 12:32:27', '', 'Dr. Pessoa 1', '', 'publish', 'closed', 'closed', '', 'dr-pessoa-1', '', '', '2017-01-11 10:32:27', '2017-01-11 12:32:27', '', 0, 'http://clinicaodontop.com/?post_type=doutores&#038;p=30', 3, 'doutores', '', 0),
(31, 1, '2017-01-11 10:32:06', '2017-01-11 12:32:06', '', 'img-dr', '', 'inherit', 'open', 'closed', '', 'img-dr', '', '', '2017-01-11 10:32:06', '2017-01-11 12:32:06', '', 30, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-dr.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2017-01-11 10:32:10', '2017-01-11 12:32:10', '', 'img-tratamento', '', 'inherit', 'open', 'closed', '', 'img-tratamento', '', '', '2017-01-11 10:32:10', '2017-01-11 12:32:10', '', 30, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-tratamento.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2017-01-11 10:32:46', '2017-01-11 12:32:46', '', 'Dr. Pessoa 2', '', 'publish', 'closed', 'closed', '', 'dr-pessoa-2', '', '', '2017-02-14 18:43:37', '2017-02-14 20:43:37', '', 0, 'http://clinicaodontop.com/?post_type=doutores&#038;p=33', 2, 'doutores', '', 0),
(34, 1, '2017-01-11 10:33:04', '2017-01-11 12:33:04', '', 'Dr. Pessoa 3', '', 'publish', 'closed', 'closed', '', 'dr-pessoa-3', '', '', '2017-01-11 10:33:04', '2017-01-11 12:33:04', '', 0, 'http://clinicaodontop.com/?post_type=doutores&#038;p=34', 1, 'doutores', '', 0),
(36, 1, '2017-01-11 10:56:24', '2017-01-11 12:56:24', '', 'Serviços / Tratamento', '', 'publish', 'closed', 'closed', '', 'acf_servicos-tratamento', '', '', '2017-01-11 15:24:59', '2017-01-11 17:24:59', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=36', 0, 'acf', '', 0),
(37, 1, '2017-01-11 10:59:51', '2017-01-11 12:59:51', '', 'Implates', '', 'publish', 'closed', 'closed', '', 'implates', '', '', '2017-01-11 15:04:28', '2017-01-11 17:04:28', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=37', 0, 'servicos', '', 0),
(38, 1, '2017-01-11 10:58:16', '2017-01-11 12:58:16', '', 'icon-servico6', '', 'inherit', 'open', 'closed', '', 'icon-servico6', '', '', '2017-01-11 10:58:16', '2017-01-11 12:58:16', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico6.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2017-01-11 10:58:17', '2017-01-11 12:58:17', '', 'icon-servico1', '', 'inherit', 'open', 'closed', '', 'icon-servico1', '', '', '2017-01-11 10:58:17', '2017-01-11 12:58:17', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico1.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2017-01-11 10:58:18', '2017-01-11 12:58:18', '', 'icon-servico2', '', 'inherit', 'open', 'closed', '', 'icon-servico2', '', '', '2017-01-11 10:58:18', '2017-01-11 12:58:18', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico2.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2017-01-11 10:58:18', '2017-01-11 12:58:18', '', 'icon-servico3', '', 'inherit', 'open', 'closed', '', 'icon-servico3', '', '', '2017-01-11 10:58:18', '2017-01-11 12:58:18', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico3.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2017-01-11 10:58:19', '2017-01-11 12:58:19', '', 'icon-servico4', '', 'inherit', 'open', 'closed', '', 'icon-servico4', '', '', '2017-01-11 10:58:19', '2017-01-11 12:58:19', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico4.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2017-01-11 10:58:20', '2017-01-11 12:58:20', '', 'icon-servico5', '', 'inherit', 'open', 'closed', '', 'icon-servico5', '', '', '2017-01-11 10:58:20', '2017-01-11 12:58:20', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-servico5.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2017-01-11 11:00:12', '2017-01-11 13:00:12', '', 'Ortodontia', '', 'publish', 'closed', 'closed', '', 'ortodontia', '', '', '2017-01-11 15:12:49', '2017-01-11 17:12:49', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=44', 1, 'servicos', '', 0),
(45, 1, '2017-01-11 11:01:14', '2017-01-11 13:01:14', '', 'Próteses', '', 'publish', 'closed', 'closed', '', 'proteses', '', '', '2017-01-11 15:13:16', '2017-01-11 17:13:16', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=45', 2, 'servicos', '', 0),
(46, 1, '2017-01-11 11:01:43', '2017-01-11 13:01:43', '', 'Clareamento', '', 'publish', 'closed', 'closed', '', 'clareamento', '', '', '2017-01-11 15:13:13', '2017-01-11 17:13:13', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=46', 3, 'servicos', '', 0),
(47, 1, '2017-01-11 11:02:48', '2017-01-11 13:02:48', '', 'Extração de Siso', '', 'publish', 'closed', 'closed', '', 'extracao-de-siso', '', '', '2017-01-11 15:13:09', '2017-01-11 17:13:09', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=47', 5, 'servicos', '', 0),
(48, 1, '2017-01-11 11:02:09', '2017-01-11 13:02:09', '', 'Clínica Geral', '', 'publish', 'closed', 'closed', '', 'clinica-geral', '', '', '2017-01-11 15:13:11', '2017-01-11 17:13:11', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=48', 4, 'servicos', '', 0),
(49, 1, '2017-01-11 11:06:00', '2017-01-11 13:06:00', '', 'Endodotia', '', 'publish', 'closed', 'closed', '', 'endodotia', '', '', '2017-01-11 15:13:07', '2017-01-11 17:13:07', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=49', 6, 'servicos', '', 0),
(50, 1, '2017-01-11 11:06:20', '2017-01-11 13:06:20', '', 'Odontopediatria', '', 'publish', 'closed', 'closed', '', 'odontopediatria', '', '', '2017-01-11 15:12:56', '2017-01-11 17:12:56', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=50', 10, 'servicos', '', 0),
(51, 1, '2017-01-11 11:06:43', '2017-01-11 13:06:43', '', 'Ronco e Apneia', '', 'publish', 'closed', 'closed', '', 'ronco-e-apneia', '', '', '2017-01-11 15:12:58', '2017-01-11 17:12:58', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=51', 9, 'servicos', '', 0),
(52, 1, '2017-01-11 11:06:52', '2017-01-11 13:06:52', '', 'DTM', '', 'publish', 'closed', 'closed', '', 'dtm', '', '', '2017-01-11 15:13:02', '2017-01-11 17:13:02', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=52', 8, 'servicos', '', 0),
(53, 1, '2017-01-11 11:08:38', '2017-01-11 13:08:38', '', 'Periodontia', '', 'publish', 'closed', 'closed', '', 'periodontia', '', '', '2017-01-11 15:13:04', '2017-01-11 17:13:04', '', 0, 'http://clinicaodontop.com/?post_type=servicos&#038;p=53', 7, 'servicos', '', 0),
(54, 1, '2017-01-11 11:47:24', '2017-01-11 13:47:24', '', 'Depoimentos', '', 'publish', 'closed', 'closed', '', 'acf_depoimentos', '', '', '2017-01-11 11:47:24', '2017-01-11 13:47:24', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=54', 0, 'acf', '', 0),
(55, 1, '2017-01-11 11:48:28', '2017-01-11 13:48:28', '', 'Marivânia Silvestre Brito', '', 'publish', 'closed', 'closed', '', 'fulano-da-silva', '', '', '2017-01-19 15:08:18', '2017-01-19 17:08:18', '', 0, 'http://clinicaodontop.com/?post_type=depoimentos&#038;p=55', 0, 'depoimentos', '', 0),
(56, 1, '2017-01-11 11:48:08', '2017-01-11 13:48:08', '', 'img-depoimento', '', 'inherit', 'open', 'closed', '', 'img-depoimento', '', '', '2017-01-11 11:48:08', '2017-01-11 13:48:08', '', 55, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-depoimento.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2017-01-11 11:50:18', '2017-01-11 13:50:18', '', 'Carlos Castanho', '', 'publish', 'closed', 'closed', '', 'fulana', '', '', '2017-01-19 15:07:57', '2017-01-19 17:07:57', '', 0, 'http://clinicaodontop.com/?post_type=depoimentos&#038;p=57', 0, 'depoimentos', '', 0),
(58, 1, '2017-01-11 15:04:18', '2017-01-11 17:04:18', '', 'img-mini-tratamento', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento', '', '', '2017-01-11 15:04:18', '2017-01-11 17:04:18', '', 37, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2017-01-11 15:06:45', '2017-01-11 17:06:45', '', 'Tratamentos', '', 'publish', 'closed', 'closed', '', 'tratamentos', '', '', '2017-01-19 14:54:28', '2017-01-19 16:54:28', '', 0, 'http://clinicaodontop.com/?page_id=59', 0, 'page', '', 0),
(60, 1, '2017-01-11 15:06:45', '2017-01-11 17:06:45', '', 'Tratamentos', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2017-01-11 15:06:45', '2017-01-11 17:06:45', '', 59, 'http://clinicaodontop.com/2017/01/11/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2017-01-11 15:20:47', '2017-01-11 17:20:47', '', 'Tratamentosx', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2017-01-11 15:20:47', '2017-01-11 17:20:47', '', 59, 'http://clinicaodontop.com/2017/01/11/59-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2017-01-11 15:31:53', '2017-01-11 17:31:53', '', 'Extração do Siso', '', 'publish', 'closed', 'closed', '', 'extracao-de-siso', '', '', '2017-01-19 17:33:14', '2017-01-19 19:33:14', '', 0, 'http://clinicaodontop.com/?post_type=tratamentos&#038;p=64', 5, 'tratamentos', '', 0),
(65, 1, '2017-01-11 15:31:43', '2017-01-11 17:31:43', '', 'img-mini-tratamento - Copia - Copia - Copia', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento-copia-copia-copia', '', '', '2017-01-11 15:31:43', '2017-01-11 17:31:43', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento-Copia-Copia-Copia.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2017-01-11 15:31:44', '2017-01-11 17:31:44', '', 'img-mini-tratamento - Copia - Copia', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento-copia-copia', '', '', '2017-01-11 15:31:44', '2017-01-11 17:31:44', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento-Copia-Copia.png', 0, 'attachment', 'image/png', 0),
(67, 1, '2017-01-11 15:31:44', '2017-01-11 17:31:44', '', 'img-mini-tratamento - Copia (2) - Copia', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento-copia-2-copia', '', '', '2017-01-11 15:31:44', '2017-01-11 17:31:44', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento-Copia-2-Copia.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2017-01-11 15:31:45', '2017-01-11 17:31:45', '', 'img-mini-tratamento - Copia (2)', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento-copia-2', '', '', '2017-01-11 15:31:45', '2017-01-11 17:31:45', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento-Copia-2.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2017-01-11 15:31:46', '2017-01-11 17:31:46', '', 'img-mini-tratamento - Copia', '', 'inherit', 'open', 'closed', '', 'img-mini-tratamento-copia', '', '', '2017-01-11 15:31:46', '2017-01-11 17:31:46', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-mini-tratamento-Copia.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2017-01-11 15:32:51', '2017-01-11 17:32:51', '', 'Odontopediatria', '', 'draft', 'closed', 'closed', '', 'odontopediatria', '', '', '2017-01-19 17:35:01', '2017-01-19 19:35:01', '', 0, 'http://clinicaodontop.com/tratamentos/extracao-de-siso-2/', 10, 'tratamentos', '', 0),
(71, 1, '2017-01-11 15:32:51', '2017-01-11 17:32:51', '', 'Endodotia', '', 'draft', 'closed', 'closed', '', 'endodotia', '', '', '2017-01-19 17:35:08', '2017-01-19 19:35:08', '', 0, 'http://clinicaodontop.com/tratamentos/extracao-de-siso-3/', 11, 'tratamentos', '', 0),
(72, 1, '2017-01-11 15:32:52', '2017-01-11 17:32:52', '', 'Ronco e Apneia', '', 'draft', 'closed', 'closed', '', 'ronco-e-apneia', '', '', '2017-01-19 17:34:55', '2017-01-19 19:34:55', '', 0, 'http://clinicaodontop.com/tratamentos/extracao-de-siso-4/', 9, 'tratamentos', '', 0),
(73, 1, '2017-01-11 15:32:53', '2017-01-11 17:32:53', '', 'Periodontia', '', 'draft', 'closed', 'closed', '', 'periodontia', '', '', '2017-01-19 17:34:47', '2017-01-19 19:34:47', '', 0, 'http://clinicaodontop.com/tratamentos/extracao-de-siso-5/', 7, 'tratamentos', '', 0),
(74, 1, '2017-01-11 15:32:53', '2017-01-11 17:32:53', '', 'DTM', '', 'draft', 'closed', 'closed', '', 'dtm', '', '', '2017-01-19 14:47:33', '2017-01-19 16:47:33', '', 0, 'http://clinicaodontop.com/tratamentos/extracao-de-siso-6/', 8, 'tratamentos', '', 0),
(75, 1, '2017-01-11 15:34:00', '2017-01-11 17:34:00', '', 'Prótese', '', 'publish', 'closed', 'closed', '', 'protese', '', '', '2017-01-19 15:51:14', '2017-01-19 17:51:14', '', 0, 'http://clinicaodontop.com/tratamentos/periodontia-2/', 2, 'tratamentos', '', 0),
(76, 1, '2017-01-11 15:34:18', '2017-01-11 17:34:18', '', 'Clareamento Dental', '', 'publish', 'closed', 'closed', '', 'clareamento-dental', '', '', '2017-01-19 16:25:52', '2017-01-19 18:25:52', '', 0, 'http://clinicaodontop.com/tratamentos/protese-2/', 3, 'tratamentos', '', 0),
(77, 1, '2017-01-11 15:34:19', '2017-01-11 17:34:19', '', 'Estética', '', 'draft', 'closed', 'closed', '', 'estetica', '', '', '2017-01-19 17:34:41', '2017-01-19 19:34:41', '', 0, 'http://clinicaodontop.com/tratamentos/protese-3/', 6, 'tratamentos', '', 0),
(78, 1, '2017-01-11 15:34:20', '2017-01-11 17:34:20', '', 'Implantodontia', '', 'publish', 'closed', 'closed', '', 'implantodontia', '', '', '2017-01-19 15:16:02', '2017-01-19 17:16:02', '', 0, 'http://clinicaodontop.com/tratamentos/protese-4/', 1, 'tratamentos', '', 0),
(79, 1, '2017-01-11 15:34:21', '2017-01-11 17:34:21', '', 'Ortodontia', '', 'publish', 'closed', 'closed', '', 'ortodontia', '', '', '2017-01-19 14:37:16', '2017-01-19 16:37:16', '', 0, 'http://clinicaodontop.com/tratamentos/protese-5/', 0, 'tratamentos', '', 0),
(80, 1, '2017-01-11 15:37:29', '2017-01-11 17:37:29', '', 'Clínica Geral', '', 'publish', 'closed', 'closed', '', 'clinica-geral', '', '', '2017-01-19 17:01:10', '2017-01-19 19:01:10', '', 0, 'http://clinicaodontop.com/tratamentos/implantodontia-2/', 4, 'tratamentos', '', 0),
(95, 1, '2017-01-12 09:19:00', '2017-01-12 11:19:00', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-01-12 09:21:31', '2017-01-12 11:21:31', '', 0, 'http://clinicaodontop.com/?page_id=95', 0, 'page', '', 0),
(96, 1, '2017-01-12 09:19:00', '2017-01-12 11:19:00', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '95-revision-v1', '', '', '2017-01-12 09:19:00', '2017-01-12 11:19:00', '', 95, 'http://clinicaodontop.com/2017/01/12/95-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2017-01-12 09:44:29', '2017-01-12 11:44:29', '', 'Posts', '', 'publish', 'closed', 'closed', '', 'acf_posts', '', '', '2017-01-12 13:47:14', '2017-01-12 15:47:14', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=99', 0, 'acf', '', 0),
(100, 1, '2017-01-12 09:46:06', '2017-01-12 11:46:06', '', 'img-blog1', '', 'inherit', 'open', 'closed', '', 'img-blog1', '', '', '2017-01-12 09:46:06', '2017-01-12 11:46:06', '', 0, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-blog1.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2017-01-12 09:46:57', '2017-01-12 11:46:57', '<div>O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo.Muito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes.\r\n\r\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado.\r\n\r\n</div>\r\n<div>\r\n<h4>Quais as diferenças?&lt;h/4&gt;</h4>\r\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\r\n\r\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser.\r\n\r\n</div>\r\n<div>\r\n<h4>Qual o melhor tipo de clareamento?</h4>\r\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo.\r\nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda.\r\n\r\n</div>\r\n<div>\r\n<h4>Por que os dentes escurecem?</h4>\r\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não.\r\nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes.\r\n\r\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc.\r\nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\r\n\r\n</div>\r\n<div>Em relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.Se você deseja fazer um clareamento, converse conosco. Aqui na Odontop temos profissionais especializados aguardando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Estética: Clareamento Dental', '', 'publish', 'open', 'open', '', 'clareamento', '', '', '2017-02-14 19:14:25', '2017-02-14 21:14:25', '', 0, 'http://clinicaodontop.com/2017/01/12/manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-2/', 2, 'post', '', 0),
(104, 1, '2017-01-12 09:46:57', '2017-01-12 11:46:57', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Manchas nos dentes, alimentos que podem amarelar seu sorriso.', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2017-01-12 09:46:57', '2017-01-12 11:46:57', '', 103, 'http://clinicaodontop.com/2017/01/12/103-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2017-01-12 09:46:58', '2017-01-12 11:46:58', 'A ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"aligncenter\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'publish', 'open', 'open', '', 'ortodontia', '', '', '2017-02-20 16:44:59', '2017-02-20 19:44:59', '', 0, 'http://clinicaodontop.com/2017/01/12/manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-3/', 1, 'post', '', 0),
(106, 1, '2017-01-12 09:46:59', '2017-01-12 11:46:59', '<div>\r\nConhecido popularmente como \"dente do juízo\", o siso tem uma notoriedade bem grande no meio dos jovens adultos. Responsável por inúmeras dores de cabeça, desconforto na mastigação e muitas vezes até entortando os demais dentes. \r\nSua erupção ocorre entre os 16 aos 20 anos normalmente, mas em muitos casos só surge perto dos 30. \r\n\r\nNem todos precisam passar pela extração dos dentes, mas é muito comum que já retirem os quatro antes mesmo de incomodar. Existem 3 maneiras do dente nascer:\r\n\r\n</div>\r\n<div>\r\n<strong>- Normal:</strong> onde ele nasce no seu espaço, sem empurrar os demais dentes e toma sua posição de auxiliador, principalmente na mastigação.\r\n\r\n<strong>- Incluso:</strong> quando ele fica preso entre o osso e não consegue sair, assim você não precisará passar pelo desconforto de ter um dente nascendo pelos 20 anos.\r\n\r\n<strong>- Semi-incluso:</strong> o mais comum, não consegue nascer completamente por estar torto, assim ele empurra os demais e pode causar o apinhamento. \r\n\r\nHá também a possibilidade de que ele nem seja gerado, pela falta do \"germe dental\", que é o responsável pela formação do dente.\r\n</div>\r\n<div>\r\n<h4>Todos precisam extrair o siso?</h4>\r\n\r\nNão é uma obrigatoriedade, embora a grande maioria dos dentistas aconselhem que a extração seja feita como forma de precaver os demais dentes. Só o diagnóstico do seu dentista poderá afirmar a necessidade de extração ou não, através do raio-x panorâmico é possível identificar qual a forma de erupção do seu dente siso para então se preparar para a cirurgia.\r\n\r\nÉ importante que seja feito um acompanhamento médico, desde os cuidados pré cirúrgico quanto o pós. A recuperação é muito importante, por isso siga as recomendações do seu dentista. \r\nLembre-se de que a recuperação é quase tão importante quanto a extração. Não pegue muito sol, não se exalte e mantenha se alimentando de alimentos gelados no início. \r\n\r\nE se ainda está em dúvida em extrair ou não, marque uma avaliação conosco aqui na Odontop. Se a extração for necessária, ficaremos felizes em ajudar.\r\n</div>\r\n\r\n<h5>Equipe Odontop</h5>', 'Extração do Siso', '', 'publish', 'open', 'open', '', 'extracao-do-siso', '', '', '2017-01-20 16:57:47', '2017-01-20 18:57:47', '', 0, 'http://clinicaodontop.com/2017/01/12/manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-4/', 3, 'post', '', 0),
(108, 1, '2017-01-12 09:46:59', '2017-01-12 11:46:59', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Manchas nos dentes, alimentos que podem amarelar seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-01-12 09:46:59', '2017-01-12 11:46:59', '', 105, 'http://clinicaodontop.com/2017/01/12/105-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2017-01-12 09:47:00', '2017-01-12 11:47:00', '<div>\r\n\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Mantenha seu sorriso. Faca-nos uma visita', '', 'publish', 'open', 'open', '', 'saude', '', '', '2017-02-14 19:17:10', '2017-02-14 21:17:10', '', 0, 'http://clinicaodontop.com/2017/01/12/manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso-6/', 0, 'post', '', 0),
(110, 1, '2017-01-12 09:47:00', '2017-01-12 11:47:00', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Manchas nos dentes, alimentos que podem amarelar seu sorriso.', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2017-01-12 09:47:00', '2017-01-12 11:47:00', '', 106, 'http://clinicaodontop.com/2017/01/12/106-revision-v1/', 0, 'revision', '', 0),
(112, 1, '2017-01-12 09:47:02', '2017-01-12 11:47:02', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Manchas nos dentes, alimentos que podem amarelar seu sorriso.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-12 09:47:02', '2017-01-12 11:47:02', '', 109, 'http://clinicaodontop.com/2017/01/12/109-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2017-01-12 10:43:13', '2017-01-12 12:43:13', '', 'icon-calendar', '', 'inherit', 'open', 'closed', '', 'icon-calendar', '', '', '2017-01-12 10:43:13', '2017-01-12 12:43:13', '', 0, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon-calendar.png', 0, 'attachment', 'image/png', 0),
(116, 1, '2017-01-12 13:10:17', '2017-01-12 15:10:17', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-12 13:10:17', '2017-01-12 15:10:17', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2017-01-12 13:12:31', '2017-01-12 15:12:31', '', 'Topo', '', 'publish', 'closed', 'closed', '', 'topo', '', '', '2017-02-14 19:26:11', '2017-02-14 21:26:11', '', 0, 'http://clinicaodontop.com/?page_id=118', 0, 'page', '', 0),
(119, 1, '2017-01-12 13:12:31', '2017-01-12 15:12:31', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-01-12 13:12:31', '2017-01-12 15:12:31', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2017-01-12 13:13:27', '2017-01-12 15:13:27', '', 'Topo', '', 'publish', 'closed', 'closed', '', 'acf_topo', '', '', '2017-01-12 13:13:27', '2017-01-12 15:13:27', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=120', 0, 'acf', '', 0),
(121, 1, '2017-01-12 13:13:41', '2017-01-12 15:13:41', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1', '', '', '2017-01-12 13:13:41', '2017-01-12 15:13:41', '', 118, 'http://clinicaodontop.com/wp-content/uploads/2017/01/banner1.png', 0, 'attachment', 'image/png', 0),
(122, 1, '2017-01-12 13:14:22', '2017-01-12 15:14:22', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-01-12 13:14:22', '2017-01-12 15:14:22', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2017-01-12 13:16:04', '2017-01-12 15:16:04', '', 'Agendar Consulta', '', 'publish', 'closed', 'closed', '', 'agendar-consulta', '', '', '2017-01-19 14:57:04', '2017-01-19 16:57:04', '', 0, 'http://clinicaodontop.com/?page_id=123', 0, 'page', '', 0),
(124, 1, '2017-01-12 13:16:04', '2017-01-12 15:16:04', '', 'Agendar Consulta', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2017-01-12 13:16:04', '2017-01-12 15:16:04', '', 123, 'http://clinicaodontop.com/123-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2017-01-12 13:18:37', '2017-01-12 15:18:37', '', 'Agendar Consulta', '', 'publish', 'closed', 'closed', '', 'acf_agendar-consulta', '', '', '2017-01-12 13:18:37', '2017-01-12 15:18:37', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=125', 0, 'acf', '', 0),
(126, 1, '2017-01-12 13:19:14', '2017-01-12 15:19:14', '', 'Agendar Consulta', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2017-01-12 13:19:14', '2017-01-12 15:19:14', '', 123, 'http://clinicaodontop.com/123-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2017-01-12 13:38:07', '2017-01-12 15:38:07', '', 'Faixa Laranja Home', '', 'publish', 'closed', 'closed', '', 'faixa-laranja-home', '', '', '2017-01-23 12:48:56', '2017-01-23 14:48:56', '', 0, 'http://clinicaodontop.com/?page_id=127', 0, 'page', '', 0),
(128, 1, '2017-01-12 13:38:07', '2017-01-12 15:38:07', '', 'Faixa Laranja Home', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-01-12 13:38:07', '2017-01-12 15:38:07', '', 127, 'http://clinicaodontop.com/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2017-01-12 13:39:47', '2017-01-12 15:39:47', '', 'Faixa Laranja Home', '', 'publish', 'closed', 'closed', '', 'acf_faixa-laranja-home', '', '', '2017-01-12 13:39:47', '2017-01-12 15:39:47', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=129', 0, 'acf', '', 0),
(130, 1, '2017-01-12 13:40:06', '2017-01-12 15:40:06', '', 'img-spe2', '', 'inherit', 'open', 'closed', '', 'img-spe2', '', '', '2017-01-12 13:40:06', '2017-01-12 15:40:06', '', 127, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-spe2.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2017-01-12 13:40:07', '2017-01-12 15:40:07', '', 'img-spe3', '', 'inherit', 'open', 'closed', '', 'img-spe3', '', '', '2017-01-12 13:40:07', '2017-01-12 15:40:07', '', 127, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-spe3.png', 0, 'attachment', 'image/png', 0),
(132, 1, '2017-01-12 13:40:08', '2017-01-12 15:40:08', '', 'img-spe1', '', 'inherit', 'open', 'closed', '', 'img-spe1', '', '', '2017-01-12 13:40:08', '2017-01-12 15:40:08', '', 127, 'http://clinicaodontop.com/wp-content/uploads/2017/01/img-spe1.png', 0, 'attachment', 'image/png', 0),
(133, 1, '2017-01-12 13:40:58', '2017-01-12 15:40:58', '', 'Faixa Laranja Home', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-01-12 13:40:58', '2017-01-12 15:40:58', '', 127, 'http://clinicaodontop.com/127-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2017-01-12 13:46:41', '2017-01-12 15:46:41', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Saúde', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-12 13:46:41', '2017-01-12 15:46:41', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(135, 1, '2017-01-12 13:47:22', '2017-01-12 15:47:22', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Saúde', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-12 13:47:22', '2017-01-12 15:47:22', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(136, 1, '2017-01-12 13:47:51', '2017-01-12 15:47:51', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Prevenção', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2017-01-12 13:47:51', '2017-01-12 15:47:51', '', 106, 'http://clinicaodontop.com/106-revision-v1/', 0, 'revision', '', 0),
(139, 1, '2017-01-19 13:51:33', '2017-01-19 15:51:33', '', 'Campina Grande do Sul', '', 'inherit', 'closed', 'closed', '', '19-autosave-v1', '', '', '2017-01-19 13:51:33', '2017-01-19 15:51:33', '', 19, 'http://clinicaodontop.com/19-autosave-v1/', 0, 'revision', '', 0),
(140, 1, '2017-01-19 13:55:39', '2017-01-19 15:55:39', '', 'Foto em 07-11-16 às 11.44', '', 'inherit', 'open', 'closed', '', 'foto-em-07-11-16-as-11-44', '', '', '2017-01-19 13:55:39', '2017-01-19 15:55:39', '', 57, 'http://clinicaodontop.com/wp-content/uploads/2017/01/Foto-em-07-11-16-às-11.44.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2017-01-19 13:56:56', '2017-01-19 15:56:56', '', 'Marivânia Silvestre Brito', '', 'inherit', 'closed', 'closed', '', '55-autosave-v1', '', '', '2017-01-19 13:56:56', '2017-01-19 15:56:56', '', 55, 'http://clinicaodontop.com/55-autosave-v1/', 0, 'revision', '', 0),
(142, 1, '2017-01-19 14:02:45', '2017-01-19 16:02:45', '', 'MARINALVA', '', 'inherit', 'open', 'closed', '', 'marinalva', '', '', '2017-01-19 14:02:45', '2017-01-19 16:02:45', '', 55, 'http://clinicaodontop.com/wp-content/uploads/2017/01/MARINALVA.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2017-01-19 14:15:06', '2017-01-19 16:15:06', '', 'odontop_implante', '', 'inherit', 'open', 'closed', '', 'odontop_implante', '', '', '2017-01-19 14:15:06', '2017-01-19 16:15:06', '', 78, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_implante.jpg', 0, 'attachment', 'image/jpeg', 0),
(145, 1, '2017-01-19 14:29:29', '2017-01-19 16:29:29', '', 'odontop_aparelho_estetico', '', 'inherit', 'open', 'closed', '', 'odontop_aparelho_estetico', '', '', '2017-01-19 14:29:29', '2017-01-19 16:29:29', '', 79, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_aparelho_estetico.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 1, '2017-01-19 14:29:31', '2017-01-19 16:29:31', '', 'odontop_aparelho_metalico', '', 'inherit', 'open', 'closed', '', 'odontop_aparelho_metalico', '', '', '2017-01-19 14:29:31', '2017-01-19 16:29:31', '', 79, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_aparelho_metalico.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 1, '2017-01-19 14:29:33', '2017-01-19 16:29:33', '', 'odontop_aparelho_móvel', '', 'inherit', 'open', 'closed', '', 'odontop_aparelho_movel', '', '', '2017-01-19 14:29:33', '2017-01-19 16:29:33', '', 79, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_aparelho_móvel.jpg', 0, 'attachment', 'image/jpeg', 0),
(149, 1, '2017-01-19 14:36:21', '2017-01-19 16:36:21', '', 'odontop_aparelho_autoligavel', '', 'inherit', 'open', 'closed', '', 'odontop_aparelho_autoligavel', '', '', '2017-01-19 14:36:21', '2017-01-19 16:36:21', '', 79, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_aparelho_autoligavel.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 1, '2017-01-19 14:44:10', '2017-01-19 16:44:10', '<div>\r\nO flúor é um elemento que auxilia a dentição a permanecer protegida contra cáries, sua fórmula fortalece o esmalte dental.\r\nEle faz parte da vida das pessoas desde a infância, muitos ainda devem se lembrar dos inúmeros bochechos durante o ensino fundamental, ou das aplicações em gel em consultórios odontológicos.\r\nDesde a década de 30 é comum adicionar flúor na água potável distribuída pelas empresas de saneamento locais, é interessante confirmar se o abastecimento de água da sua região contém flúor.\r\n\r\n</div>\r\n<div>\r\nQuando ingerido, em menor quantidade ele levará um tempo maior para o fortalecimento do esmalte dental, mas em adultos ele funciona como auxílio.\r\nMais importante é durante a troca de dentes, os bochechos com flúor são mais concentrados, auxiliando a formação dos novos dentes, para que no decorrer da vida, a quantidade menor a ser ingerida seja somente um auxiliador.\r\n\r\n<div>\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\n<div>\r\nQuando ingerido, em menor quantidade ele levará um tempo maior para o fortalecimento do esmalte dental, mas em adultos ele funciona como auxílio.\r\nMais importante é durante a troca de dentes, os bochechos com flúor são mais concentrados, auxiliando a formação dos novos dentes, para que no decorrer da vida, a quantidade menor a ser ingerida seja somente um auxiliador.\r\n\r\n<div>\r\n<h5>Equipe Odontop</h5>', 'Flúor e seus benefícios para um sorriso saudável.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-19 14:44:10', '2017-01-19 16:44:10', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(151, 1, '2017-01-19 14:53:36', '2017-01-19 16:53:36', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-01-19 14:53:36', '2017-01-19 16:53:36', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(152, 1, '2017-01-19 14:54:28', '2017-01-19 16:54:28', '', 'Tratamentos', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2017-01-19 14:54:28', '2017-01-19 16:54:28', '', 59, 'http://clinicaodontop.com/59-revision-v1/', 0, 'revision', '', 0),
(153, 1, '2017-01-19 14:54:59', '2017-01-19 16:54:59', '', 'Faixa Laranja Home', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-01-19 14:54:59', '2017-01-19 16:54:59', '', 127, 'http://clinicaodontop.com/127-revision-v1/', 0, 'revision', '', 0),
(154, 1, '2017-01-19 14:56:57', '2017-01-19 16:56:57', '', 'Agendar Consulta', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2017-01-19 14:56:57', '2017-01-19 16:56:57', '', 123, 'http://clinicaodontop.com/123-revision-v1/', 0, 'revision', '', 0),
(155, 1, '2017-01-19 15:02:49', '2017-01-19 17:02:49', '', 'cezar', '', 'inherit', 'open', 'closed', '', 'cezar', '', '', '2017-01-19 15:02:49', '2017-01-19 17:02:49', '', 57, 'http://clinicaodontop.com/wp-content/uploads/2017/01/cezar.jpg', 0, 'attachment', 'image/jpeg', 0),
(156, 1, '2017-01-19 15:10:02', '2017-01-19 17:10:02', 'As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n\r\nAs manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados. Alimentos e bebidas proporcionam prazer mas se não houver uma boa higienização será difícil combater o amarelamento. Os dentes mudam de pigmentação\r\n<h5>Equipe Odontop</h5>\r\nLorem ipsum dolor sit amet\r\nconsectetur adipisicing elit.', 'Prevenção', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2017-01-19 15:10:02', '2017-01-19 17:10:02', '', 106, 'http://clinicaodontop.com/106-revision-v1/', 0, 'revision', '', 0),
(158, 1, '2017-01-19 15:14:18', '2017-01-19 17:14:18', 'O flúor é um elemento que auxilia a dentição a permanecer protegida contra cáries, sua fórmula fortalece o esmalte dental.\r\nEle faz parte da vida das pessoas desde a infância, muitos ainda devem se lembrar dos inúmeros bochechos durante o ensino fundamental, ou das aplicações em gel em consultórios odontológicos.\r\nDesde a década de 30 é comum adicionar flúor na água potável distribuída pelas empresas de saneamento locais, é interessante confirmar se o abastecimento de água da sua região contém flúor.\r\n\r\nQuando ingerido, em menor quantidade ele levará um tempo maior para o fortalecimento do esmalte dental, mas em adultos ele funciona como auxílio.\r\nMais importante é durante a troca de dentes, os bochechos com flúor são mais concentrados, auxiliando a formação dos novos dentes, para que no decorrer da vida, a quantidade menor a ser ingerida seja somente um auxiliador.\r\n<h4>Separamos hoje alguns alimentos que propiciam as manchas nos dentes:</h4>\r\nQuando ingerido, em menor quantidade ele levará um tempo maior para o fortalecimento do esmalte dental, mas em adultos ele funciona como auxílio.\r\nMais importante é durante a troca de dentes, os bochechos com flúor são mais concentrados, auxiliando a formação dos novos dentes, para que no decorrer da vida, a quantidade menor a ser ingerida seja somente um auxiliador.\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Flúor e seus benefícios para um sorriso saudável.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-19 15:14:18', '2017-01-19 17:14:18', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2017-01-19 15:15:10', '2017-01-19 17:15:10', '', 'odontop_implante_protocolo', '', 'inherit', 'open', 'closed', '', 'odontop_implante_protocolo', '', '', '2017-01-19 15:15:10', '2017-01-19 17:15:10', '', 78, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_implante_protocolo.jpg', 0, 'attachment', 'image/jpeg', 0),
(160, 1, '2017-01-19 15:15:20', '2017-01-19 17:15:20', '', 'odontop_implante2', '', 'inherit', 'open', 'closed', '', 'odontop_implante2', '', '', '2017-01-19 15:15:20', '2017-01-19 17:15:20', '', 78, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_implante2.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 1, '2017-01-19 15:15:35', '2017-01-19 17:15:35', '', 'odontop_implante3', '', 'inherit', 'open', 'closed', '', 'odontop_implante3', '', '', '2017-01-19 15:15:35', '2017-01-19 17:15:35', '', 78, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_implante3.jpg', 0, 'attachment', 'image/jpeg', 0),
(162, 1, '2017-01-19 15:15:45', '2017-01-19 17:15:45', '', 'odontop_implante', '', 'inherit', 'open', 'closed', '', 'odontop_implante-2', '', '', '2017-01-19 15:15:45', '2017-01-19 17:15:45', '', 78, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_implante-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(163, 1, '2017-01-19 15:32:02', '2017-01-19 17:32:02', '', 'odontop_protese', '', 'inherit', 'open', 'closed', '', 'odontop_protese', '', '', '2017-01-19 15:32:02', '2017-01-19 17:32:02', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_protese.jpg', 0, 'attachment', 'image/jpeg', 0),
(164, 1, '2017-01-19 15:35:38', '2017-01-19 17:35:38', '', 'odontop_protese', '', 'inherit', 'open', 'closed', '', 'odontop_protese-2', '', '', '2017-01-19 15:35:38', '2017-01-19 17:35:38', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_protese-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 1, '2017-01-19 15:50:22', '2017-01-19 17:50:22', '', 'odontop_prótese4', '', 'inherit', 'open', 'closed', '', 'odontop_protese4', '', '', '2017-01-19 15:50:22', '2017-01-19 17:50:22', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_prótese4.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2017-01-19 15:50:34', '2017-01-19 17:50:34', '', 'odontop_prótese', '', 'inherit', 'open', 'closed', '', 'odontop_protese-3', '', '', '2017-01-19 15:50:34', '2017-01-19 17:50:34', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_prótese.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2017-01-19 15:50:43', '2017-01-19 17:50:43', '', 'odontop_prótese2', '', 'inherit', 'open', 'closed', '', 'odontop_protese2', '', '', '2017-01-19 15:50:43', '2017-01-19 17:50:43', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_prótese2.jpg', 0, 'attachment', 'image/jpeg', 0),
(168, 1, '2017-01-19 15:50:52', '2017-01-19 17:50:52', '', 'odontop_prótese3', '', 'inherit', 'open', 'closed', '', 'odontop_protese3', '', '', '2017-01-19 15:50:52', '2017-01-19 17:50:52', '', 75, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_prótese3.jpg', 0, 'attachment', 'image/jpeg', 0),
(169, 1, '2017-01-19 16:05:29', '2017-01-19 18:05:29', '', 'odontop_clareamento', '', 'inherit', 'open', 'closed', '', 'odontop_clareamento', '', '', '2017-01-19 16:05:29', '2017-01-19 18:05:29', '', 76, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clareamento.jpg', 0, 'attachment', 'image/jpeg', 0),
(170, 1, '2017-01-19 16:25:17', '2017-01-19 18:25:17', '', 'odontop_clareamento4', '', 'inherit', 'open', 'closed', '', 'odontop_clareamento4', '', '', '2017-01-19 16:25:17', '2017-01-19 18:25:17', '', 76, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clareamento4.jpg', 0, 'attachment', 'image/jpeg', 0),
(171, 1, '2017-01-19 16:25:26', '2017-01-19 18:25:26', '', 'odontop_clareamento2', '', 'inherit', 'open', 'closed', '', 'odontop_clareamento2', '', '', '2017-01-19 16:25:26', '2017-01-19 18:25:26', '', 76, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clareamento2.jpg', 0, 'attachment', 'image/jpeg', 0),
(172, 1, '2017-01-19 16:25:37', '2017-01-19 18:25:37', '', 'odontop_clareamento1', '', 'inherit', 'open', 'closed', '', 'odontop_clareamento1', '', '', '2017-01-19 16:25:37', '2017-01-19 18:25:37', '', 76, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clareamento1.jpg', 0, 'attachment', 'image/jpeg', 0),
(173, 1, '2017-01-19 16:25:44', '2017-01-19 18:25:44', '', 'odontop_clareamento3', '', 'inherit', 'open', 'closed', '', 'odontop_clareamento3', '', '', '2017-01-19 16:25:44', '2017-01-19 18:25:44', '', 76, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clareamento3.jpg', 0, 'attachment', 'image/jpeg', 0),
(174, 1, '2017-01-19 16:44:47', '2017-01-19 18:44:47', '', 'odontop_clinica', '', 'inherit', 'open', 'closed', '', 'odontop_clinica', '', '', '2017-01-19 16:44:47', '2017-01-19 18:44:47', '', 80, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clinica.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2017-01-19 16:52:23', '2017-01-19 18:52:23', '', 'odontop_clinica1', '', 'inherit', 'open', 'closed', '', 'odontop_clinica1', '', '', '2017-01-19 16:52:23', '2017-01-19 18:52:23', '', 80, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clinica1.jpg', 0, 'attachment', 'image/jpeg', 0),
(176, 1, '2017-01-19 16:52:36', '2017-01-19 18:52:36', '', 'odontop_clinica4', '', 'inherit', 'open', 'closed', '', 'odontop_clinica4', '', '', '2017-01-19 16:52:36', '2017-01-19 18:52:36', '', 80, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clinica4.jpg', 0, 'attachment', 'image/jpeg', 0),
(177, 1, '2017-01-19 16:52:48', '2017-01-19 18:52:48', '', 'odontop_clinica3', '', 'inherit', 'open', 'closed', '', 'odontop_clinica3', '', '', '2017-01-19 16:52:48', '2017-01-19 18:52:48', '', 80, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clinica3.jpg', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2017-01-19 16:53:17', '2017-01-19 18:53:17', '', 'Clínica Geral', '', 'inherit', 'closed', 'closed', '', '80-autosave-v1', '', '', '2017-01-19 16:53:17', '2017-01-19 18:53:17', '', 80, 'http://clinicaodontop.com/80-autosave-v1/', 0, 'revision', '', 0),
(179, 1, '2017-01-19 16:56:04', '2017-01-19 18:56:04', '', 'odontop_clinica2', '', 'inherit', 'open', 'closed', '', 'odontop_clinica2', '', '', '2017-01-19 16:56:04', '2017-01-19 18:56:04', '', 80, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_clinica2.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2017-01-19 17:12:06', '2017-01-19 19:12:06', '', 'odontop_siso', '', 'inherit', 'open', 'closed', '', 'odontop_siso', '', '', '2017-01-19 17:12:06', '2017-01-19 19:12:06', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_siso.jpg', 0, 'attachment', 'image/jpeg', 0),
(181, 1, '2017-01-19 17:31:30', '2017-01-19 19:31:30', '', 'odontop_siso1', '', 'inherit', 'open', 'closed', '', 'odontop_siso1', '', '', '2017-01-19 17:31:30', '2017-01-19 19:31:30', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_siso1.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2017-01-19 17:31:41', '2017-01-19 19:31:41', '', 'odontop_siso3', '', 'inherit', 'open', 'closed', '', 'odontop_siso3', '', '', '2017-01-19 17:31:41', '2017-01-19 19:31:41', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_siso3.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 1, '2017-01-19 17:31:52', '2017-01-19 19:31:52', '', 'odontop_siso4', '', 'inherit', 'open', 'closed', '', 'odontop_siso4', '', '', '2017-01-19 17:31:52', '2017-01-19 19:31:52', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_siso4.jpg', 0, 'attachment', 'image/jpeg', 0),
(184, 1, '2017-01-19 17:32:00', '2017-01-19 19:32:00', '', 'odontop_siso2', '', 'inherit', 'open', 'closed', '', 'odontop_siso2', '', '', '2017-01-19 17:32:00', '2017-01-19 19:32:00', '', 64, 'http://clinicaodontop.com/wp-content/uploads/2017/01/odontop_siso2.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 1, '2017-02-20 16:28:51', '2017-02-20 19:28:51', '<div>\r\n\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Mantenha seu sorriso. Faca-nos uma visita', '', 'inherit', 'closed', 'closed', '', '109-autosave-v1', '', '', '2017-02-20 16:28:51', '2017-02-20 19:28:51', '', 109, 'http://clinicaodontop.com/109-autosave-v1/', 0, 'revision', '', 0),
(186, 1, '2017-01-19 17:43:48', '2017-01-19 19:43:48', '', 'A consulta regular ao dentista pode salvar o seu sorriso', '', 'inherit', 'open', 'closed', '', 'a-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso', '', '', '2017-01-19 17:43:48', '2017-01-19 19:43:48', '', 109, 'http://clinicaodontop.com/wp-content/uploads/2017/01/A-consulta-regular-ao-dentista-pode-salvar-o-seu-sorriso.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2017-01-19 17:45:56', '2017-01-19 19:45:56', '<div>\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.\r\n\r\nÉ comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n\r\n<h4><strong>- Prevenção</strong></h4>\r\n\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\n\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\n\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\n\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'A consulta regular ao dentista pode salvar o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-19 17:45:56', '2017-01-19 19:45:56', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2017-02-20 16:37:41', '2017-02-20 19:37:41', 'A ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\n\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\n<img class=\"aligncenter\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\n\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\n\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\n<h4>Devo procurar um especialista em ortodontia?</h4>\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\n\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\n<h4>Adultos podem usar aparelho?</h4>\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\n\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\n\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-autosave-v1', '', '', '2017-02-20 16:37:41', '2017-02-20 19:37:41', '', 105, 'http://clinicaodontop.com/105-autosave-v1/', 0, 'revision', '', 0),
(189, 1, '2017-01-20 15:59:44', '2017-01-20 17:59:44', '', 'Ortodontia-mordida-300x253', '', 'inherit', 'open', 'closed', '', 'ortodontia-mordida-300x253', '', '', '2017-01-20 15:59:44', '2017-01-20 17:59:44', '', 105, 'http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253.png', 0, 'attachment', 'image/png', 0),
(190, 1, '2017-01-20 16:05:03', '2017-01-20 18:05:03', '', 'Ortodontia-transforme-o-seu-sorriso', '', 'inherit', 'open', 'closed', '', 'ortodontia-transforme-o-seu-sorriso', '', '', '2017-01-20 16:05:03', '2017-01-20 18:05:03', '', 105, 'http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-transforme-o-seu-sorriso.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2017-01-20 16:05:10', '2017-01-20 18:05:10', '<div>\r\nA ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento.  O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. \r\n\r\nExistem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\n<div>\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano. \r\n\r\n<img src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" alt=\"\" width=\"300\" height=\"253\" class=\"alignnone size-medium wp-image-189\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso. \r\n\r\n</div>\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\n\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos. \r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo. \r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado. \r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\n\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados. \r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais. \r\n\r\nPor isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você. \r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-01-20 16:05:10', '2017-01-20 18:05:10', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(192, 1, '2017-01-20 16:28:01', '2017-01-20 18:28:01', '<div>\nO clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo. \n\nMuito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes. \n\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado. \n\n</div>\n<div>\n<h4>Quais as diferenças?<h/4>\n\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\n\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser. \n\n</div>\n<div>\n<h4>Qual o melhor tipo de clareamento?</h4>\n\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo. \nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda. \n\n</div>\n<div>\n<h4>Por que os dentes escurecem?</h4>\n\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não. \nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes. \n\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc. \nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\n\n</div>\n<div>\nEm relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.\n\nSe você deseja fazer um clareamento, converse conosco. Aqui na Odontop tempos profissionais aguardando por você.\n\n\n</div>\n<div>\n<h5>Equipe Odontop</h5>', 'Clareamento Dental', '', 'inherit', 'closed', 'closed', '', '103-autosave-v1', '', '', '2017-01-20 16:28:01', '2017-01-20 18:28:01', '', 103, 'http://clinicaodontop.com/103-autosave-v1/', 0, 'revision', '', 0),
(193, 1, '2017-01-20 16:28:34', '2017-01-20 18:28:34', '', 'clareamento-dental', '', 'inherit', 'open', 'closed', '', 'clareamento-dental-2', '', '', '2017-01-20 16:28:34', '2017-01-20 18:28:34', '', 103, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clareamento-dental.jpg', 0, 'attachment', 'image/jpeg', 0),
(194, 1, '2017-01-20 16:29:00', '2017-01-20 18:29:00', '<div>\r\nO clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo. \r\n\r\nMuito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes. \r\n\r\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado. \r\n\r\n</div>\r\n<div>\r\n<h4>Quais as diferenças?<h/4>\r\n\r\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\r\n\r\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser. \r\n\r\n</div>\r\n<div>\r\n<h4>Qual o melhor tipo de clareamento?</h4>\r\n\r\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo. \r\nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda. \r\n\r\n</div>\r\n<div>\r\n<h4>Por que os dentes escurecem?</h4>\r\n\r\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não. \r\nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes. \r\n\r\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc. \r\nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\r\n\r\n</div>\r\n<div>\r\nEm relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.\r\n\r\nSe você deseja fazer um clareamento, converse conosco. Aqui na Odontop tempos profissionais aguardando por você.\r\n\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>', 'Clareamento Dental', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2017-01-20 16:29:00', '2017-01-20 18:29:00', '', 103, 'http://clinicaodontop.com/103-revision-v1/', 0, 'revision', '', 0),
(195, 1, '2017-01-20 16:32:18', '2017-01-20 18:32:18', '<div>\r\nO clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo. \r\n\r\nMuito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes. \r\n\r\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado. \r\n\r\n</div>\r\n<div>\r\n<h4>Quais as diferenças?<h/4>\r\n\r\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\r\n\r\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser. \r\n\r\n</div>\r\n<div>\r\n<h4>Qual o melhor tipo de clareamento?</h4>\r\n\r\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo. \r\nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda. \r\n\r\n</div>\r\n<div>\r\n<h4>Por que os dentes escurecem?</h4>\r\n\r\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não. \r\nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes. \r\n\r\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc. \r\nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\r\n\r\n</div>\r\n<div>\r\nEm relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.\r\n\r\nSe você deseja fazer um clareamento, converse conosco. Aqui na Odontop temos profissionais especializados aguardando por você.\r\n\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>', 'Clareamento Dental', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2017-01-20 16:32:18', '2017-01-20 18:32:18', '', 103, 'http://clinicaodontop.com/103-revision-v1/', 0, 'revision', '', 0),
(196, 1, '2017-01-20 16:56:16', '2017-01-20 18:56:16', '<div>\nConhecido popularmente como \"dente do juízo\", o siso tem uma notoriedade bem grande no meio dos jovens adultos. Responsável por inúmeras dores de cabeça, desconforto na mastigação e muitas vezes até entortando os demais dentes. \nSua erupção ocorre entre os 16 aos 20 anos normalmente, mas em muitos casos só surge perto dos 30. \n\nNem todos precisam passar pela extração dos dentes, mas é muito comum que já retirem os quatro antes mesmo de incomodar. Existem 3 maneiras do dente nascer:\n\n</div>\n<div>\n<strong>- Normal:</strong> onde ele nasce no seu espaço, sem empurrar os demais dentes e toma sua posição de auxiliador, principalmente na mastigação.\n\n<strong>- Incluso:</strong> quando ele fica preso entre o osso e não consegue sair, assim você não precisará passar pelo desconforto de ter um dente nascendo pelos 20 anos.\n\n<strong>- Semi-incluso:</strong> o mais comum, não consegue nascer completamente por estar torto, assim ele empurra os demais e pode causar o apinhamento. \n\nHá também a possibilidade de que ele nem seja gerado, pela falta do \"germe dental\", que é o responsável pela formação do dente.\n</div>\n<div>\n<h4>Todos precisam extrair o siso?</h4>\n\nNão é uma obrigatoriedade, embora a grande maioria dos dentistas aconselhem que a extração seja feita como forma de precaver os demais dentes. Só o diagnóstico do seu dentista poderá afirmar a necessidade de extração ou não, através do raio-x panorâmico é possível identificar qual a forma de erupção do seu dente siso para então se preparar para a cirurgia.\n\n\nSe você você acabou de passar por isso, lembre-se de que a recuperação é quase tão importante quanto a extração. Não pegue muito sol, não se exalte e mantenha se alimentando de alimentos gelados no início. \nE se ainda está em dúvida em extrair ou não, marque uma avaliação conosco aqui na Odontop. Se a extração for necessária, ficaremos felizes em ajudar.\n</div>\n\n<h5>Equipe Odontop</h5>', 'Extração do Siso', '', 'inherit', 'closed', 'closed', '', '106-autosave-v1', '', '', '2017-01-20 16:56:16', '2017-01-20 18:56:16', '', 106, 'http://clinicaodontop.com/106-autosave-v1/', 0, 'revision', '', 0),
(197, 1, '2017-01-20 16:55:18', '2017-01-20 18:55:18', '', 'extração do siso', '', 'inherit', 'open', 'closed', '', 'extracao-do-siso', '', '', '2017-01-20 16:55:18', '2017-01-20 18:55:18', '', 106, 'http://clinicaodontop.com/wp-content/uploads/2017/01/extração-do-siso.jpg', 0, 'attachment', 'image/jpeg', 0),
(198, 1, '2017-01-20 16:57:47', '2017-01-20 18:57:47', '<div>\r\nConhecido popularmente como \"dente do juízo\", o siso tem uma notoriedade bem grande no meio dos jovens adultos. Responsável por inúmeras dores de cabeça, desconforto na mastigação e muitas vezes até entortando os demais dentes. \r\nSua erupção ocorre entre os 16 aos 20 anos normalmente, mas em muitos casos só surge perto dos 30. \r\n\r\nNem todos precisam passar pela extração dos dentes, mas é muito comum que já retirem os quatro antes mesmo de incomodar. Existem 3 maneiras do dente nascer:\r\n\r\n</div>\r\n<div>\r\n<strong>- Normal:</strong> onde ele nasce no seu espaço, sem empurrar os demais dentes e toma sua posição de auxiliador, principalmente na mastigação.\r\n\r\n<strong>- Incluso:</strong> quando ele fica preso entre o osso e não consegue sair, assim você não precisará passar pelo desconforto de ter um dente nascendo pelos 20 anos.\r\n\r\n<strong>- Semi-incluso:</strong> o mais comum, não consegue nascer completamente por estar torto, assim ele empurra os demais e pode causar o apinhamento. \r\n\r\nHá também a possibilidade de que ele nem seja gerado, pela falta do \"germe dental\", que é o responsável pela formação do dente.\r\n</div>\r\n<div>\r\n<h4>Todos precisam extrair o siso?</h4>\r\n\r\nNão é uma obrigatoriedade, embora a grande maioria dos dentistas aconselhem que a extração seja feita como forma de precaver os demais dentes. Só o diagnóstico do seu dentista poderá afirmar a necessidade de extração ou não, através do raio-x panorâmico é possível identificar qual a forma de erupção do seu dente siso para então se preparar para a cirurgia.\r\n\r\nÉ importante que seja feito um acompanhamento médico, desde os cuidados pré cirúrgico quanto o pós. A recuperação é muito importante, por isso siga as recomendações do seu dentista. \r\nLembre-se de que a recuperação é quase tão importante quanto a extração. Não pegue muito sol, não se exalte e mantenha se alimentando de alimentos gelados no início. \r\n\r\nE se ainda está em dúvida em extrair ou não, marque uma avaliação conosco aqui na Odontop. Se a extração for necessária, ficaremos felizes em ajudar.\r\n</div>\r\n\r\n<h5>Equipe Odontop</h5>', 'Extração do Siso', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2017-01-20 16:57:47', '2017-01-20 18:57:47', '', 106, 'http://clinicaodontop.com/106-revision-v1/', 0, 'revision', '', 0),
(200, 1, '2017-01-23 12:47:16', '2017-01-23 14:47:16', '', 'Faixa Laranja Home', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-01-23 12:47:16', '2017-01-23 14:47:16', '', 127, 'http://clinicaodontop.com/127-revision-v1/', 0, 'revision', '', 0),
(201, 1, '2017-01-23 12:48:56', '2017-01-23 14:48:56', '', 'Faixa Laranja Home', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-01-23 12:48:56', '2017-01-23 14:48:56', '', 127, 'http://clinicaodontop.com/127-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(202, 1, '2017-01-23 13:39:18', '2017-01-23 15:39:18', '<div>\r\nA ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento.  O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise. \r\n\r\nExistem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\n<div>\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano. \r\n\r\n<img src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" alt=\"\" width=\"300\" height=\"253\" class=\"alignnone size-medium wp-image-189\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso. \r\n\r\n</div>\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\n\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos. \r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo. \r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado. \r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\n\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados. \r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais. \r\n\r\nPor isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você. \r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-01-23 13:39:18', '2017-01-23 15:39:18', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(203, 1, '2017-01-23 13:39:39', '2017-01-23 15:39:39', '<div>\r\nO clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo. \r\n\r\nMuito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes. \r\n\r\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado. \r\n\r\n</div>\r\n<div>\r\n<h4>Quais as diferenças?<h/4>\r\n\r\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\r\n\r\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser. \r\n\r\n</div>\r\n<div>\r\n<h4>Qual o melhor tipo de clareamento?</h4>\r\n\r\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo. \r\nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda. \r\n\r\n</div>\r\n<div>\r\n<h4>Por que os dentes escurecem?</h4>\r\n\r\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não. \r\nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes. \r\n\r\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc. \r\nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\r\n\r\n</div>\r\n<div>\r\nEm relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.\r\n\r\nSe você deseja fazer um clareamento, converse conosco. Aqui na Odontop temos profissionais especializados aguardando por você.\r\n\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>', 'Clareamento Dental', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2017-01-23 13:39:39', '2017-01-23 15:39:39', '', 103, 'http://clinicaodontop.com/103-revision-v1/', 0, 'revision', '', 0),
(204, 1, '2017-01-25 16:34:16', '2017-01-25 18:34:16', '<div>A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.</div>\r\n<div></div>\r\n<div>A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'A consulta regular ao dentista pode salvar o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-25 16:34:16', '2017-01-25 18:34:16', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(205, 1, '2017-01-25 16:34:49', '2017-01-25 18:34:49', '<div>A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'A consulta regular ao dentista pode salvar o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-25 16:34:49', '2017-01-25 18:34:49', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2017-01-25 16:39:26', '2017-01-25 18:39:26', '<div><p>A consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.</p></div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'A consulta regular ao dentista pode salvar o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-01-25 16:39:26', '2017-01-25 18:39:26', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(207, 1, '2017-01-27 09:47:37', '2017-01-27 11:47:37', '', 'Responsável tecnico', '', 'publish', 'closed', 'closed', '', 'responsavel-tecnico', '', '', '2017-02-24 15:14:27', '2017-02-24 18:14:27', '', 0, 'http://clinicaodontop.com/?page_id=207', 0, 'page', '', 0),
(208, 1, '2017-01-27 09:47:37', '2017-01-27 11:47:37', '', 'Responsável Técnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-01-27 09:47:37', '2017-01-27 11:47:37', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(209, 1, '2017-01-27 09:50:20', '2017-01-27 11:50:20', '', 'Responsável Técnico', '', 'publish', 'closed', 'closed', '', 'acf_responsavel-tecnico', '', '', '2017-01-27 09:50:20', '2017-01-27 11:50:20', '', 0, 'http://clinicaodontop.com/?post_type=acf&#038;p=209', 0, 'acf', '', 0),
(210, 1, '2017-01-27 09:51:51', '2017-01-27 11:51:51', '', 'Responsável Técnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-01-27 09:51:51', '2017-01-27 11:51:51', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(211, 1, '2017-02-14 18:43:04', '2017-02-14 20:43:04', '', 'carla_icone', '', 'inherit', 'open', 'closed', '', 'carla_icone', '', '', '2017-02-14 18:43:04', '2017-02-14 20:43:04', '', 33, 'http://clinicaodontop.com/wp-content/uploads/2017/01/carla_icone.jpg', 0, 'attachment', 'image/jpeg', 0),
(212, 1, '2017-02-14 18:45:05', '2017-02-14 20:45:05', '', 'Responsável Técnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:45:05', '2017-02-14 20:45:05', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(213, 1, '2017-02-14 18:45:18', '2017-02-14 20:45:18', '', 'Teste', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:45:18', '2017-02-14 20:45:18', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(214, 1, '2017-02-14 18:49:07', '2017-02-14 20:49:07', '', 'CARLA_foto', '', 'inherit', 'open', 'closed', '', 'carla_foto', '', '', '2017-02-14 18:49:07', '2017-02-14 20:49:07', '', 207, 'http://clinicaodontop.com/wp-content/uploads/2017/01/CARLA_foto.png', 0, 'attachment', 'image/png', 0),
(215, 1, '2017-02-14 18:49:15', '2017-02-14 20:49:15', '', 'Teste', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:49:15', '2017-02-14 20:49:15', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(216, 1, '2017-02-14 18:49:31', '2017-02-14 20:49:31', '', 'Responsável tecnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:49:31', '2017-02-14 20:49:31', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(217, 1, '2017-02-14 18:50:21', '2017-02-14 20:50:21', '', 'Responsável tecnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:50:21', '2017-02-14 20:50:21', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(218, 1, '2017-02-14 18:51:15', '2017-02-14 20:51:15', '', 'Responsável tecnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-14 18:51:15', '2017-02-14 20:51:15', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(219, 1, '2017-02-14 18:53:32', '2017-02-14 20:53:32', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 18:53:32', '2017-02-14 20:53:32', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(221, 1, '2017-02-14 18:53:54', '2017-02-14 20:53:54', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 18:53:54', '2017-02-14 20:53:54', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(222, 1, '2017-02-14 18:54:39', '2017-02-14 20:54:39', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 18:54:39', '2017-02-14 20:54:39', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(223, 1, '2017-02-14 19:05:28', '2017-02-14 21:05:28', '', 'clinca_odontop_colombo', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_colombo', '', '', '2017-02-14 19:05:28', '2017-02-14 21:05:28', '', 22, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_colombo.jpg', 0, 'attachment', 'image/jpeg', 0),
(224, 1, '2017-02-14 19:06:09', '2017-02-14 21:06:09', '', 'clinca_odontop_quatrobarras', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_quatrobarras', '', '', '2017-02-14 19:06:09', '2017-02-14 21:06:09', '', 21, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_quatrobarras.jpg', 0, 'attachment', 'image/jpeg', 0),
(225, 1, '2017-02-14 19:07:11', '2017-02-14 21:07:11', '', 'clinca_odontop_colombs', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_colombs', '', '', '2017-02-14 19:07:11', '2017-02-14 21:07:11', '', 22, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_colombs.jpg', 0, 'attachment', 'image/jpeg', 0),
(226, 1, '2017-02-14 19:07:40', '2017-02-14 21:07:40', '', 'clinica_odontop_campina', '', 'inherit', 'open', 'closed', '', 'clinica_odontop_campina', '', '', '2017-02-14 19:07:40', '2017-02-14 21:07:40', '', 19, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinica_odontop_campina.jpg', 0, 'attachment', 'image/jpeg', 0),
(227, 1, '2017-02-14 19:07:57', '2017-02-14 21:07:57', '', 'clinca_odontop_riobranco', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_riobranco', '', '', '2017-02-14 19:07:57', '2017-02-14 21:07:57', '', 20, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_riobranco.jpg', 0, 'attachment', 'image/jpeg', 0),
(228, 1, '2017-02-14 19:08:22', '2017-02-14 21:08:22', '', 'clinca_odontop_Almirante', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_almirante', '', '', '2017-02-14 19:08:22', '2017-02-14 21:08:22', '', 13, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_Almirante.jpg', 0, 'attachment', 'image/jpeg', 0),
(229, 1, '2017-02-14 19:14:25', '2017-02-14 21:14:25', '<div>O clareamento é uma técnica usada há anos para retomar a tonalidade branca dos dentes, que por alguma razão foram amarelando com o passar do tempo. Hoje traremos algumas informações importantes sobre este procedimento e como ele tem transformado a autoestima de centenas de pessoas ao redor do mundo.Muito se ouviu falar sobre o clareamento, mas por anos ele esteve recluso somente a algumas pessoas por seu custo exuberante. Conforme o tempo foi passando, a evolução do mercado odontológico \"abriu portas\" para que seus serviços não ficassem restritos somente a uma gama de pacientes.\r\n\r\nHoje existem duas formas de clareamento usadas em todo o Brasil, a laser, realizado somente em consultórios ou o método caseiro, em que a pasta e a moldeira são feitos pelo especialista mas o tratamento é feito em casa embora seu resultado seja mais demorado.\r\n\r\n</div>\r\n<div>\r\n<h4>Quais as diferenças?&lt;h/4&gt;</h4>\r\nLaser - Tratamento mais intenso, pode sensibilizar os dentes na primeiras sessões, mas o desconforto passa em seguida. Atinge a brancura com muito mais facilidade e rapidez. É mais caro.\r\n\r\nCaseiro - Menos abrasivo, o que torna o processo mais demorado. Atinge o branco desejado de forma gradativa e geralmente tem o custo bem abaixo do procedimento por laser.\r\n\r\n</div>\r\n<div>\r\n<h4>Qual o melhor tipo de clareamento?</h4>\r\nNão há melhor ou pior, cada um atinge suas expectativas dentro das suas limitações. Vai depender do paciente e do seu dentista, com o auxílio de um profissional você perceberá que é necessário ter um preparo.\r\nPor isso o acompanhamento de um profissional é altamente importante. Se for feito de maneira errada ou ser mais invasivo do que seu sorriso pode aguentar, a chance de ter muita sensibilidade aumenta mais ainda.\r\n\r\n</div>\r\n<div>\r\n<h4>Por que os dentes escurecem?</h4>\r\nO esmalte que protege os dentes é uma camada finíssima e porosa. Conforme envelhecemos, este esmalte também envelhece e vai se tornando cada vez mais frágil, sua porosidade aumenta a chance de manchas com alimentos e bebidas muito pigmentados, sejam naturais ou não.\r\nE o uso de outros produtos como o tabaco, também auxiliam no amarelamento dos dentes.\r\n\r\nPor isso, mesmo após o clareamento é necessário ter um cuidado maior com os dentes, tentar não deixa-los em contato direto com pigmentos muito concentrados, como o açaí, vinhos, coca-cola, café, chá preto e etc.\r\nA dica dos dentistas, é que se possível em bebidas de cor muito forte, tente ingerir através de canudo, assim o liquido não entra em contato direto com os dentes.\r\n\r\n</div>\r\n<div>Em relação aos alimentos, aguarde no mínimo 30 min após a ingestão para escovar os dentes. Na mastigação, o ácido presente no alimento e na saliva entram em conflito, o que enfraquece o esmalte dos dentes e propicia o surgimento de manchas. Os trinta minutos de aguardo fazem com que o PH da saliva consiga neutralizar a ação dos ácidos e não permitir que os dentes estejam sensíveis na hora da escovação.Se você deseja fazer um clareamento, converse conosco. Aqui na Odontop temos profissionais especializados aguardando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Estética: Clareamento Dental', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2017-02-14 19:14:25', '2017-02-14 21:14:25', '', 103, 'http://clinicaodontop.com/103-revision-v1/', 0, 'revision', '', 0),
(230, 1, '2017-02-14 19:15:23', '2017-02-14 21:15:23', '<div>\r\n\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Mantenha seu sorriso. Visite seu dentista regularmente', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-02-14 19:15:23', '2017-02-14 21:15:23', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(231, 1, '2017-02-14 19:15:47', '2017-02-14 21:15:47', '<div>\r\n\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Mantenha seu sorriso. Faca-nos uma visita', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-02-14 19:15:47', '2017-02-14 21:15:47', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(232, 1, '2017-02-14 19:16:39', '2017-02-14 21:16:39', '<div>A ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\n<div>Esses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.<img class=\"alignnone size-medium wp-image-189\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" alt=\"\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n\r\n</div>\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>Mas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-14 19:16:39', '2017-02-14 21:16:39', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(233, 1, '2017-02-14 19:17:10', '2017-02-14 21:17:10', '<div>\r\n\r\nA consulta regular ao dentista é uma das formas de precaver doenças bucais, cáries, extrações e doenças mais sérias. Quando alarmamos os pacientes de que essa visita é necessária no mínimo 2 vezes ao ano, é porque uma consulta rotineira pode identificar problemas que se não tratados previamente, podem se transformar em grandes incômodos.É comum que os consultórios e clínicas odontológicas sejam frequentados somente para consultas emergenciais, por inúmeros motivos, alguns por medo, outros pelo custo e muitos por postergar o problema até que ele incomode o suficiente para procurar ajuda.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>Benefícios de uma consulta regular:</strong></h4>\r\n<h4><strong>- Prevenção</strong></h4>\r\nO acompanhamento ao dentista pode apresentar o início de algumas doenças que se não tratadas podem prejudicar sua saúde. A boca é apresenta sinais de todo o organismo, o câncer de boca por exemplo, se diagnosticado no início tem chances muito maiores de cura do que se descoberto em nível avançado.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Higiene</strong></h4>\r\nTendo a supervisão de um profissional você pode garantir a higienização bucal em dia, como a retirada de tártaros que se não extraídos podem gerar uma infecção na gengiva e dar início a gengivite.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Além da boca</strong></h4>\r\nA consulta odontológica não se mantém somente na saúde bucal, pequenos problemas não tratados podem acarretar grandes problemas no organismo. Como por exemplo, os tártaros que falamos no item anterior, carregam bactérias que se em contato com a corrente sanguínea (possibilidade facilitada pela Gengivite) podem gerar uma infecção cardiológica.\r\n\r\n</div>\r\n<div>\r\n<h4><strong>- Dar exemplo</strong></h4>\r\nSe você é pai ou mãe, certamente se preocupa com a saúde dos seus filhos, então, aproveite as consultas regulares para dar o exemplo, o acesso das crianças desde cedo ao dentista pode gerar uma segurança maior quando iniciar o tratamento delas também. É muito comum que crianças tenham medo de dentistas, por isso, mostre que não há problema nenhum, muito pelo contrário, torne uma experiência agradável.\r\n\r\n</div>\r\n<div>\r\n\r\nEntão se você não tem o costume de fazer sua consulta regular ao dentista, não perca mais tempo, escolha uma de nossas sedes e marque sua avaliação. Aqui na <a href=\"http://clinicaodontop.com/contato/\">Odontop</a> temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<div>\r\n<h5>Equipe Odontop</h5>\r\n</div>', 'Mantenha seu sorriso. Faca-nos uma visita', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2017-02-14 19:17:10', '2017-02-14 21:17:10', '', 109, 'http://clinicaodontop.com/109-revision-v1/', 0, 'revision', '', 0),
(235, 1, '2017-02-14 19:24:09', '2017-02-14 21:24:09', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 19:24:09', '2017-02-14 21:24:09', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(236, 1, '2017-02-14 19:24:16', '2017-02-14 21:24:16', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-autosave-v1', '', '', '2017-02-14 19:24:16', '2017-02-14 21:24:16', '', 118, 'http://clinicaodontop.com/118-autosave-v1/', 0, 'revision', '', 0),
(237, 1, '2017-02-14 19:24:44', '2017-02-14 21:24:44', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 19:24:44', '2017-02-14 21:24:44', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(238, 1, '2017-02-14 19:25:34', '2017-02-14 21:25:34', '', 'clinca_odontop_header', '', 'inherit', 'open', 'closed', '', 'clinca_odontop_header', '', '', '2017-02-14 19:25:34', '2017-02-14 21:25:34', '', 118, 'http://clinicaodontop.com/wp-content/uploads/2017/01/clinca_odontop_header.jpg', 0, 'attachment', 'image/jpeg', 0),
(239, 1, '2017-02-14 19:25:38', '2017-02-14 21:25:38', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 19:25:38', '2017-02-14 21:25:38', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(240, 1, '2017-02-14 19:25:59', '2017-02-14 21:25:59', '', 'Topo', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2017-02-14 19:25:59', '2017-02-14 21:25:59', '', 118, 'http://clinicaodontop.com/118-revision-v1/', 0, 'revision', '', 0),
(241, 1, '2017-02-14 19:29:27', '2017-02-14 21:29:27', '', 'Rio Branco do Sul', '', 'inherit', 'closed', 'closed', '', '20-autosave-v1', '', '', '2017-02-14 19:29:27', '2017-02-14 21:29:27', '', 20, 'http://clinicaodontop.com/20-autosave-v1/', 0, 'revision', '', 0),
(243, 1, '2017-02-14 19:32:51', '2017-02-14 21:32:51', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-14 19:32:51', '2017-02-14 21:32:51', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(244, 1, '2017-02-14 19:33:36', '2017-02-14 21:33:36', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-14 19:33:36', '2017-02-14 21:33:36', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(245, 1, '2017-02-14 19:42:12', '2017-02-14 21:42:12', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-14 19:42:12', '2017-02-14 21:42:12', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(246, 1, '2017-02-14 19:44:00', '2017-02-14 21:44:00', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-14 19:44:00', '2017-02-14 21:44:00', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(247, 1, '2017-02-14 19:44:04', '2017-02-14 21:44:04', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-autosave-v1', '', '', '2017-02-14 19:44:04', '2017-02-14 21:44:04', '', 4, 'http://clinicaodontop.com/4-autosave-v1/', 0, 'revision', '', 0),
(248, 1, '2017-02-14 19:44:20', '2017-02-14 21:44:20', '', 'A Clínica', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-14 19:44:20', '2017-02-14 21:44:20', '', 4, 'http://clinicaodontop.com/4-revision-v1/', 0, 'revision', '', 0),
(249, 1, '2017-02-20 16:30:23', '2017-02-20 19:30:23', '<div>\r\n\r\nA ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\n<div>\r\n\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"aligncenter wp-image-189 size-medium\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n\r\n</div>\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-20 16:30:23', '2017-02-20 19:30:23', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(250, 1, '2017-02-20 16:30:57', '2017-02-20 19:30:57', '<div>\r\n\r\nA ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\n\r\n\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"aligncenter wp-image-189 size-medium\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n\r\n\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-20 16:30:57', '2017-02-20 19:30:57', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(251, 1, '2017-02-20 16:31:38', '2017-02-20 19:31:38', '<div>\r\n\r\nA ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\n</div>\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"aligncenter wp-image-189 size-full\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n<div>\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n\r\n</div>\r\n<div>\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\n</div>\r\n<div>\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n\r\n</div>\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-20 16:31:38', '2017-02-20 19:31:38', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(252, 1, '2017-02-20 16:33:35', '2017-02-20 19:33:35', 'A ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"alignnone wp-image-189 size-medium\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n\r\n\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-20 16:33:35', '2017-02-20 19:33:35', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(253, 1, '2017-02-20 16:44:59', '2017-02-20 19:44:59', 'A ortodontia é a área responsável por reajustar o alinhamento dos dentes, podendo variar o tempo de tratamento. O médico ortodontista é o profissional que definirá o tempo de tratamento, através da análise.Existem diversos fatores que contribuem para a necessidade do uso de aparelho, seja a forma da mordida, posicionamento dos dentes ou pela influência da mandíbula. Se não tratado, podem gerar inúmeros problemas bucais e com reflexos no corpo todo.\r\n\r\nEsses são os tipos de mordida, cada uma delas influencia a posição que os dentes vão assumir no envelhecimento do ser humano.\r\n<img class=\"aligncenter\" src=\"http://clinicaodontop.com/wp-content/uploads/2017/01/Ortodontia-mordida-300x253-300x253.png\" width=\"300\" height=\"253\" />\r\n\r\nA maioria dos tratamentos são feitos na adolescência, onde o corpo está se adaptando a forma adulta e fica mais “maleável” para algumas mudanças e na boca não é diferente. A arcada dentária ainda está em processo de formação e é mais fácil de se reajustar do que quando o tratamento é feito em idade avançada.\r\n\r\nO resultado não ficará diferente, mas o processo pode ser mais dolorido e trabalhoso.\r\n<h4>Devo procurar um especialista em ortodontia?</h4>\r\nSeu dentista saberá se há ou não a necessidade de um tratamento ortodôntico, por isso a consulta regular é importante. Se feita desde a infância, o médico poderá diagnosticar se a criança sofrerá ou não com dentes tortos.\r\n\r\nLogo, poderá encaminhar para o especialista e dar início ao tratamento desde cedo.\r\nQuanto antes se trata, mais cedo chega-se ao resultado esperado.\r\n<h4>Adultos podem usar aparelho?</h4>\r\nCertamente que sim, mas como falamos anteriormente, quanto mais avantajada for a idade mais difícil será o processo de tratamento. Porque há uma série de agravantes que podem postergar os resultados.\r\n\r\nA partir de uma certa idade, as responsabilidades vão surgindo e com elas nosso tempo vai ficando cada vez mais escasso, por mais que nos dias de hoje existem diversas formas para facilitar, sem prejudicar a auto estima, como com os aparelhos estéticos ainda assim é necessário o acompanhamento contínuo ao dentista.\r\n\r\nMas, o mais importante ainda é ter a saúde bucal em dia. Com os dentes tortos, a limpeza fica cada vez mais difícil e os dentes mais propícios ao acúmulo de resíduos entre eles, que se não retirados vão aumentar a incidência de placa bacteriana e dar vazão ao surgimento de cáries e demais doenças bucais.Por isso, não perca mais tempo. Venha até uma de nossas Clínicas Odontop e marque uma avaliação, aqui temos profissionais especializados esperando por você.\r\n<h5>Equipe Odontop</h5>', 'Ortodontia: Transforme o seu sorriso.', '', 'inherit', 'closed', 'closed', '', '105-revision-v1', '', '', '2017-02-20 16:44:59', '2017-02-20 19:44:59', '', 105, 'http://clinicaodontop.com/105-revision-v1/', 0, 'revision', '', 0),
(254, 1, '2017-02-24 15:14:22', '2017-02-24 18:14:22', '', 'icon_carla', '', 'inherit', 'open', 'closed', '', 'icon_carla', '', '', '2017-02-24 15:14:22', '2017-02-24 18:14:22', '', 207, 'http://clinicaodontop.com/wp-content/uploads/2017/01/icon_carla.png', 0, 'attachment', 'image/png', 0),
(255, 1, '2017-02-24 15:14:27', '2017-02-24 18:14:27', '', 'Responsável tecnico', '', 'inherit', 'closed', 'closed', '', '207-revision-v1', '', '', '2017-02-24 15:14:27', '2017-02-24 18:14:27', '', 207, 'http://clinicaodontop.com/207-revision-v1/', 0, 'revision', '', 0),
(256, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:16:\"page-clinica.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'A Clinica', 'a-clinica', 'publish', 'closed', 'closed', '', 'group_5e3b23e694287', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=256', 0, 'acf-field-group', '', 0),
(257, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:4:\"tabs\";s:3:\"all\";s:5:\"delay\";i:0;}', 'Por que escolher a odontop', 'por_que_escolher_a_odontop', 'publish', 'closed', 'closed', '', 'field_58754664571a6', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=257', 0, 'acf-field', '', 0),
(258, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:4:\"tabs\";s:3:\"all\";s:5:\"delay\";i:0;}', 'História', 'historia', 'publish', 'closed', 'closed', '', 'field_5875468d571a7', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=258', 2, 'acf-field', '', 0),
(259, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:4:\"tabs\";s:3:\"all\";s:5:\"delay\";i:0;}', 'Valores', 'valores', 'publish', 'closed', 'closed', '', 'field_58754698571a8', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=259', 3, 'acf-field', '', 0),
(260, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Formas de Pagamento', 'formas_de_pagamento', 'publish', 'closed', 'closed', '', 'field_587546a3571a9', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=260', 4, 'acf-field', '', 0),
(261, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:17:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";s:6:\"insert\";s:6:\"append\";}', 'Galeria', 'galeria', 'publish', 'closed', 'closed', '', 'field_58754713405f0', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=261', 1, 'acf-field', '', 0),
(262, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Facebook', 'facebook', 'publish', 'closed', 'closed', '', 'field_58779c31a2e47', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 256, 'http://clinicaodontop.com/?post_type=acf-field&p=262', 5, 'acf-field', '', 0),
(263, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"123\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Agendar Consulta', 'agendar-consulta', 'publish', 'closed', 'closed', '', 'group_5e3b23e6b6040', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=263', 0, 'acf-field-group', '', 0),
(264, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'field_58779defed7b5', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=264', 0, 'acf-field', '', 0),
(265, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Título', 'destaque1_titulo', 'publish', 'closed', 'closed', '', 'field_58779e05ed7b6', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=265', 1, 'acf-field', '', 0),
(266, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'destaque1_descricao', 'publish', 'closed', 'closed', '', 'field_58779e18ed7b7', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=266', 2, 'acf-field', '', 0),
(267, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Destaque 2', '', 'publish', 'closed', 'closed', '', 'field_58779e25ed7b8', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=267', 3, 'acf-field', '', 0),
(268, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Título', 'destaque2_titulo', 'publish', 'closed', 'closed', '', 'field_58779e2eed7b9', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=268', 4, 'acf-field', '', 0),
(269, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'destaque2_descricao', 'publish', 'closed', 'closed', '', 'field_58779e37ed7ba', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 263, 'http://clinicaodontop.com/?post_type=acf-field&p=269', 5, 'acf-field', '', 0),
(270, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"depoimentos\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Depoimentos', 'depoimentos', 'publish', 'closed', 'closed', '', 'group_5e3b23e6d80ac', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=270', 0, 'acf-field-group', '', 0),
(271, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_5876373dd5321', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 270, 'http://clinicaodontop.com/?post_type=acf-field&p=271', 0, 'acf-field', '', 0),
(272, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Cargo', 'cargo', 'publish', 'closed', 'closed', '', 'field_58763749d5322', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 270, 'http://clinicaodontop.com/?post_type=acf-field&p=272', 1, 'acf-field', '', 0),
(273, 1, '2020-02-05 17:21:58', '2020-02-05 20:21:58', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'descricao', 'publish', 'closed', 'closed', '', 'field_58763758d5323', '', '', '2020-02-05 17:21:58', '2020-02-05 20:21:58', '', 270, 'http://clinicaodontop.com/?post_type=acf-field&p=273', 2, 'acf-field', '', 0),
(274, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:8:\"doutores\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Doutores', 'doutores', 'publish', 'closed', 'closed', '', 'group_5e3b23e70b14f', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=274', 0, 'acf-field-group', '', 0),
(275, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_587624ed8eb9b', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 274, 'http://clinicaodontop.com/?post_type=acf-field&p=275', 0, 'acf-field', '', 0),
(276, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'CRM', 'crm', 'publish', 'closed', 'closed', '', 'field_587624f88eb9c', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 274, 'http://clinicaodontop.com/?post_type=acf-field&p=276', 1, 'acf-field', '', 0),
(277, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'descricao', 'publish', 'closed', 'closed', '', 'field_587625038eb9d', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 274, 'http://clinicaodontop.com/?post_type=acf-field&p=277', 2, 'acf-field', '', 0),
(278, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"127\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Faixa Laranja Home', 'faixa-laranja-home', 'publish', 'closed', 'closed', '', 'group_5e3b23e7254d1', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=278', 0, 'acf-field-group', '', 0),
(279, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:12:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";i:3;s:6:\"layout\";s:3:\"row\";s:12:\"button_label\";s:18:\"Adicionar Registro\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:9:\"collapsed\";s:0:\"\";}', 'Itens', 'itens', 'publish', 'closed', 'closed', '', 'field_5877a2ece5c26', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 278, 'http://clinicaodontop.com/?post_type=acf-field&p=279', 0, 'acf-field', '', 0),
(280, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Icone', 'icone', 'publish', 'closed', 'closed', '', 'field_5877a311e5c28', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 279, 'http://clinicaodontop.com/?post_type=acf-field&p=280', 0, 'acf-field', '', 0),
(281, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Título', 'titulo', 'publish', 'closed', 'closed', '', 'field_5877a31be5c29', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 279, 'http://clinicaodontop.com/?post_type=acf-field&p=281', 1, 'acf-field', '', 0),
(282, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'descricao', 'publish', 'closed', 'closed', '', 'field_5877a322e5c2a', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 279, 'http://clinicaodontop.com/?post_type=acf-field&p=282', 2, 'acf-field', '', 0),
(283, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:15:\"acf_after_title\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:0:{}s:11:\"description\";s:0:\"\";}', 'Posts', 'posts', 'publish', 'closed', 'closed', '', 'group_5e3b23e740206', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=283', 0, 'acf-field-group', '', 0),
(284, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Breve Descrição', 'breve_descricao', 'publish', 'closed', 'closed', '', 'field_58776c0610352', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 283, 'http://clinicaodontop.com/?post_type=acf-field&p=284', 1, 'acf-field', '', 0),
(285, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:0:\"\";s:13:\"default_value\";i:0;s:2:\"ui\";i:0;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}', 'Mostrar na Página Inicial', 'mostrar_na_pagina_inicial', 'publish', 'closed', 'closed', '', 'field_5877a4ac873fb', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 283, 'http://clinicaodontop.com/?post_type=acf-field&p=285', 0, 'acf-field', '', 0),
(286, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"207\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Responsável Técnico', 'responsavel-tecnico', 'publish', 'closed', 'closed', '', 'group_5e3b23e746f09', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=286', 0, 'acf-field-group', '', 0),
(287, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Nome', 'nome', 'publish', 'closed', 'closed', '', 'field_588b33aec0440', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 286, 'http://clinicaodontop.com/?post_type=acf-field&p=287', 0, 'acf-field', '', 0),
(288, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_588b3388c043f', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 286, 'http://clinicaodontop.com/?post_type=acf-field&p=288', 1, 'acf-field', '', 0),
(289, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'CRO', 'cro', 'publish', 'closed', 'closed', '', 'field_588b33d9c0441', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 286, 'http://clinicaodontop.com/?post_type=acf-field&p=289', 2, 'acf-field', '', 0),
(290, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Descrição', 'descricao', 'publish', 'closed', 'closed', '', 'field_588b33edc0442', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 286, 'http://clinicaodontop.com/?post_type=acf-field&p=290', 3, 'acf-field', '', 0),
(291, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"sedes\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Sedes', 'sedes', 'publish', 'closed', 'closed', '', 'group_5e3b23e751378', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=291', 0, 'acf-field-group', '', 0),
(292, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_58754bfb3a483', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 291, 'http://clinicaodontop.com/?post_type=acf-field&p=292', 0, 'acf-field', '', 0),
(293, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Endereço', 'endereco', 'publish', 'closed', 'closed', '', 'field_58754c083a484', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 291, 'http://clinicaodontop.com/?post_type=acf-field&p=293', 1, 'acf-field', '', 0),
(294, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:12:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"row_min\";s:0:\"\";s:9:\"row_limit\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:18:\"Adicionar Registro\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:9:\"collapsed\";s:0:\"\";}', 'Horários', 'horarios', 'publish', 'closed', 'closed', '', 'field_58754c2e3a485', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 291, 'http://clinicaodontop.com/?post_type=acf-field&p=294', 2, 'acf-field', '', 0),
(295, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Dias', 'dias', 'publish', 'closed', 'closed', '', 'field_58759cefddfc3', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 294, 'http://clinicaodontop.com/?post_type=acf-field&p=295', 0, 'acf-field', '', 0),
(296, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Horário', 'horario', 'publish', 'closed', 'closed', '', 'field_58759cf6ddfc4', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 294, 'http://clinicaodontop.com/?post_type=acf-field&p=296', 1, 'acf-field', '', 0),
(297, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Telefone', 'telefone', 'publish', 'closed', 'closed', '', 'field_58754c513a486', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 291, 'http://clinicaodontop.com/?post_type=acf-field&p=297', 3, 'acf-field', '', 0),
(298, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:10:\"formatting\";s:4:\"html\";s:9:\"maxlength\";s:0:\"\";}', 'Whatsapp', 'whatsapp', 'publish', 'closed', 'closed', '', 'field_58754c593a487', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 291, 'http://clinicaodontop.com/?post_type=acf-field&p=298', 4, 'acf-field', '', 0),
(299, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"tratamentos\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:2:{i:0;s:11:\"the_content\";i:1;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Serviços / Tratamento', 'servicos-tratamento', 'publish', 'closed', 'closed', '', 'group_5e3b23e76072f', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=299', 0, 'acf-field-group', '', 0),
(300, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Icone', 'icone', 'publish', 'closed', 'closed', '', 'field_58762b248a4ec', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 299, 'http://clinicaodontop.com/?post_type=acf-field&p=300', 0, 'acf-field', '', 0),
(301, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Breve Descrição', 'breve_descricao', 'publish', 'closed', 'closed', '', 'field_58762b4c8a4ed', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 299, 'http://clinicaodontop.com/?post_type=acf-field&p=301', 1, 'acf-field', '', 0),
(302, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_58763ed1b045f', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 299, 'http://clinicaodontop.com/?post_type=acf-field&p=302', 2, 'acf-field', '', 0),
(303, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:4:\"tabs\";s:3:\"all\";s:5:\"delay\";i:0;}', 'Descrição', 'descricao', 'publish', 'closed', 'closed', '', 'field_58763ee1b0460', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 299, 'http://clinicaodontop.com/?post_type=acf-field&p=303', 3, 'acf-field', '', 0),
(304, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:17:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:3:\"min\";i:0;s:3:\"max\";i:0;s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";s:6:\"insert\";s:6:\"append\";}', 'Galeria', 'galeria', 'publish', 'closed', 'closed', '', 'field_58763eefb0461', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 299, 'http://clinicaodontop.com/?post_type=acf-field&p=304', 4, 'acf-field', '', 0),
(305, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"118\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:3:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:14:\"featured_image\";}s:11:\"description\";s:0:\"\";}', 'Topo', 'topo', 'publish', 'closed', 'closed', '', 'group_5e3b23e76d315', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 0, 'http://clinicaodontop.com/?post_type=acf-field-group&p=305', 0, 'acf-field-group', '', 0),
(306, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:13:\"return_format\";s:3:\"url\";s:9:\"min_width\";i:0;s:10:\"min_height\";i:0;s:8:\"min_size\";i:0;s:9:\"max_width\";i:0;s:10:\"max_height\";i:0;s:8:\"max_size\";i:0;s:10:\"mime_types\";s:0:\"\";}', 'Imagem', 'imagem', 'publish', 'closed', 'closed', '', 'field_58779ceb62bf4', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 305, 'http://clinicaodontop.com/?post_type=acf-field&p=306', 0, 'acf-field', '', 0),
(307, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Título', 'titulo', 'publish', 'closed', 'closed', '', 'field_58779cfc62bf5', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 305, 'http://clinicaodontop.com/?post_type=acf-field&p=307', 1, 'acf-field', '', 0),
(308, 1, '2020-02-05 17:21:59', '2020-02-05 20:21:59', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Subtítulo', 'subtitulo', 'publish', 'closed', 'closed', '', 'field_58779d0462bf6', '', '', '2020-02-05 17:21:59', '2020-02-05 20:21:59', '', 305, 'http://clinicaodontop.com/?post_type=acf-field&p=308', 2, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Ortodontia', 'ortodontia', 0),
(3, 'clareamento', 'clareamento', 0),
(4, 'Manchas', 'manchas', 0),
(5, 'Prevenção', 'prevencao', 0),
(6, 'prevenção', 'prevencao', 0),
(7, 'saúde', 'saude', 0),
(8, 'Ortodontia', 'ortodontia', 0),
(9, 'Estética', 'estetica', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(103, 3, 0),
(103, 7, 0),
(103, 9, 0),
(105, 2, 0),
(105, 7, 0),
(105, 8, 0),
(106, 5, 0),
(106, 6, 0),
(106, 7, 0),
(106, 8, 0),
(109, 5, 0),
(109, 6, 0),
(109, 7, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'category', '', 0, 1),
(3, 3, 'post_tag', '', 0, 1),
(4, 4, 'post_tag', '', 0, 0),
(5, 5, 'category', '', 0, 2),
(6, 6, 'post_tag', '', 0, 2),
(7, 7, 'post_tag', '', 0, 4),
(8, 8, 'post_tag', '', 0, 2),
(9, 9, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'odontop'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '0'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '3'),
(16, 1, 'wp_user-settings', 'libraryContent=browse&hidetb=1&editor=tinymce&advImgDetails=show'),
(17, 1, 'wp_user-settings-time', '1487619896'),
(18, 1, 'locale', ''),
(19, 2, 'session_tokens', 'a:1:{s:64:\"5c57350577482513d28f2450f78725b9b878fdab8d25cca87d36edc1d34435ea\";a:4:{s:10:\"expiration\";i:1581106563;s:2:\"ip\";s:15:\"179.186.178.203\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\";s:5:\"login\";i:1580933763;}}'),
(20, 1, 'last_login_time', '2020-05-25 14:23:18'),
(21, 1, 'syntax_highlighting', 'true'),
(22, 3, 'nickname', 'gran'),
(23, 3, 'first_name', 'Gra'),
(24, 3, 'last_name', ''),
(25, 3, 'description', ''),
(26, 3, 'rich_editing', 'true'),
(27, 3, 'syntax_highlighting', 'true'),
(28, 3, 'comment_shortcuts', 'false'),
(29, 3, 'admin_color', 'fresh'),
(30, 3, 'use_ssl', '0'),
(31, 3, 'show_admin_bar_front', 'true'),
(32, 3, 'locale', ''),
(33, 3, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(34, 3, 'wp_user_level', '10'),
(35, 3, 'dismissed_wp_pointers', ''),
(37, 3, 'session_tokens', 'a:1:{s:64:\"3b33188961386437e38ba6a79a0d155d5e29d6db3167e456b7f3f251269ed820\";a:4:{s:10:\"expiration\";i:1593281518;s:2:\"ip\";s:13:\"177.82.26.240\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36\";s:5:\"login\";i:1593108718;}}'),
(38, 3, 'last_login_time', '2020-06-25 15:11:58');

-- --------------------------------------------------------

--
-- Estrutura para tabela `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Despejando dados para a tabela `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'clinicaodontop', '$P$B7fUHr.9rBQhR8oqBGPYpKvBOA2fov1', 'odontop', 'lucasbarros22@hotmail.com', '', '2017-01-10 18:16:17', '1580933052:$P$BoHQPTHhAAqkdP8SZu5YlR3EzCdZPL/', 0, 'odontop'),
(3, 'gran', '$P$BYrj.Svv.jIEcmUiHqDZr2ByBJgkPM1', 'gran', 'dev@gran.ag', '', '2020-02-05 20:27:01', '1580934421:$P$BECWpUAnaS7JGsHgZ03F2dX8Wq.Ksr.', 0, 'Gra');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `wp_aiowps_events`
--
ALTER TABLE `wp_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `wp_aiowps_failed_logins`
--
ALTER TABLE `wp_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `wp_aiowps_global_meta`
--
ALTER TABLE `wp_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Índices de tabela `wp_aiowps_login_activity`
--
ALTER TABLE `wp_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `wp_aiowps_login_lockdown`
--
ALTER TABLE `wp_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `wp_aiowps_permanent_block`
--
ALTER TABLE `wp_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Índices de tabela `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Índices de tabela `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Índices de tabela `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Índices de tabela `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Índices de tabela `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Índices de tabela `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Índices de tabela `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Índices de tabela `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `wp_aiowps_events`
--
ALTER TABLE `wp_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_aiowps_failed_logins`
--
ALTER TABLE `wp_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `wp_aiowps_global_meta`
--
ALTER TABLE `wp_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_aiowps_login_activity`
--
ALTER TABLE `wp_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `wp_aiowps_login_lockdown`
--
ALTER TABLE `wp_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `wp_aiowps_permanent_block`
--
ALTER TABLE `wp_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33381;

--
-- AUTO_INCREMENT de tabela `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1602;

--
-- AUTO_INCREMENT de tabela `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;

--
-- AUTO_INCREMENT de tabela `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de tabela `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
